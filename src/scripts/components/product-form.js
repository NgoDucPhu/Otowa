import hugMoneyFormat from "../utils/hugMoneyFormat";
import MicroModal from "micromodal";
// Only need to import these once
import 'unfetch/polyfill';
import 'es6-promise/auto';

// Import @shopify/theme-cart anywhere you need it
import * as cart from '@shopify/theme-cart';

const imgURL = (src, size, crop) => src
  .replace(/_(pico|icon|thumb|small|compact|medium|large|grande|original|500x500|768x768|1024x1024|2048x2048|master)+\./g, '.')
  .replace(/\.jpg|\.png|\.gif|\.jpeg/g, (match) => `_${size}_crop_${crop}${match}`);

class ProductForm extends HTMLElement {
  constructor() {
    super();
    this.product = productJson;
    this.variantSelected = {};
    this.theme = theme;
    this.data = cartInit;

    this.selectVariantsEle = this.querySelector('.product-page .select__select'); 
    this.addBtn = this.querySelector('.product-page .add-to-cart');
    this.productPrice = this.querySelector('.product__detail .price-item--regular');
    this.init();
  }

  init() {
    this.getVariantSelected();
    this.selectVariantsEle && this.updateVartianSelected();
    this.addToCart();
  }

  getVariantSelected() {
    const {variants} = this.product;
    this.variantSelected = variants.find((item) => item.available === true);
  }

  updateVartianSelected() { 
    this.selectVariantsEle.addEventListener('change', (e) => {
      this.handleChangeVariant(Number(e.target.value))
      this.productPrice.innerHTML = hugMoneyFormat(this.variantSelected.price, this.theme);
    })
  }

  addToCart() {
    this.addBtn.addEventListener('click',  (e) => {
     e.preventDefault();
     this.handleAddToCart();
    })
  }

  handleChangeVariant = (variantID) => {
    const variant = this.product.variants.find((variant) => variant.id === variantID);
    this.variantSelected = variant;
    const {avaiable} = this.variantSelected;
    if (!avaiable) this.addBtn.classList.add('p-button--soldout');
    this.addBtn.classList.remove('p-button--soldout');
  };

  handleAddToCart = async () => {
    const formData = {
      items: [{
        id: this.variantSelected.id,
        quantity: 1,
      }],
    };
  
    try {
      const request = await fetch('/cart/add.js', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      
      const res = await fetch('/cart.js');
      if (!res.ok) throw new Error('Bad response from server');
      const result = await res.json();
      
      MicroModal.show('modal-1');

      this.generateCart(result);
    } catch (error) {
      console.log(error);
    }
  };

   generateCart(cart) {
    const tableBodyContent = document.querySelector('#tableContent');
    const totalPrice = document.querySelector('.total__price-final');
    const cartCount = document.querySelector('.cart-count');

    if (cart.item_count > 0) {
      cartCount.classList.add('is-active');
      cartCount.innerHTML = cart.item_count;
    } else {
      cartCount.classList.remove('is-active');
    }

    if (totalPrice) {
      totalPrice.innerHTML = hugMoneyFormat(cart.total_price, this.theme);
    }
    
    const cartContent = cart.items.map((item, index) => {
      return `<tr class="cart-item">
      <td class="row-item">
        <svg class="cart-item__remove" data-key="${item.key}" style="cursor: pointer" width="15" height="15" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M0.736328 1.62598L23.7363 24.626" stroke="#A86D54" stroke-width="2"/>
          <path d="M23.6445 1.70801L0.817893 24.5346" stroke="#A86D54" stroke-width="2"/>
        </svg>
        <a href="${item.url}" class="cart-item__image">
          <img src="${imgURL(item.featured_image.url, '60x60', 'center')}" alt="${item.featured_image.alt}">
          <div class="cart-item__title">${item.title}</div>
        </a>
      </td>
      <td class="row-price">${hugMoneyFormat(item.price, this.theme) }</td>
      <td class="row-quantity">
        <div class="js-input-number" data-variantid="${item.id}" data-key="${item.key}">
          <button type="button" class="input-decreasement">-</button>
          <input class="input-value" type="text" value="${item.quantity}" min="1">
          <button type="button" class="input-increasement">+</button>
        </div>
      </td>
      <td class="row-total">
        ${hugMoneyFormat(item.price * item.quantity, this.theme) }
      </td>
    </tr>`;
    });

     if (tableBodyContent) {
      tableBodyContent.innerHTML = cartContent.join("")
     }

    this.handleIncrease();
    this.handleRemove();
  }

  handleIncrease() {
    const btnDecreasement = document.querySelectorAll('.input-decreasement');
    const btnIncreasement = document.querySelectorAll('.input-increasement');

    btnIncreasement.forEach((item) => {
      item.addEventListener('click', () => {
        const key = item.closest('.js-input-number').dataset.key;
        let quantity = Number(item.previousElementSibling.value) + 1;

        cart.updateItem(key, { quantity })
          .then(state => {
            this.generateCart(state);
          })
          .catch((error) => {
            alert('Sold out or bad response from Shopify')
          });
      })
    })

    btnDecreasement.forEach((item) => {
      item.addEventListener('click', () => {
        const key = item.closest('.js-input-number').dataset.key;
        let quantity = Number(item.nextElementSibling.value) - 1;

        cart.updateItem(key, { quantity })
          .then(state => {
            this.generateCart(state);
          })
          .catch((error) => {
            alert('Sold out or bad response from Shopify')
          });
        
      })
    })
  }

  handleRemove() {
    const removeButton = document.querySelectorAll('.cart-item__remove');
    removeButton.forEach((item) => {
      item.addEventListener('click', () => {
        cart.removeItem(item.dataset.key).then(state => {
          this.generateCart(state)
        });
      })
    })
  }
}

customElements.define('product-form', ProductForm);