import hugMoneyFormat from "../utils/hugMoneyFormat";
import MicroModal from "micromodal";
// Only need to import these once
import 'unfetch/polyfill';
import 'es6-promise/auto';

// Import @shopify/theme-cart anywhere you need it
import * as cart from '@shopify/theme-cart';


const imgURL = (src, size, crop) => src
  .replace(/_(pico|icon|thumb|small|compact|medium|large|grande|original|500x500|768x768|1024x1024|2048x2048|master)+\./g, '.')
  .replace(/\.jpg|\.png|\.gif|\.jpeg/g, (match) => `_${size}_crop_${crop}${match}`);
class TheHeaderComponent extends HTMLElement {
  constructor() {
    super();
    this.theme = theme;

    this.headerCartDrawer = this.querySelector('.header__drawer');

    this.menuBtn = this.querySelector('.p-button--menu');
    this.closeMenuBtn = this.querySelector('.p-button--close');
    this.headerEl = this.querySelector('.header');
    this.details = this.querySelectorAll('li > details');

    this.headerSearchBar = this.querySelector('.header__search-bar');
    this.headerSearchWrapper = this.querySelector('.header__search-wrapper');
    this.input = this.querySelector('.header__search-input');

    this.cartBtn = this.querySelector('.p-button--cart');

    this.customerBtn = this.querySelector('.p-button--customer');
    this.customerList = this.querySelector('.menu-customer__list');
    this.customerName = this.querySelector('.header__drawer-login');

    this.customerName && this.toggleCustomerMenuExpand();

    this.toggleDrawer();
    this.handeSearchExpand();

    this.handleShowCartPopup();
    // this.customerBtn && this.handleCustomerExpand();
  }

  toggleDrawer() {
    this.menuBtn.addEventListener('click', () => {
      this.headerEl.classList.add('header--expand');
      document.body.style.width = '100%';
      document.body.style.overflow = 'hidden';
    })

    this.closeMenuBtn.addEventListener('click', () => {
      this.headerEl.classList.remove('header--expand')
      document.body.style.width = 'unset';
      document.body.style.overflow = 'unset';
    })
  }

  toggleCustomerMenuExpand() {
    this.customerName.addEventListener('click', () => {
      this.headerEl.classList.toggle('is-expand');
    })
  }

  handleShowCartPopup() {
    this.cartBtn.addEventListener('click', async () => {
      const respon = await fetch('/cart.js');
      const responJson = await respon.json();

      if (responJson.item_count >= 1) {
        MicroModal.show('modal-1');
      }

      this.generateCart(responJson);
    })
  }

  generateCart(cart) {
    const tableBodyContent = document.querySelector('#tableContent');
    const totalPrice = document.querySelector('.total__price-final');

    const cartCount = document.querySelector('.cart-count');

    if (cart.item_count > 0) {
      cartCount.classList.add('is-active');
      cartCount.innerHTML = cart.item_count;
    } else {
      cartCount.classList.remove('is-active');
    }


    if (totalPrice) {
      totalPrice.innerHTML = hugMoneyFormat(cart.total_price, this.theme);
    }
    
    const cartContent = cart.items.map((item, index) => {
      return `<tr class="cart-item">
      <td class="row-item">
        <svg class="cart-item__remove" data-key="${item.key}" style="cursor: pointer" width="15" height="15" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M0.736328 1.62598L23.7363 24.626" stroke="#A86D54" stroke-width="2"/>
          <path d="M23.6445 1.70801L0.817893 24.5346" stroke="#A86D54" stroke-width="2"/>
        </svg>
        <a href="${item.url}" class="cart-item__image">
          <img src="${imgURL(item.featured_image.url, '60x60', 'center')}" alt="${item.featured_image.alt}">
          <div class="cart-item__title">${item.title}</div>
        </a>
      </td>
      <td class="row-price">${hugMoneyFormat(item.price, this.theme) }</td>
      <td class="row-quantity">
        <div class="js-input-number" data-variantid="${item.id}" data-key="${item.key}">
          <button type="button" class="input-decreasement">-</button>
          <input class="input-value" type="text" value="${item.quantity}" min="1">
          <button type="button" class="input-increasement">+</button>
        </div>
      </td>
      <td class="row-total">
        ${hugMoneyFormat(item.price * item.quantity, this.theme) }
      </td>
    </tr>`;
    });

     if (tableBodyContent) {
      tableBodyContent.innerHTML = cartContent.join("")
     }

     this.handleIncrease();
    this.handleRemove();
  }

  handleIncrease() {
    const btnDecreasement = document.querySelectorAll('.input-decreasement');
    const btnIncreasement = document.querySelectorAll('.input-increasement');

    
    btnIncreasement.forEach((item) => {
      item.addEventListener('click', () => {
        const key = item.closest('.js-input-number').dataset.key;
        let quantity = Number(item.previousElementSibling.value) + 1;


        cart.updateItem(key, { quantity })
          .then(state => {
            this.generateCart(state);
          })
          .catch((error) => {
            alert('Sold out or bad response from Shopify')
          });
        
      })
    })

    btnDecreasement.forEach((item) => {
      item.addEventListener('click', () => {
        const key = item.closest('.js-input-number').dataset.key;
        let quantity = Number(item.nextElementSibling.value) - 1;

        cart.updateItem(key, { quantity })
          .then(state => {
            this.generateCart(state);
          })
          .catch((error) => {
            alert('Sold out or bad response from Shopify')
          });
        
      })
    })
  }

  handleRemove() {
    const removeButton = document.querySelectorAll('.cart-item__remove');
    removeButton.forEach((item) => {
      item.addEventListener('click', () => {
        cart.removeItem(item.dataset.key).then(state => {
          this.generateCart(state)
        });
      })
    })
  }

  handeSearchExpand() {
    this.headerSearchWrapper.addEventListener('click', () => {
      this.headerSearchBar.classList.add('is-active');

      window.addEventListener('click', (e) => {   
        if (!this.headerSearchBar.contains(e.target)){
          this.input.value = '';
          this.headerSearchBar.classList.remove('is-active');
        }
      });
    })
  }
}

customElements.define('the-header-component', TheHeaderComponent)
