import Splide from '@splidejs/splide';

class ProductSlider extends HTMLElement {
  splideContainer;

  constructor() {
    super();
    this.init();
  }

  init() {
    document.addEventListener('DOMContentLoaded', function () {
      var main = new Splide('#main-slider', {
        type: 'slide',
        rewind: true,
        pagination: false,
        arrows: false
      });

      var thumbnails = new Splide('#thumbnail-slider', {
        fixedWidth: 50,
        fixedHeight: 50,
        gap: 5,
        rewind: true,
        pagination: false,
        cover: true,
        isNavigation: true,
        arrows: false,
      });

      main.sync(thumbnails);
      main.mount();
      thumbnails.mount();
    });
  }
}

customElements.define('product-slider-component', ProductSlider);