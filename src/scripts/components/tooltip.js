import tippy from 'tippy.js';

/**
 Render tooltip component

 Accepts:
 - data-content : Normal content tooltip

 Usage: 
 
 <tooltip-component>
    <template>
      <strong>Bolded content</strong>
    </template>
    <button class="p-button p-button--primary p-button--info">
      {%- render 'component-icon' icon: 'info', icon_size: 25 -%}
    </button>
  </tooltip-component>


  or ( Normal content with data-content attr )

  <tooltip-component
    data-content="Hello Rio Tooltip"
  >
    <button class="p-button p-button--primary p-button--info">
      {%- render 'component-icon' icon: 'info', icon_size: 25 -%}
    </button>
  </tooltip-component>
 */

class TooltipComponent extends HTMLElement {
  constructor() {
    super();
    this.tooltipContent = this.dataset.content;
    this.tooltipTemplate = this.querySelector('template');
    this.isTemplate = Boolean(this.tooltipTemplate);

    this.init();
  }

  init() {
    if (this.isTemplate) {
      this.onTooltipHTML();
    } else {
      this.onTooltipContent();
    }
  }

  onTooltipContent() {
    tippy(this, {
      theme: 'light',
      content: this.tooltipContent
    });
  }

  onTooltipHTML() {
    tippy(this, {
      theme: 'light',
      content: this.tooltipTemplate.innerHTML,
      allowHTML: true,
      interactive: true
    });
  }
}

customElements.define('tooltip-component', TooltipComponent)
