import Splide from '@splidejs/splide';

class SlideshowSlider extends HTMLElement {
  splideContainer;

  constructor() {
    super();
    this.splideContainer = document.querySelector('.slideshow-slider-container');
    this.init();
  }

  init() {
    this.handleSlider();
  }

  handleSlider = () => {
    const container = document.querySelector('.slideshow .splide');

    const splide = new Splide(this.splideContainer, {
      rewind: true,
      type: 'loop',
      perPage: 2,
      autoplay: Boolean(container.dataset.autoplay),
      interval: container.dataset.autoplay ? (Number(container.dataset.interval)) : 0,
      breakpoints: {
        768: {
          perPage: 1,
          padding: '0%',
        },
        992: {
          perPage: 1,
          padding: '20%',
        },
        1200: {
          perPage: 1,
          padding: '20%',
        },
        1440: {
          perPage: 1,
          padding: '20%',
        },
        1920: {
          perPage: 1,
          padding: '20%',
        },
      },
    });
    splide.mount();
  };
}

customElements.define('slideshow-slider', SlideshowSlider);