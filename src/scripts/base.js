import MicroModal from "micromodal";
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import hugMoneyFormat from "./utils/hugMoneyFormat";
// Only need to import these once
import 'unfetch/polyfill';
import 'es6-promise/auto';

// Import @shopify/theme-cart anywhere you need it
import * as cart from '@shopify/theme-cart';

const imgURL = (src, size, crop) => src
  .replace(/_(pico|icon|thumb|small|compact|medium|large|grande|original|500x500|768x768|1024x1024|2048x2048|master)+\./g, '.')
  .replace(/\.jpg|\.png|\.gif|\.jpeg/g, (match) => `_${size}_crop_${crop}${match}`);


if (document.querySelector('.faq_shiping')) {
  document.querySelector('.faq_shiping').addEventListener('click', () => {
    MicroModal.show('modal-shiping');
  })
}


if (document.querySelector('.specified-item-link-popup')) {
  document.querySelector('.specified-item-link-popup').addEventListener('click', () => {
    MicroModal.show('modal-shiping');
  })
}

if (document.getElementById('register')) {
  const input = document.querySelector('.field-check input');
  const button = document.querySelector('.field-check__btn');

  input.addEventListener('click', () => {
    input.checked ? button.classList.remove('p-button--soldout') : button.classList.add('p-button--soldout')
  })
}

fetch('/cart.js')
  .then(response => response.json())
  .then(data => {
    const cartCount = document.querySelector('.cart-count');

    if (data.item_count > 0) {
      cartCount.classList.add('is-active');
      cartCount.innerHTML = data.item_count;
    } else {
      cartCount.classList.remove('is-active');
    }
  });
class ShipingPopuup extends HTMLElement {
  constructor() {
    super();
    this.openPopupBtn = this.querySelector('button');

    this.onOpen();
  }

  onOpen() {
    this.openPopupBtn.addEventListener('click', () => {
      MicroModal.show('modal-shiping');
    })
  }

}

customElements.define('shiping-popup-component', ShipingPopuup);


class MessageBox extends HTMLElement {
  constructor() {
    super();
    this.openPopupBtn = this.querySelector('a');

    this.onOpen();
  }

  onOpen() {
    this.openPopupBtn.addEventListener('click', () => {
      MicroModal.show('modal-message')
    })
  }
}

customElements.define('message-box', MessageBox);

class SoldoutModal extends HTMLElement {
  constructor() {
    super();
    this.openPopupBtn = this.querySelector('a');

    this.onOpen();
  }

  onOpen() {
    this.openPopupBtn.addEventListener('click', () => {
      MicroModal.show('modal-soldout');
    })
  }
}

customElements.define('soldout-modal', SoldoutModal);

class ProductInfor extends HTMLElement {
  constructor() {
    super();

    this.wrapperEle = this.querySelector('.product__infor');
    this.imgSrc = document.querySelector('#img-src');
    this.productTitle = document.querySelector('#product-page-vue');
    this.init();
  }

  init() {
    if (this.imgSrc && this.productTitle) {
      const src = this.imgSrc.dataset.src;
      const title = this.productTitle.dataset.productTitle;
      const content = `<img src="${src}" alt="image">
        <label>${title}</label>
        <input name="contact[product]" value="${title}" style="display: none;">
      `;

      this.wrapperEle.innerHTML = content;
    }
  }
}

customElements.define('product-infor', ProductInfor);

class CartModal extends HTMLElement {
  constructor() {
    super();
    this.theme = theme;
    this.dateEle = this.querySelector('.date');
    this.timeEle = this.querySelector('#time');
    this.btnCheckout = this.querySelectorAll('.btn__checkout');
    this.loginEle = this.querySelector('.btn__checkout--showmodal');
    this.inputDate = this.querySelector('.table-date input[type="date"]')
    this.init();
  }

  init() {
    this.handleChangeTime();
    this.handleChangeDate();
    this.handleUpdateNote();
    this.handleDatePicker();
    this.loginEle && this.showLoginModal();
    window.addEventListener('DOMContentLoaded', async () => {
      if (window.location.href.indexOf('cart') != -1) {
        const respon = await fetch('/cart.js');
        const responJson = await respon.json();

        if (responJson.item_count >= 1) {
          MicroModal.show('modal-1');
        }

        this.generateCart(responJson);
      }
    });
  }

  generateCart(cart) {
    const tableBodyContent = document.querySelector('#tableContent');
    const totalPrice = document.querySelector('.total__price-final');

    const cartCount = document.querySelector('.cart-count');

    if (cart.item_count > 0) {
      cartCount.classList.add('is-active');
      cartCount.innerHTML = cart.item_count;
    } else {
      cartCount.classList.remove('is-active');
    }


    if (totalPrice) {
      totalPrice.innerHTML = hugMoneyFormat(cart.total_price, this.theme);
    }
    
    const cartContent = cart.items.map((item, index) => {
      return `<tr class="cart-item">
      <td class="row-item">
        <svg class="cart-item__remove" data-key="${item.key}" style="cursor: pointer" width="15" height="15" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M0.736328 1.62598L23.7363 24.626" stroke="#A86D54" stroke-width="2"/>
          <path d="M23.6445 1.70801L0.817893 24.5346" stroke="#A86D54" stroke-width="2"/>
        </svg>
        <a href="${item.url}" class="cart-item__image">
          <img src="${imgURL(item.featured_image.url, '60x60', 'center')}" alt="${item.featured_image.alt}">
          <div class="cart-item__title">${item.title}</div>
        </a>
      </td>
      <td class="row-price">${hugMoneyFormat(item.price, this.theme) }</td>
      <td class="row-quantity">
        <div class="js-input-number" data-variantid="${item.id}" data-key="${item.key}">
          <button type="button" class="input-decreasement">-</button>
          <input class="input-value" type="text" value="${item.quantity}" min="1">
          <button type="button" class="input-increasement">+</button>
        </div>
      </td>
      <td class="row-total">
        ${hugMoneyFormat(item.price * item.quantity, this.theme) }
      </td>
    </tr>`;
    });

     if (tableBodyContent) {
      tableBodyContent.innerHTML = cartContent.join("")
     }

     this.handleIncrease();
      this.handleRemove();
  }

  handleIncrease() {
    const btnDecreasement = document.querySelectorAll('.input-decreasement');
    const btnIncreasement = document.querySelectorAll('.input-increasement');

    
    btnIncreasement.forEach((item) => {
      item.addEventListener('click', () => {
        const key = item.closest('.js-input-number').dataset.key;
        let quantity = Number(item.previousElementSibling.value) + 1;


        cart.updateItem(key, { quantity })
          .then(state => {
            this.generateCart(state);
          })
          .catch((error) => {
            alert('Sold out or bad response from Shopify')
          });
        
      })
    })

    btnDecreasement.forEach((item) => {
      item.addEventListener('click', () => {
        const key = item.closest('.js-input-number').dataset.key;
        let quantity = Number(item.nextElementSibling.value) - 1;

        cart.updateItem(key, { quantity })
          .then(state => {
            this.generateCart(state);
          })
          .catch((error) => {
            alert('Sold out or bad response from Shopify')
          });
        
      })
    })
  }

  handleRemove() {
    const removeButton = document.querySelectorAll('.cart-item__remove');
    removeButton.forEach((item) => {
      item.addEventListener('click', () => {
        cart.removeItem(item.dataset.key).then(state => {
          this.generateCart(state)
        });
      })
    })
  }

  showLoginModal() {
    this.loginEle.addEventListener('click', () => {
      MicroModal.show('modal-login');
    })
  }

  handleChangeDate() {
    this.dateEle.setAttribute('data-date', '');
    if (localStorage.getItem('date')) {
      var now = $.datepicker.formatDate('yy/mm/dd', new Date());
      if( localStorage.getItem('date') <= now) {
        this.dateEle.value = '';
        localStorage.setItem('date', '');
      } else {
        this.dateEle.value =localStorage.getItem('date');
      }
    }

    // this.dateEle.addEventListener('change', (e) => {
    //   console.log("event change: ", e.target);
    //   let date = '';
    //   date = e.target.value;
    //   date = `お届け希望日 : ${date}`;
    //   this.dateEle.setAttribute('data-date', date);

    //   const dateStorage = e.target.value;
    //   localStorage.setItem('date', dateStorage);
    // })
    $("#datepicker").on("change",function(){
        let date = '';
        date = $(this).val();
        date = `お届け希望日 : ${date}`;
        $(this).attr('data-date', date);

        const dateStorage = $(this).val();
        localStorage.setItem('date', dateStorage);
    });

  }

  handleChangeTime() {
    this.timeEle.setAttribute('data-time', '');

    if (localStorage.getItem('time')) {
      this.timeEle.value = localStorage.getItem('time');
    }
    
    this.timeEle.addEventListener('change', (e) => {
      let time = '';
      if (e.target.value !== '指定なし') {
        time = `時間帯指定 : ${e.target.value}`;
        this.timeEle.setAttribute('data-time', time);
      }

      const timeStorage = e.target.value;
      localStorage.setItem('time', timeStorage);
    })
  }

  handleUpdateNote() {
    this.btnCheckout.forEach(item => {
      item.addEventListener('click', async () => {
        let date = this.dateEle.dataset.date;
        let time = this.timeEle.dataset.time;

        if (date && time) {
          await fetch('/cart/update.js', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              note: `${date}, ${time}`
            }),
          });
        }
      })
    })
  }

  // handleDisablePastDate() {
  //   this.dateEle.setAttribute('min', '');
  //   var dtToday = new Date();
    
  //   var month = dtToday.getMonth() + 1;
  //   var day = dtToday.getDate() + 1;
  //   var year = dtToday.getFullYear();
  //   if(month < 10)
  //       month = '0' + month.toString();
  //   if(day < 10)
  //       day = '0' + day.toString();
    
  //   var minDate = year + '-' + month + '-' + day;


  //   this.dateEle.setAttribute('min', minDate);
  // }

  handleDatePicker() {
    $("#datepicker").datepicker({
      minDate: '+1D', //最短日の指定*3日後から指定可能
      maxDate: '+2M', //最長日の指定*2ヶ月後まで指定可能
       beforeShowDay: function(date) {
         if (date.getDay() == 0) {
           return [true, 'day-sunday', null];//trueで土曜日を選択可に変更
         } else if (date.getDay() == 6) {
           return [true, 'day-saturday', null];//trueで日曜日を選択可に変更
         }
         return [true, 'day-weekday', null];
       }
     });
  }
}

customElements.define('cart-modal', CartModal);