import { formatMoney } from '@shopify/theme-currency';

const hugMoneyFormat = (value, theme) => {
  const currencyFormat = formatMoney(Number(value), theme.moneyFormat);
  const currencyDecimal = currencyFormat.slice(currencyFormat.indexOf(','));
  if (currencyDecimal === ',00') {
    return currencyFormat.slice(0, currencyFormat.indexOf(','));
  }
  return currencyFormat;
};

export default hugMoneyFormat;