/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/scripts/components/the-header.js":
/*!**********************************************!*\
  !*** ./src/scripts/components/the-header.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/wrapNativeSuper */ "./node_modules/@babel/runtime/helpers/esm/wrapNativeSuper.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utils/hugMoneyFormat */ "./src/scripts/utils/hugMoneyFormat.js");
/* harmony import */ var micromodal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! micromodal */ "./node_modules/micromodal/dist/micromodal.es.js");
/* harmony import */ var unfetch_polyfill__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! unfetch/polyfill */ "./node_modules/unfetch/polyfill/index.js");
/* harmony import */ var unfetch_polyfill__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(unfetch_polyfill__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var es6_promise_auto__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! es6-promise/auto */ "./node_modules/es6-promise/auto.js");
/* harmony import */ var es6_promise_auto__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(es6_promise_auto__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @shopify/theme-cart */ "./node_modules/@shopify/theme-cart/theme-cart.js");









function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0,_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0,_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0,_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }


 // Only need to import these once


 // Import @shopify/theme-cart anywhere you need it



var imgURL = function imgURL(src, size, crop) {
  return src.replace(/_(pico|icon|thumb|small|compact|medium|large|grande|original|500x500|768x768|1024x1024|2048x2048|master)+\./g, '.').replace(/\.jpg|\.png|\.gif|\.jpeg/g, function (match) {
    return "_".concat(size, "_crop_").concat(crop).concat(match);
  });
};

var TheHeaderComponent = /*#__PURE__*/function (_HTMLElement) {
  (0,_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(TheHeaderComponent, _HTMLElement);

  var _super = _createSuper(TheHeaderComponent);

  function TheHeaderComponent() {
    var _this;

    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, TheHeaderComponent);

    _this = _super.call(this);
    _this.theme = theme;
    _this.headerCartDrawer = _this.querySelector('.header__drawer');
    _this.menuBtn = _this.querySelector('.p-button--menu');
    _this.closeMenuBtn = _this.querySelector('.p-button--close');
    _this.headerEl = _this.querySelector('.header');
    _this.details = _this.querySelectorAll('li > details');
    _this.headerSearchBar = _this.querySelector('.header__search-bar');
    _this.headerSearchWrapper = _this.querySelector('.header__search-wrapper');
    _this.input = _this.querySelector('.header__search-input');
    _this.cartBtn = _this.querySelector('.p-button--cart');
    _this.customerBtn = _this.querySelector('.p-button--customer');
    _this.customerList = _this.querySelector('.menu-customer__list');
    _this.customerName = _this.querySelector('.header__drawer-login');
    _this.customerName && _this.toggleCustomerMenuExpand();

    _this.toggleDrawer();

    _this.handeSearchExpand();

    _this.handleShowCartPopup(); // this.customerBtn && this.handleCustomerExpand();


    return _this;
  }

  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(TheHeaderComponent, [{
    key: "toggleDrawer",
    value: function toggleDrawer() {
      var _this2 = this;

      this.menuBtn.addEventListener('click', function () {
        _this2.headerEl.classList.add('header--expand');

        document.body.style.width = '100%';
        document.body.style.overflow = 'hidden';
      });
      this.closeMenuBtn.addEventListener('click', function () {
        _this2.headerEl.classList.remove('header--expand');

        document.body.style.width = 'unset';
        document.body.style.overflow = 'unset';
      });
    }
  }, {
    key: "toggleCustomerMenuExpand",
    value: function toggleCustomerMenuExpand() {
      var _this3 = this;

      this.customerName.addEventListener('click', function () {
        _this3.headerEl.classList.toggle('is-expand');
      });
    }
  }, {
    key: "handleShowCartPopup",
    value: function handleShowCartPopup() {
      var _this4 = this;

      this.cartBtn.addEventListener('click', /*#__PURE__*/(0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__["default"])( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default().mark(function _callee() {
        var respon, responJson;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return fetch('/cart.js');

              case 2:
                respon = _context.sent;
                _context.next = 5;
                return respon.json();

              case 5:
                responJson = _context.sent;

                if (responJson.item_count >= 1) {
                  micromodal__WEBPACK_IMPORTED_MODULE_9__["default"].show('modal-1');
                }

                _this4.generateCart(responJson);

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      })));
    } // handleCustomerExpand() {
    //   this.customerBtn.addEventListener('mouseover', () => {
    //     this.customerList.classList.add('is-active');
    //   });
    //   this.customerBtn.addEventListener('mouseleave', () => {
    //     this.customerList.classList.remove('is-active');
    //   });
    // }

  }, {
    key: "generateCart",
    value: function generateCart(cart) {
      var _this5 = this;

      var tableBodyContent = document.querySelector('#tableContent');
      var totalPrice = document.querySelector('.total__price-final');
      var cartCount = document.querySelector('.cart-count');

      if (cart.item_count > 0) {
        cartCount.classList.add('is-active');
        cartCount.innerHTML = cart.item_count;
      } else {
        cartCount.classList.remove('is-active');
      }

      if (totalPrice) {
        totalPrice.innerHTML = (0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_8__["default"])(cart.total_price, this.theme);
      }

      var cartContent = cart.items.map(function (item, index) {
        return "<tr class=\"cart-item\">\n      <td class=\"row-item\">\n        <svg class=\"cart-item__remove\" data-key=\"".concat(item.key, "\" style=\"cursor: pointer\" width=\"15\" height=\"15\" viewBox=\"0 0 25 26\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n          <path d=\"M0.736328 1.62598L23.7363 24.626\" stroke=\"#A86D54\" stroke-width=\"2\"/>\n          <path d=\"M23.6445 1.70801L0.817893 24.5346\" stroke=\"#A86D54\" stroke-width=\"2\"/>\n        </svg>\n        <a href=\"").concat(item.url, "\" class=\"cart-item__image\">\n          <img src=\"").concat(imgURL(item.featured_image.url, '60x60', 'center'), "\" alt=\"").concat(item.featured_image.alt, "\">\n          <div class=\"cart-item__title\">").concat(item.title, "</div>\n        </a>\n      </td>\n      <td class=\"row-price\">").concat((0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_8__["default"])(item.price, _this5.theme), "</td>\n      <td class=\"row-quantity\">\n        <div class=\"js-input-number\" data-variantid=\"").concat(item.id, "\" data-key=\"").concat(item.key, "\">\n          <button type=\"button\" class=\"input-decreasement\">-</button>\n          <input class=\"input-value\" type=\"text\" value=\"").concat(item.quantity, "\" min=\"1\">\n          <button type=\"button\" class=\"input-increasement\">+</button>\n        </div>\n      </td>\n      <td class=\"row-total\">\n        ").concat((0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_8__["default"])(item.price * item.quantity, _this5.theme), "\n      </td>\n    </tr>");
      });

      if (tableBodyContent) {
        tableBodyContent.innerHTML = cartContent.join("");
      }

      this.handleIncrease();
      this.handleRemove();
    }
  }, {
    key: "handleIncrease",
    value: function handleIncrease() {
      var _this6 = this;

      var btnDecreasement = document.querySelectorAll('.input-decreasement');
      var btnIncreasement = document.querySelectorAll('.input-increasement');
      btnIncreasement.forEach(function (item) {
        item.addEventListener('click', function () {
          var key = item.closest('.js-input-number').dataset.key;
          var quantity = Number(item.previousElementSibling.value) + 1;
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_12__.updateItem(key, {
            quantity: quantity
          }).then(function (state) {
            _this6.generateCart(state);
          }).catch(function (error) {
            alert('Sold out or bad response from Shopify');
          });
        });
      });
      btnDecreasement.forEach(function (item) {
        item.addEventListener('click', function () {
          var key = item.closest('.js-input-number').dataset.key;
          var quantity = Number(item.nextElementSibling.value) - 1;
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_12__.updateItem(key, {
            quantity: quantity
          }).then(function (state) {
            _this6.generateCart(state);
          }).catch(function (error) {
            alert('Sold out or bad response from Shopify');
          });
        });
      });
    }
  }, {
    key: "handleRemove",
    value: function handleRemove() {
      var _this7 = this;

      var removeButton = document.querySelectorAll('.cart-item__remove');
      removeButton.forEach(function (item) {
        item.addEventListener('click', function () {
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_12__.removeItem(item.dataset.key).then(function (state) {
            _this7.generateCart(state);
          });
        });
      });
    }
  }, {
    key: "handeSearchExpand",
    value: function handeSearchExpand() {
      var _this8 = this;

      this.headerSearchWrapper.addEventListener('click', function () {
        _this8.headerSearchBar.classList.add('is-active');

        window.addEventListener('click', function (e) {
          if (!_this8.headerSearchBar.contains(e.target)) {
            _this8.input.value = '';

            _this8.headerSearchBar.classList.remove('is-active');
          }
        });
      });
    }
  }]);

  return TheHeaderComponent;
}( /*#__PURE__*/(0,_babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__["default"])(HTMLElement));

customElements.define('the-header-component', TheHeaderComponent);

/***/ }),

/***/ "./src/scripts/utils/hugMoneyFormat.js":
/*!*********************************************!*\
  !*** ./src/scripts/utils/hugMoneyFormat.js ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shopify_theme_currency__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shopify/theme-currency */ "./node_modules/@shopify/theme-currency/currency.js");


var hugMoneyFormat = function hugMoneyFormat(value, theme) {
  var currencyFormat = (0,_shopify_theme_currency__WEBPACK_IMPORTED_MODULE_0__.formatMoney)(Number(value), theme.moneyFormat);
  var currencyDecimal = currencyFormat.slice(currencyFormat.indexOf(','));

  if (currencyDecimal === ',00') {
    return currencyFormat.slice(0, currencyFormat.indexOf(','));
  }

  return currencyFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (hugMoneyFormat);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"component-the-header": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkworkflow"] = self["webpackChunkworkflow"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors"], function() { return __webpack_require__("./src/scripts/components/the-header.js"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LXRoZS1oZWFkZXIuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0NBRUE7O0FBQ0E7Q0FHQTs7QUFDQTs7QUFHQSxJQUFNRyxNQUFNLEdBQUcsU0FBVEEsTUFBUyxDQUFDQyxHQUFELEVBQU1DLElBQU4sRUFBWUMsSUFBWjtBQUFBLFNBQXFCRixHQUFHLENBQ3BDRyxPQURpQyxDQUN6Qiw4R0FEeUIsRUFDdUYsR0FEdkYsRUFFakNBLE9BRmlDLENBRXpCLDJCQUZ5QixFQUVJLFVBQUNDLEtBQUQ7QUFBQSxzQkFBZUgsSUFBZixtQkFBNEJDLElBQTVCLFNBQW1DRSxLQUFuQztBQUFBLEdBRkosQ0FBckI7QUFBQSxDQUFmOztJQUdNQzs7Ozs7QUFDSixnQ0FBYztBQUFBOztBQUFBOztBQUNaO0FBQ0EsVUFBS0MsS0FBTCxHQUFhQSxLQUFiO0FBRUEsVUFBS0MsZ0JBQUwsR0FBd0IsTUFBS0MsYUFBTCxDQUFtQixpQkFBbkIsQ0FBeEI7QUFFQSxVQUFLQyxPQUFMLEdBQWUsTUFBS0QsYUFBTCxDQUFtQixpQkFBbkIsQ0FBZjtBQUNBLFVBQUtFLFlBQUwsR0FBb0IsTUFBS0YsYUFBTCxDQUFtQixrQkFBbkIsQ0FBcEI7QUFDQSxVQUFLRyxRQUFMLEdBQWdCLE1BQUtILGFBQUwsQ0FBbUIsU0FBbkIsQ0FBaEI7QUFDQSxVQUFLSSxPQUFMLEdBQWUsTUFBS0MsZ0JBQUwsQ0FBc0IsY0FBdEIsQ0FBZjtBQUVBLFVBQUtDLGVBQUwsR0FBdUIsTUFBS04sYUFBTCxDQUFtQixxQkFBbkIsQ0FBdkI7QUFDQSxVQUFLTyxtQkFBTCxHQUEyQixNQUFLUCxhQUFMLENBQW1CLHlCQUFuQixDQUEzQjtBQUNBLFVBQUtRLEtBQUwsR0FBYSxNQUFLUixhQUFMLENBQW1CLHVCQUFuQixDQUFiO0FBRUEsVUFBS1MsT0FBTCxHQUFlLE1BQUtULGFBQUwsQ0FBbUIsaUJBQW5CLENBQWY7QUFFQSxVQUFLVSxXQUFMLEdBQW1CLE1BQUtWLGFBQUwsQ0FBbUIscUJBQW5CLENBQW5CO0FBQ0EsVUFBS1csWUFBTCxHQUFvQixNQUFLWCxhQUFMLENBQW1CLHNCQUFuQixDQUFwQjtBQUNBLFVBQUtZLFlBQUwsR0FBb0IsTUFBS1osYUFBTCxDQUFtQix1QkFBbkIsQ0FBcEI7QUFFQSxVQUFLWSxZQUFMLElBQXFCLE1BQUtDLHdCQUFMLEVBQXJCOztBQUVBLFVBQUtDLFlBQUw7O0FBQ0EsVUFBS0MsaUJBQUw7O0FBRUEsVUFBS0MsbUJBQUwsR0ExQlksQ0EyQlo7OztBQTNCWTtBQTRCYjs7OztXQUVELHdCQUFlO0FBQUE7O0FBQ2IsV0FBS2YsT0FBTCxDQUFhZ0IsZ0JBQWIsQ0FBOEIsT0FBOUIsRUFBdUMsWUFBTTtBQUMzQyxjQUFJLENBQUNkLFFBQUwsQ0FBY2UsU0FBZCxDQUF3QkMsR0FBeEIsQ0FBNEIsZ0JBQTVCOztBQUNBQyxRQUFBQSxRQUFRLENBQUNDLElBQVQsQ0FBY0MsS0FBZCxDQUFvQkMsS0FBcEIsR0FBNEIsTUFBNUI7QUFDQUgsUUFBQUEsUUFBUSxDQUFDQyxJQUFULENBQWNDLEtBQWQsQ0FBb0JFLFFBQXBCLEdBQStCLFFBQS9CO0FBQ0QsT0FKRDtBQU1BLFdBQUt0QixZQUFMLENBQWtCZSxnQkFBbEIsQ0FBbUMsT0FBbkMsRUFBNEMsWUFBTTtBQUNoRCxjQUFJLENBQUNkLFFBQUwsQ0FBY2UsU0FBZCxDQUF3Qk8sTUFBeEIsQ0FBK0IsZ0JBQS9COztBQUNBTCxRQUFBQSxRQUFRLENBQUNDLElBQVQsQ0FBY0MsS0FBZCxDQUFvQkMsS0FBcEIsR0FBNEIsT0FBNUI7QUFDQUgsUUFBQUEsUUFBUSxDQUFDQyxJQUFULENBQWNDLEtBQWQsQ0FBb0JFLFFBQXBCLEdBQStCLE9BQS9CO0FBQ0QsT0FKRDtBQUtEOzs7V0FFRCxvQ0FBMkI7QUFBQTs7QUFDekIsV0FBS1osWUFBTCxDQUFrQkssZ0JBQWxCLENBQW1DLE9BQW5DLEVBQTRDLFlBQU07QUFDaEQsY0FBSSxDQUFDZCxRQUFMLENBQWNlLFNBQWQsQ0FBd0JRLE1BQXhCLENBQStCLFdBQS9CO0FBQ0QsT0FGRDtBQUdEOzs7V0FFRCwrQkFBc0I7QUFBQTs7QUFDcEIsV0FBS2pCLE9BQUwsQ0FBYVEsZ0JBQWIsQ0FBOEIsT0FBOUIsd0xBQXVDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBQ2hCVSxLQUFLLENBQUMsVUFBRCxDQURXOztBQUFBO0FBQy9CQyxnQkFBQUEsTUFEK0I7QUFBQTtBQUFBLHVCQUVaQSxNQUFNLENBQUNDLElBQVAsRUFGWTs7QUFBQTtBQUUvQkMsZ0JBQUFBLFVBRitCOztBQUlyQyxvQkFBSUEsVUFBVSxDQUFDQyxVQUFYLElBQXlCLENBQTdCLEVBQWdDO0FBQzlCMUMsa0JBQUFBLHVEQUFBLENBQWdCLFNBQWhCO0FBQ0Q7O0FBRUQsc0JBQUksQ0FBQzRDLFlBQUwsQ0FBa0JILFVBQWxCOztBQVJxQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUF2QztBQVVELE1BRUQ7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7OztXQUVBLHNCQUFheEMsSUFBYixFQUFtQjtBQUFBOztBQUNqQixVQUFNNEMsZ0JBQWdCLEdBQUdkLFFBQVEsQ0FBQ3BCLGFBQVQsQ0FBdUIsZUFBdkIsQ0FBekI7QUFDQSxVQUFNbUMsVUFBVSxHQUFHZixRQUFRLENBQUNwQixhQUFULENBQXVCLHFCQUF2QixDQUFuQjtBQUVBLFVBQU1vQyxTQUFTLEdBQUdoQixRQUFRLENBQUNwQixhQUFULENBQXVCLGFBQXZCLENBQWxCOztBQUVBLFVBQUlWLElBQUksQ0FBQ3lDLFVBQUwsR0FBa0IsQ0FBdEIsRUFBeUI7QUFDdkJLLFFBQUFBLFNBQVMsQ0FBQ2xCLFNBQVYsQ0FBb0JDLEdBQXBCLENBQXdCLFdBQXhCO0FBQ0FpQixRQUFBQSxTQUFTLENBQUNDLFNBQVYsR0FBc0IvQyxJQUFJLENBQUN5QyxVQUEzQjtBQUNELE9BSEQsTUFHTztBQUNMSyxRQUFBQSxTQUFTLENBQUNsQixTQUFWLENBQW9CTyxNQUFwQixDQUEyQixXQUEzQjtBQUNEOztBQUdELFVBQUlVLFVBQUosRUFBZ0I7QUFDZEEsUUFBQUEsVUFBVSxDQUFDRSxTQUFYLEdBQXVCakQsaUVBQWMsQ0FBQ0UsSUFBSSxDQUFDZ0QsV0FBTixFQUFtQixLQUFLeEMsS0FBeEIsQ0FBckM7QUFDRDs7QUFFRCxVQUFNeUMsV0FBVyxHQUFHakQsSUFBSSxDQUFDa0QsS0FBTCxDQUFXQyxHQUFYLENBQWUsVUFBQ0MsSUFBRCxFQUFPQyxLQUFQLEVBQWlCO0FBQ2xELHNJQUU2Q0QsSUFBSSxDQUFDRSxHQUZsRCxtWEFNYUYsSUFBSSxDQUFDRyxHQU5sQixrRUFPZ0J0RCxNQUFNLENBQUNtRCxJQUFJLENBQUNJLGNBQUwsQ0FBb0JELEdBQXJCLEVBQTBCLE9BQTFCLEVBQW1DLFFBQW5DLENBUHRCLHNCQU80RUgsSUFBSSxDQUFDSSxjQUFMLENBQW9CQyxHQVBoRyw0REFRb0NMLElBQUksQ0FBQ00sS0FSekMsOEVBV3dCNUQsaUVBQWMsQ0FBQ3NELElBQUksQ0FBQ08sS0FBTixFQUFhLE1BQUksQ0FBQ25ELEtBQWxCLENBWHRDLCtHQWFpRDRDLElBQUksQ0FBQ1EsRUFidEQsMkJBYXVFUixJQUFJLENBQUNFLEdBYjVFLDBKQWVvREYsSUFBSSxDQUFDUyxRQWZ6RCw0S0FvQkkvRCxpRUFBYyxDQUFDc0QsSUFBSSxDQUFDTyxLQUFMLEdBQWFQLElBQUksQ0FBQ1MsUUFBbkIsRUFBNkIsTUFBSSxDQUFDckQsS0FBbEMsQ0FwQmxCO0FBdUJELE9BeEJtQixDQUFwQjs7QUEwQkMsVUFBSW9DLGdCQUFKLEVBQXNCO0FBQ3JCQSxRQUFBQSxnQkFBZ0IsQ0FBQ0csU0FBakIsR0FBNkJFLFdBQVcsQ0FBQ2EsSUFBWixDQUFpQixFQUFqQixDQUE3QjtBQUNBOztBQUVELFdBQUtDLGNBQUw7QUFDRCxXQUFLQyxZQUFMO0FBQ0Q7OztXQUVELDBCQUFpQjtBQUFBOztBQUNmLFVBQU1DLGVBQWUsR0FBR25DLFFBQVEsQ0FBQ2YsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQXhCO0FBQ0EsVUFBTW1ELGVBQWUsR0FBR3BDLFFBQVEsQ0FBQ2YsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQXhCO0FBR0FtRCxNQUFBQSxlQUFlLENBQUNDLE9BQWhCLENBQXdCLFVBQUNmLElBQUQsRUFBVTtBQUNoQ0EsUUFBQUEsSUFBSSxDQUFDekIsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsWUFBTTtBQUNuQyxjQUFNMkIsR0FBRyxHQUFHRixJQUFJLENBQUNnQixPQUFMLENBQWEsa0JBQWIsRUFBaUNDLE9BQWpDLENBQXlDZixHQUFyRDtBQUNBLGNBQUlPLFFBQVEsR0FBR1MsTUFBTSxDQUFDbEIsSUFBSSxDQUFDbUIsc0JBQUwsQ0FBNEJDLEtBQTdCLENBQU4sR0FBNEMsQ0FBM0Q7QUFHQXhFLFVBQUFBLDREQUFBLENBQWdCc0QsR0FBaEIsRUFBcUI7QUFBRU8sWUFBQUEsUUFBUSxFQUFSQTtBQUFGLFdBQXJCLEVBQ0dhLElBREgsQ0FDUSxVQUFBQyxLQUFLLEVBQUk7QUFDYixrQkFBSSxDQUFDaEMsWUFBTCxDQUFrQmdDLEtBQWxCO0FBQ0QsV0FISCxFQUlHQyxLQUpILENBSVMsVUFBQ0MsS0FBRCxFQUFXO0FBQ2hCQyxZQUFBQSxLQUFLLENBQUMsdUNBQUQsQ0FBTDtBQUNELFdBTkg7QUFRRCxTQWJEO0FBY0QsT0FmRDtBQWlCQWIsTUFBQUEsZUFBZSxDQUFDRSxPQUFoQixDQUF3QixVQUFDZixJQUFELEVBQVU7QUFDaENBLFFBQUFBLElBQUksQ0FBQ3pCLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFlBQU07QUFDbkMsY0FBTTJCLEdBQUcsR0FBR0YsSUFBSSxDQUFDZ0IsT0FBTCxDQUFhLGtCQUFiLEVBQWlDQyxPQUFqQyxDQUF5Q2YsR0FBckQ7QUFDQSxjQUFJTyxRQUFRLEdBQUdTLE1BQU0sQ0FBQ2xCLElBQUksQ0FBQzJCLGtCQUFMLENBQXdCUCxLQUF6QixDQUFOLEdBQXdDLENBQXZEO0FBRUF4RSxVQUFBQSw0REFBQSxDQUFnQnNELEdBQWhCLEVBQXFCO0FBQUVPLFlBQUFBLFFBQVEsRUFBUkE7QUFBRixXQUFyQixFQUNHYSxJQURILENBQ1EsVUFBQUMsS0FBSyxFQUFJO0FBQ2Isa0JBQUksQ0FBQ2hDLFlBQUwsQ0FBa0JnQyxLQUFsQjtBQUNELFdBSEgsRUFJR0MsS0FKSCxDQUlTLFVBQUNDLEtBQUQsRUFBVztBQUNoQkMsWUFBQUEsS0FBSyxDQUFDLHVDQUFELENBQUw7QUFDRCxXQU5IO0FBUUQsU0FaRDtBQWFELE9BZEQ7QUFlRDs7O1dBRUQsd0JBQWU7QUFBQTs7QUFDYixVQUFNRSxZQUFZLEdBQUdsRCxRQUFRLENBQUNmLGdCQUFULENBQTBCLG9CQUExQixDQUFyQjtBQUNBaUUsTUFBQUEsWUFBWSxDQUFDYixPQUFiLENBQXFCLFVBQUNmLElBQUQsRUFBVTtBQUM3QkEsUUFBQUEsSUFBSSxDQUFDekIsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsWUFBTTtBQUNuQzNCLFVBQUFBLDREQUFBLENBQWdCb0QsSUFBSSxDQUFDaUIsT0FBTCxDQUFhZixHQUE3QixFQUFrQ29CLElBQWxDLENBQXVDLFVBQUFDLEtBQUssRUFBSTtBQUM5QyxrQkFBSSxDQUFDaEMsWUFBTCxDQUFrQmdDLEtBQWxCO0FBQ0QsV0FGRDtBQUdELFNBSkQ7QUFLRCxPQU5EO0FBT0Q7OztXQUVELDZCQUFvQjtBQUFBOztBQUNsQixXQUFLMUQsbUJBQUwsQ0FBeUJVLGdCQUF6QixDQUEwQyxPQUExQyxFQUFtRCxZQUFNO0FBQ3ZELGNBQUksQ0FBQ1gsZUFBTCxDQUFxQlksU0FBckIsQ0FBK0JDLEdBQS9CLENBQW1DLFdBQW5DOztBQUVBcUQsUUFBQUEsTUFBTSxDQUFDdkQsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBQ3dELENBQUQsRUFBTztBQUN0QyxjQUFJLENBQUMsTUFBSSxDQUFDbkUsZUFBTCxDQUFxQm9FLFFBQXJCLENBQThCRCxDQUFDLENBQUNFLE1BQWhDLENBQUwsRUFBNkM7QUFDM0Msa0JBQUksQ0FBQ25FLEtBQUwsQ0FBV3NELEtBQVgsR0FBbUIsRUFBbkI7O0FBQ0Esa0JBQUksQ0FBQ3hELGVBQUwsQ0FBcUJZLFNBQXJCLENBQStCTyxNQUEvQixDQUFzQyxXQUF0QztBQUNEO0FBQ0YsU0FMRDtBQU1ELE9BVEQ7QUFVRDs7OzttR0EzTDhCbUQ7O0FBOExqQ0MsY0FBYyxDQUFDQyxNQUFmLENBQXNCLHNCQUF0QixFQUE4Q2pGLGtCQUE5Qzs7Ozs7Ozs7Ozs7O0FDM01BOztBQUVBLElBQU1ULGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQzBFLEtBQUQsRUFBUWhFLEtBQVIsRUFBa0I7QUFDdkMsTUFBTWtGLGNBQWMsR0FBR0Qsb0VBQVcsQ0FBQ25CLE1BQU0sQ0FBQ0UsS0FBRCxDQUFQLEVBQWdCaEUsS0FBSyxDQUFDbUYsV0FBdEIsQ0FBbEM7QUFDQSxNQUFNQyxlQUFlLEdBQUdGLGNBQWMsQ0FBQ0csS0FBZixDQUFxQkgsY0FBYyxDQUFDSSxPQUFmLENBQXVCLEdBQXZCLENBQXJCLENBQXhCOztBQUNBLE1BQUlGLGVBQWUsS0FBSyxLQUF4QixFQUErQjtBQUM3QixXQUFPRixjQUFjLENBQUNHLEtBQWYsQ0FBcUIsQ0FBckIsRUFBd0JILGNBQWMsQ0FBQ0ksT0FBZixDQUF1QixHQUF2QixDQUF4QixDQUFQO0FBQ0Q7O0FBQ0QsU0FBT0osY0FBUDtBQUNELENBUEQ7O0FBU0EsK0RBQWU1RixjQUFmOzs7Ozs7VUNYQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOztVQUVBO1VBQ0E7Ozs7O1dDekJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EsK0JBQStCLHdDQUF3QztXQUN2RTtXQUNBO1dBQ0E7V0FDQTtXQUNBLGlCQUFpQixxQkFBcUI7V0FDdEM7V0FDQTtXQUNBO1dBQ0E7V0FDQSxrQkFBa0IscUJBQXFCO1dBQ3ZDLG9IQUFvSCxpREFBaUQ7V0FDcks7V0FDQSxLQUFLO1dBQ0w7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBOzs7OztXQzdCQTtXQUNBO1dBQ0E7V0FDQSxlQUFlLDRCQUE0QjtXQUMzQyxlQUFlO1dBQ2YsaUNBQWlDLFdBQVc7V0FDNUM7V0FDQTs7Ozs7V0NQQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLHlDQUF5Qyx3Q0FBd0M7V0FDakY7V0FDQTtXQUNBOzs7OztXQ1BBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EsR0FBRztXQUNIO1dBQ0E7V0FDQSxDQUFDOzs7OztXQ1BELDhDQUE4Qzs7Ozs7V0NBOUM7V0FDQTtXQUNBO1dBQ0EsdURBQXVELGlCQUFpQjtXQUN4RTtXQUNBLGdEQUFnRCxhQUFhO1dBQzdEOzs7OztXQ05BOztXQUVBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTs7V0FFQTs7V0FFQTs7V0FFQTs7V0FFQTs7V0FFQTs7V0FFQSw4Q0FBOEM7O1dBRTlDO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQSxpQ0FBaUMsbUNBQW1DO1dBQ3BFO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQSxNQUFNLHFCQUFxQjtXQUMzQjtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBOztXQUVBO1dBQ0E7V0FDQTs7Ozs7VUVsREE7VUFDQTtVQUNBO1VBQ0EscUZBQXFGLHVFQUF1RTtVQUM1SiIsInNvdXJjZXMiOlsid2VicGFjazovL3dvcmtmbG93Ly4vc3JjL3NjcmlwdHMvY29tcG9uZW50cy90aGUtaGVhZGVyLmpzIiwid2VicGFjazovL3dvcmtmbG93Ly4vc3JjL3NjcmlwdHMvdXRpbHMvaHVnTW9uZXlGb3JtYXQuanMiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2NodW5rIGxvYWRlZCIsIndlYnBhY2s6Ly93b3JrZmxvdy93ZWJwYWNrL3J1bnRpbWUvY29tcGF0IGdldCBkZWZhdWx0IGV4cG9ydCIsIndlYnBhY2s6Ly93b3JrZmxvdy93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2dsb2JhbCIsIndlYnBhY2s6Ly93b3JrZmxvdy93ZWJwYWNrL3J1bnRpbWUvaGFzT3duUHJvcGVydHkgc2hvcnRoYW5kIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2pzb25wIGNodW5rIGxvYWRpbmciLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9iZWZvcmUtc3RhcnR1cCIsIndlYnBhY2s6Ly93b3JrZmxvdy93ZWJwYWNrL3N0YXJ0dXAiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9hZnRlci1zdGFydHVwIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBodWdNb25leUZvcm1hdCBmcm9tIFwiLi4vdXRpbHMvaHVnTW9uZXlGb3JtYXRcIjtcclxuaW1wb3J0IE1pY3JvTW9kYWwgZnJvbSBcIm1pY3JvbW9kYWxcIjtcclxuLy8gT25seSBuZWVkIHRvIGltcG9ydCB0aGVzZSBvbmNlXHJcbmltcG9ydCAndW5mZXRjaC9wb2x5ZmlsbCc7XHJcbmltcG9ydCAnZXM2LXByb21pc2UvYXV0byc7XHJcblxyXG4vLyBJbXBvcnQgQHNob3BpZnkvdGhlbWUtY2FydCBhbnl3aGVyZSB5b3UgbmVlZCBpdFxyXG5pbXBvcnQgKiBhcyBjYXJ0IGZyb20gJ0BzaG9waWZ5L3RoZW1lLWNhcnQnO1xyXG5cclxuXHJcbmNvbnN0IGltZ1VSTCA9IChzcmMsIHNpemUsIGNyb3ApID0+IHNyY1xyXG4gIC5yZXBsYWNlKC9fKHBpY298aWNvbnx0aHVtYnxzbWFsbHxjb21wYWN0fG1lZGl1bXxsYXJnZXxncmFuZGV8b3JpZ2luYWx8NTAweDUwMHw3Njh4NzY4fDEwMjR4MTAyNHwyMDQ4eDIwNDh8bWFzdGVyKStcXC4vZywgJy4nKVxyXG4gIC5yZXBsYWNlKC9cXC5qcGd8XFwucG5nfFxcLmdpZnxcXC5qcGVnL2csIChtYXRjaCkgPT4gYF8ke3NpemV9X2Nyb3BfJHtjcm9wfSR7bWF0Y2h9YCk7XHJcbmNsYXNzIFRoZUhlYWRlckNvbXBvbmVudCBleHRlbmRzIEhUTUxFbGVtZW50IHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgICB0aGlzLnRoZW1lID0gdGhlbWU7XHJcblxyXG4gICAgdGhpcy5oZWFkZXJDYXJ0RHJhd2VyID0gdGhpcy5xdWVyeVNlbGVjdG9yKCcuaGVhZGVyX19kcmF3ZXInKTtcclxuXHJcbiAgICB0aGlzLm1lbnVCdG4gPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5wLWJ1dHRvbi0tbWVudScpO1xyXG4gICAgdGhpcy5jbG9zZU1lbnVCdG4gPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5wLWJ1dHRvbi0tY2xvc2UnKTtcclxuICAgIHRoaXMuaGVhZGVyRWwgPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXInKTtcclxuICAgIHRoaXMuZGV0YWlscyA9IHRoaXMucXVlcnlTZWxlY3RvckFsbCgnbGkgPiBkZXRhaWxzJyk7XHJcblxyXG4gICAgdGhpcy5oZWFkZXJTZWFyY2hCYXIgPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXJfX3NlYXJjaC1iYXInKTtcclxuICAgIHRoaXMuaGVhZGVyU2VhcmNoV3JhcHBlciA9IHRoaXMucXVlcnlTZWxlY3RvcignLmhlYWRlcl9fc2VhcmNoLXdyYXBwZXInKTtcclxuICAgIHRoaXMuaW5wdXQgPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXJfX3NlYXJjaC1pbnB1dCcpO1xyXG5cclxuICAgIHRoaXMuY2FydEJ0biA9IHRoaXMucXVlcnlTZWxlY3RvcignLnAtYnV0dG9uLS1jYXJ0Jyk7XHJcblxyXG4gICAgdGhpcy5jdXN0b21lckJ0biA9IHRoaXMucXVlcnlTZWxlY3RvcignLnAtYnV0dG9uLS1jdXN0b21lcicpO1xyXG4gICAgdGhpcy5jdXN0b21lckxpc3QgPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5tZW51LWN1c3RvbWVyX19saXN0Jyk7XHJcbiAgICB0aGlzLmN1c3RvbWVyTmFtZSA9IHRoaXMucXVlcnlTZWxlY3RvcignLmhlYWRlcl9fZHJhd2VyLWxvZ2luJyk7XHJcblxyXG4gICAgdGhpcy5jdXN0b21lck5hbWUgJiYgdGhpcy50b2dnbGVDdXN0b21lck1lbnVFeHBhbmQoKTtcclxuXHJcbiAgICB0aGlzLnRvZ2dsZURyYXdlcigpO1xyXG4gICAgdGhpcy5oYW5kZVNlYXJjaEV4cGFuZCgpO1xyXG5cclxuICAgIHRoaXMuaGFuZGxlU2hvd0NhcnRQb3B1cCgpO1xyXG4gICAgLy8gdGhpcy5jdXN0b21lckJ0biAmJiB0aGlzLmhhbmRsZUN1c3RvbWVyRXhwYW5kKCk7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVEcmF3ZXIoKSB7XHJcbiAgICB0aGlzLm1lbnVCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgIHRoaXMuaGVhZGVyRWwuY2xhc3NMaXN0LmFkZCgnaGVhZGVyLS1leHBhbmQnKTtcclxuICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS53aWR0aCA9ICcxMDAlJztcclxuICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdyA9ICdoaWRkZW4nO1xyXG4gICAgfSlcclxuXHJcbiAgICB0aGlzLmNsb3NlTWVudUJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgICAgdGhpcy5oZWFkZXJFbC5jbGFzc0xpc3QucmVtb3ZlKCdoZWFkZXItLWV4cGFuZCcpXHJcbiAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUud2lkdGggPSAndW5zZXQnO1xyXG4gICAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gJ3Vuc2V0JztcclxuICAgIH0pXHJcbiAgfVxyXG5cclxuICB0b2dnbGVDdXN0b21lck1lbnVFeHBhbmQoKSB7XHJcbiAgICB0aGlzLmN1c3RvbWVyTmFtZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgICAgdGhpcy5oZWFkZXJFbC5jbGFzc0xpc3QudG9nZ2xlKCdpcy1leHBhbmQnKTtcclxuICAgIH0pXHJcbiAgfVxyXG5cclxuICBoYW5kbGVTaG93Q2FydFBvcHVwKCkge1xyXG4gICAgdGhpcy5jYXJ0QnRuLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgYXN5bmMgKCkgPT4ge1xyXG4gICAgICBjb25zdCByZXNwb24gPSBhd2FpdCBmZXRjaCgnL2NhcnQuanMnKTtcclxuICAgICAgY29uc3QgcmVzcG9uSnNvbiA9IGF3YWl0IHJlc3Bvbi5qc29uKCk7XHJcblxyXG4gICAgICBpZiAocmVzcG9uSnNvbi5pdGVtX2NvdW50ID49IDEpIHtcclxuICAgICAgICBNaWNyb01vZGFsLnNob3coJ21vZGFsLTEnKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5nZW5lcmF0ZUNhcnQocmVzcG9uSnNvbik7XHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgLy8gaGFuZGxlQ3VzdG9tZXJFeHBhbmQoKSB7XHJcbiAgLy8gICB0aGlzLmN1c3RvbWVyQnRuLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlb3ZlcicsICgpID0+IHtcclxuICAvLyAgICAgdGhpcy5jdXN0b21lckxpc3QuY2xhc3NMaXN0LmFkZCgnaXMtYWN0aXZlJyk7XHJcbiAgLy8gICB9KTtcclxuXHJcbiAgLy8gICB0aGlzLmN1c3RvbWVyQnRuLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLCAoKSA9PiB7XHJcbiAgLy8gICAgIHRoaXMuY3VzdG9tZXJMaXN0LmNsYXNzTGlzdC5yZW1vdmUoJ2lzLWFjdGl2ZScpO1xyXG4gIC8vICAgfSk7XHJcbiAgLy8gfVxyXG5cclxuICBnZW5lcmF0ZUNhcnQoY2FydCkge1xyXG4gICAgY29uc3QgdGFibGVCb2R5Q29udGVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyN0YWJsZUNvbnRlbnQnKTtcclxuICAgIGNvbnN0IHRvdGFsUHJpY2UgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudG90YWxfX3ByaWNlLWZpbmFsJyk7XHJcblxyXG4gICAgY29uc3QgY2FydENvdW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNhcnQtY291bnQnKTtcclxuXHJcbiAgICBpZiAoY2FydC5pdGVtX2NvdW50ID4gMCkge1xyXG4gICAgICBjYXJ0Q291bnQuY2xhc3NMaXN0LmFkZCgnaXMtYWN0aXZlJyk7XHJcbiAgICAgIGNhcnRDb3VudC5pbm5lckhUTUwgPSBjYXJ0Lml0ZW1fY291bnQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjYXJ0Q291bnQuY2xhc3NMaXN0LnJlbW92ZSgnaXMtYWN0aXZlJyk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGlmICh0b3RhbFByaWNlKSB7XHJcbiAgICAgIHRvdGFsUHJpY2UuaW5uZXJIVE1MID0gaHVnTW9uZXlGb3JtYXQoY2FydC50b3RhbF9wcmljZSwgdGhpcy50aGVtZSk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGNvbnN0IGNhcnRDb250ZW50ID0gY2FydC5pdGVtcy5tYXAoKGl0ZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgIHJldHVybiBgPHRyIGNsYXNzPVwiY2FydC1pdGVtXCI+XHJcbiAgICAgIDx0ZCBjbGFzcz1cInJvdy1pdGVtXCI+XHJcbiAgICAgICAgPHN2ZyBjbGFzcz1cImNhcnQtaXRlbV9fcmVtb3ZlXCIgZGF0YS1rZXk9XCIke2l0ZW0ua2V5fVwiIHN0eWxlPVwiY3Vyc29yOiBwb2ludGVyXCIgd2lkdGg9XCIxNVwiIGhlaWdodD1cIjE1XCIgdmlld0JveD1cIjAgMCAyNSAyNlwiIGZpbGw9XCJub25lXCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiPlxyXG4gICAgICAgICAgPHBhdGggZD1cIk0wLjczNjMyOCAxLjYyNTk4TDIzLjczNjMgMjQuNjI2XCIgc3Ryb2tlPVwiI0E4NkQ1NFwiIHN0cm9rZS13aWR0aD1cIjJcIi8+XHJcbiAgICAgICAgICA8cGF0aCBkPVwiTTIzLjY0NDUgMS43MDgwMUwwLjgxNzg5MyAyNC41MzQ2XCIgc3Ryb2tlPVwiI0E4NkQ1NFwiIHN0cm9rZS13aWR0aD1cIjJcIi8+XHJcbiAgICAgICAgPC9zdmc+XHJcbiAgICAgICAgPGEgaHJlZj1cIiR7aXRlbS51cmx9XCIgY2xhc3M9XCJjYXJ0LWl0ZW1fX2ltYWdlXCI+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIiR7aW1nVVJMKGl0ZW0uZmVhdHVyZWRfaW1hZ2UudXJsLCAnNjB4NjAnLCAnY2VudGVyJyl9XCIgYWx0PVwiJHtpdGVtLmZlYXR1cmVkX2ltYWdlLmFsdH1cIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJ0LWl0ZW1fX3RpdGxlXCI+JHtpdGVtLnRpdGxlfTwvZGl2PlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC90ZD5cclxuICAgICAgPHRkIGNsYXNzPVwicm93LXByaWNlXCI+JHtodWdNb25leUZvcm1hdChpdGVtLnByaWNlLCB0aGlzLnRoZW1lKSB9PC90ZD5cclxuICAgICAgPHRkIGNsYXNzPVwicm93LXF1YW50aXR5XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImpzLWlucHV0LW51bWJlclwiIGRhdGEtdmFyaWFudGlkPVwiJHtpdGVtLmlkfVwiIGRhdGEta2V5PVwiJHtpdGVtLmtleX1cIj5cclxuICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiaW5wdXQtZGVjcmVhc2VtZW50XCI+LTwvYnV0dG9uPlxyXG4gICAgICAgICAgPGlucHV0IGNsYXNzPVwiaW5wdXQtdmFsdWVcIiB0eXBlPVwidGV4dFwiIHZhbHVlPVwiJHtpdGVtLnF1YW50aXR5fVwiIG1pbj1cIjFcIj5cclxuICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiaW5wdXQtaW5jcmVhc2VtZW50XCI+KzwvYnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L3RkPlxyXG4gICAgICA8dGQgY2xhc3M9XCJyb3ctdG90YWxcIj5cclxuICAgICAgICAke2h1Z01vbmV5Rm9ybWF0KGl0ZW0ucHJpY2UgKiBpdGVtLnF1YW50aXR5LCB0aGlzLnRoZW1lKSB9XHJcbiAgICAgIDwvdGQ+XHJcbiAgICA8L3RyPmA7XHJcbiAgICB9KTtcclxuXHJcbiAgICAgaWYgKHRhYmxlQm9keUNvbnRlbnQpIHtcclxuICAgICAgdGFibGVCb2R5Q29udGVudC5pbm5lckhUTUwgPSBjYXJ0Q29udGVudC5qb2luKFwiXCIpXHJcbiAgICAgfVxyXG5cclxuICAgICB0aGlzLmhhbmRsZUluY3JlYXNlKCk7XHJcbiAgICB0aGlzLmhhbmRsZVJlbW92ZSgpO1xyXG4gIH1cclxuXHJcbiAgaGFuZGxlSW5jcmVhc2UoKSB7XHJcbiAgICBjb25zdCBidG5EZWNyZWFzZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuaW5wdXQtZGVjcmVhc2VtZW50Jyk7XHJcbiAgICBjb25zdCBidG5JbmNyZWFzZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuaW5wdXQtaW5jcmVhc2VtZW50Jyk7XHJcblxyXG4gICAgXHJcbiAgICBidG5JbmNyZWFzZW1lbnQuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGtleSA9IGl0ZW0uY2xvc2VzdCgnLmpzLWlucHV0LW51bWJlcicpLmRhdGFzZXQua2V5O1xyXG4gICAgICAgIGxldCBxdWFudGl0eSA9IE51bWJlcihpdGVtLnByZXZpb3VzRWxlbWVudFNpYmxpbmcudmFsdWUpICsgMTtcclxuXHJcblxyXG4gICAgICAgIGNhcnQudXBkYXRlSXRlbShrZXksIHsgcXVhbnRpdHkgfSlcclxuICAgICAgICAgIC50aGVuKHN0YXRlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5nZW5lcmF0ZUNhcnQoc3RhdGUpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgYWxlcnQoJ1NvbGQgb3V0IG9yIGJhZCByZXNwb25zZSBmcm9tIFNob3BpZnknKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgXHJcbiAgICAgIH0pXHJcbiAgICB9KVxyXG5cclxuICAgIGJ0bkRlY3JlYXNlbWVudC5mb3JFYWNoKChpdGVtKSA9PiB7XHJcbiAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgICAgY29uc3Qga2V5ID0gaXRlbS5jbG9zZXN0KCcuanMtaW5wdXQtbnVtYmVyJykuZGF0YXNldC5rZXk7XHJcbiAgICAgICAgbGV0IHF1YW50aXR5ID0gTnVtYmVyKGl0ZW0ubmV4dEVsZW1lbnRTaWJsaW5nLnZhbHVlKSAtIDE7XHJcblxyXG4gICAgICAgIGNhcnQudXBkYXRlSXRlbShrZXksIHsgcXVhbnRpdHkgfSlcclxuICAgICAgICAgIC50aGVuKHN0YXRlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5nZW5lcmF0ZUNhcnQoc3RhdGUpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgYWxlcnQoJ1NvbGQgb3V0IG9yIGJhZCByZXNwb25zZSBmcm9tIFNob3BpZnknKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgXHJcbiAgICAgIH0pXHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgaGFuZGxlUmVtb3ZlKCkge1xyXG4gICAgY29uc3QgcmVtb3ZlQnV0dG9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNhcnQtaXRlbV9fcmVtb3ZlJyk7XHJcbiAgICByZW1vdmVCdXR0b24uZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICAgIGNhcnQucmVtb3ZlSXRlbShpdGVtLmRhdGFzZXQua2V5KS50aGVuKHN0YXRlID0+IHtcclxuICAgICAgICAgIHRoaXMuZ2VuZXJhdGVDYXJ0KHN0YXRlKVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIGhhbmRlU2VhcmNoRXhwYW5kKCkge1xyXG4gICAgdGhpcy5oZWFkZXJTZWFyY2hXcmFwcGVyLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICB0aGlzLmhlYWRlclNlYXJjaEJhci5jbGFzc0xpc3QuYWRkKCdpcy1hY3RpdmUnKTtcclxuXHJcbiAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChlKSA9PiB7ICAgXHJcbiAgICAgICAgaWYgKCF0aGlzLmhlYWRlclNlYXJjaEJhci5jb250YWlucyhlLnRhcmdldCkpe1xyXG4gICAgICAgICAgdGhpcy5pbnB1dC52YWx1ZSA9ICcnO1xyXG4gICAgICAgICAgdGhpcy5oZWFkZXJTZWFyY2hCYXIuY2xhc3NMaXN0LnJlbW92ZSgnaXMtYWN0aXZlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcblxyXG5jdXN0b21FbGVtZW50cy5kZWZpbmUoJ3RoZS1oZWFkZXItY29tcG9uZW50JywgVGhlSGVhZGVyQ29tcG9uZW50KVxyXG4iLCJpbXBvcnQgeyBmb3JtYXRNb25leSB9IGZyb20gJ0BzaG9waWZ5L3RoZW1lLWN1cnJlbmN5JztcclxuXHJcbmNvbnN0IGh1Z01vbmV5Rm9ybWF0ID0gKHZhbHVlLCB0aGVtZSkgPT4ge1xyXG4gIGNvbnN0IGN1cnJlbmN5Rm9ybWF0ID0gZm9ybWF0TW9uZXkoTnVtYmVyKHZhbHVlKSwgdGhlbWUubW9uZXlGb3JtYXQpO1xyXG4gIGNvbnN0IGN1cnJlbmN5RGVjaW1hbCA9IGN1cnJlbmN5Rm9ybWF0LnNsaWNlKGN1cnJlbmN5Rm9ybWF0LmluZGV4T2YoJywnKSk7XHJcbiAgaWYgKGN1cnJlbmN5RGVjaW1hbCA9PT0gJywwMCcpIHtcclxuICAgIHJldHVybiBjdXJyZW5jeUZvcm1hdC5zbGljZSgwLCBjdXJyZW5jeUZvcm1hdC5pbmRleE9mKCcsJykpO1xyXG4gIH1cclxuICByZXR1cm4gY3VycmVuY3lGb3JtYXQ7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBodWdNb25leUZvcm1hdDsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuLy8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbl9fd2VicGFja19yZXF1aXJlX18ubSA9IF9fd2VicGFja19tb2R1bGVzX187XG5cbiIsInZhciBkZWZlcnJlZCA9IFtdO1xuX193ZWJwYWNrX3JlcXVpcmVfXy5PID0gZnVuY3Rpb24ocmVzdWx0LCBjaHVua0lkcywgZm4sIHByaW9yaXR5KSB7XG5cdGlmKGNodW5rSWRzKSB7XG5cdFx0cHJpb3JpdHkgPSBwcmlvcml0eSB8fCAwO1xuXHRcdGZvcih2YXIgaSA9IGRlZmVycmVkLmxlbmd0aDsgaSA+IDAgJiYgZGVmZXJyZWRbaSAtIDFdWzJdID4gcHJpb3JpdHk7IGktLSkgZGVmZXJyZWRbaV0gPSBkZWZlcnJlZFtpIC0gMV07XG5cdFx0ZGVmZXJyZWRbaV0gPSBbY2h1bmtJZHMsIGZuLCBwcmlvcml0eV07XG5cdFx0cmV0dXJuO1xuXHR9XG5cdHZhciBub3RGdWxmaWxsZWQgPSBJbmZpbml0eTtcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZC5sZW5ndGg7IGkrKykge1xuXHRcdHZhciBjaHVua0lkcyA9IGRlZmVycmVkW2ldWzBdO1xuXHRcdHZhciBmbiA9IGRlZmVycmVkW2ldWzFdO1xuXHRcdHZhciBwcmlvcml0eSA9IGRlZmVycmVkW2ldWzJdO1xuXHRcdHZhciBmdWxmaWxsZWQgPSB0cnVlO1xuXHRcdGZvciAodmFyIGogPSAwOyBqIDwgY2h1bmtJZHMubGVuZ3RoOyBqKyspIHtcblx0XHRcdGlmICgocHJpb3JpdHkgJiAxID09PSAwIHx8IG5vdEZ1bGZpbGxlZCA+PSBwcmlvcml0eSkgJiYgT2JqZWN0LmtleXMoX193ZWJwYWNrX3JlcXVpcmVfXy5PKS5ldmVyeShmdW5jdGlvbihrZXkpIHsgcmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18uT1trZXldKGNodW5rSWRzW2pdKTsgfSkpIHtcblx0XHRcdFx0Y2h1bmtJZHMuc3BsaWNlKGotLSwgMSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRmdWxmaWxsZWQgPSBmYWxzZTtcblx0XHRcdFx0aWYocHJpb3JpdHkgPCBub3RGdWxmaWxsZWQpIG5vdEZ1bGZpbGxlZCA9IHByaW9yaXR5O1xuXHRcdFx0fVxuXHRcdH1cblx0XHRpZihmdWxmaWxsZWQpIHtcblx0XHRcdGRlZmVycmVkLnNwbGljZShpLS0sIDEpXG5cdFx0XHR2YXIgciA9IGZuKCk7XG5cdFx0XHRpZiAociAhPT0gdW5kZWZpbmVkKSByZXN1bHQgPSByO1xuXHRcdH1cblx0fVxuXHRyZXR1cm4gcmVzdWx0O1xufTsiLCIvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuX193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG5cdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuXHRcdGZ1bmN0aW9uKCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuXHRcdGZ1bmN0aW9uKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCB7IGE6IGdldHRlciB9KTtcblx0cmV0dXJuIGdldHRlcjtcbn07IiwiLy8gZGVmaW5lIGdldHRlciBmdW5jdGlvbnMgZm9yIGhhcm1vbnkgZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgZGVmaW5pdGlvbikge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLmcgPSAoZnVuY3Rpb24oKSB7XG5cdGlmICh0eXBlb2YgZ2xvYmFsVGhpcyA9PT0gJ29iamVjdCcpIHJldHVybiBnbG9iYWxUaGlzO1xuXHR0cnkge1xuXHRcdHJldHVybiB0aGlzIHx8IG5ldyBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpO1xuXHR9IGNhdGNoIChlKSB7XG5cdFx0aWYgKHR5cGVvZiB3aW5kb3cgPT09ICdvYmplY3QnKSByZXR1cm4gd2luZG93O1xuXHR9XG59KSgpOyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iaiwgcHJvcCkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCk7IH0iLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG5cdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuXHR9XG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG59OyIsIi8vIG5vIGJhc2VVUklcblxuLy8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3Ncbi8vIHVuZGVmaW5lZCA9IGNodW5rIG5vdCBsb2FkZWQsIG51bGwgPSBjaHVuayBwcmVsb2FkZWQvcHJlZmV0Y2hlZFxuLy8gW3Jlc29sdmUsIHJlamVjdCwgUHJvbWlzZV0gPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG52YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuXHRcImNvbXBvbmVudC10aGUtaGVhZGVyXCI6IDBcbn07XG5cbi8vIG5vIGNodW5rIG9uIGRlbWFuZCBsb2FkaW5nXG5cbi8vIG5vIHByZWZldGNoaW5nXG5cbi8vIG5vIHByZWxvYWRlZFxuXG4vLyBubyBITVJcblxuLy8gbm8gSE1SIG1hbmlmZXN0XG5cbl9fd2VicGFja19yZXF1aXJlX18uTy5qID0gZnVuY3Rpb24oY2h1bmtJZCkgeyByZXR1cm4gaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID09PSAwOyB9O1xuXG4vLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbnZhciB3ZWJwYWNrSnNvbnBDYWxsYmFjayA9IGZ1bmN0aW9uKHBhcmVudENodW5rTG9hZGluZ0Z1bmN0aW9uLCBkYXRhKSB7XG5cdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG5cdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG5cdHZhciBydW50aW1lID0gZGF0YVsyXTtcblx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG5cdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuXHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwO1xuXHRpZihjaHVua0lkcy5zb21lKGZ1bmN0aW9uKGlkKSB7IHJldHVybiBpbnN0YWxsZWRDaHVua3NbaWRdICE9PSAwOyB9KSkge1xuXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuXHRcdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcblx0XHRcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcblx0XHRcdH1cblx0XHR9XG5cdFx0aWYocnVudGltZSkgdmFyIHJlc3VsdCA9IHJ1bnRpbWUoX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cdH1cblx0aWYocGFyZW50Q2h1bmtMb2FkaW5nRnVuY3Rpb24pIHBhcmVudENodW5rTG9hZGluZ0Z1bmN0aW9uKGRhdGEpO1xuXHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuXHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcblx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSgpO1xuXHRcdH1cblx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZHNbaV1dID0gMDtcblx0fVxuXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXy5PKHJlc3VsdCk7XG59XG5cbnZhciBjaHVua0xvYWRpbmdHbG9iYWwgPSBzZWxmW1wid2VicGFja0NodW5rd29ya2Zsb3dcIl0gPSBzZWxmW1wid2VicGFja0NodW5rd29ya2Zsb3dcIl0gfHwgW107XG5jaHVua0xvYWRpbmdHbG9iYWwuZm9yRWFjaCh3ZWJwYWNrSnNvbnBDYWxsYmFjay5iaW5kKG51bGwsIDApKTtcbmNodW5rTG9hZGluZ0dsb2JhbC5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2suYmluZChudWxsLCBjaHVua0xvYWRpbmdHbG9iYWwucHVzaC5iaW5kKGNodW5rTG9hZGluZ0dsb2JhbCkpOyIsIiIsIi8vIHN0YXJ0dXBcbi8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuLy8gVGhpcyBlbnRyeSBtb2R1bGUgZGVwZW5kcyBvbiBvdGhlciBsb2FkZWQgY2h1bmtzIGFuZCBleGVjdXRpb24gbmVlZCB0byBiZSBkZWxheWVkXG52YXIgX193ZWJwYWNrX2V4cG9ydHNfXyA9IF9fd2VicGFja19yZXF1aXJlX18uTyh1bmRlZmluZWQsIFtcInZlbmRvcnNcIl0sIGZ1bmN0aW9uKCkgeyByZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhcIi4vc3JjL3NjcmlwdHMvY29tcG9uZW50cy90aGUtaGVhZGVyLmpzXCIpOyB9KVxuX193ZWJwYWNrX2V4cG9ydHNfXyA9IF9fd2VicGFja19yZXF1aXJlX18uTyhfX3dlYnBhY2tfZXhwb3J0c19fKTtcbiIsIiJdLCJuYW1lcyI6WyJodWdNb25leUZvcm1hdCIsIk1pY3JvTW9kYWwiLCJjYXJ0IiwiaW1nVVJMIiwic3JjIiwic2l6ZSIsImNyb3AiLCJyZXBsYWNlIiwibWF0Y2giLCJUaGVIZWFkZXJDb21wb25lbnQiLCJ0aGVtZSIsImhlYWRlckNhcnREcmF3ZXIiLCJxdWVyeVNlbGVjdG9yIiwibWVudUJ0biIsImNsb3NlTWVudUJ0biIsImhlYWRlckVsIiwiZGV0YWlscyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJoZWFkZXJTZWFyY2hCYXIiLCJoZWFkZXJTZWFyY2hXcmFwcGVyIiwiaW5wdXQiLCJjYXJ0QnRuIiwiY3VzdG9tZXJCdG4iLCJjdXN0b21lckxpc3QiLCJjdXN0b21lck5hbWUiLCJ0b2dnbGVDdXN0b21lck1lbnVFeHBhbmQiLCJ0b2dnbGVEcmF3ZXIiLCJoYW5kZVNlYXJjaEV4cGFuZCIsImhhbmRsZVNob3dDYXJ0UG9wdXAiLCJhZGRFdmVudExpc3RlbmVyIiwiY2xhc3NMaXN0IiwiYWRkIiwiZG9jdW1lbnQiLCJib2R5Iiwic3R5bGUiLCJ3aWR0aCIsIm92ZXJmbG93IiwicmVtb3ZlIiwidG9nZ2xlIiwiZmV0Y2giLCJyZXNwb24iLCJqc29uIiwicmVzcG9uSnNvbiIsIml0ZW1fY291bnQiLCJzaG93IiwiZ2VuZXJhdGVDYXJ0IiwidGFibGVCb2R5Q29udGVudCIsInRvdGFsUHJpY2UiLCJjYXJ0Q291bnQiLCJpbm5lckhUTUwiLCJ0b3RhbF9wcmljZSIsImNhcnRDb250ZW50IiwiaXRlbXMiLCJtYXAiLCJpdGVtIiwiaW5kZXgiLCJrZXkiLCJ1cmwiLCJmZWF0dXJlZF9pbWFnZSIsImFsdCIsInRpdGxlIiwicHJpY2UiLCJpZCIsInF1YW50aXR5Iiwiam9pbiIsImhhbmRsZUluY3JlYXNlIiwiaGFuZGxlUmVtb3ZlIiwiYnRuRGVjcmVhc2VtZW50IiwiYnRuSW5jcmVhc2VtZW50IiwiZm9yRWFjaCIsImNsb3Nlc3QiLCJkYXRhc2V0IiwiTnVtYmVyIiwicHJldmlvdXNFbGVtZW50U2libGluZyIsInZhbHVlIiwidXBkYXRlSXRlbSIsInRoZW4iLCJzdGF0ZSIsImNhdGNoIiwiZXJyb3IiLCJhbGVydCIsIm5leHRFbGVtZW50U2libGluZyIsInJlbW92ZUJ1dHRvbiIsInJlbW92ZUl0ZW0iLCJ3aW5kb3ciLCJlIiwiY29udGFpbnMiLCJ0YXJnZXQiLCJIVE1MRWxlbWVudCIsImN1c3RvbUVsZW1lbnRzIiwiZGVmaW5lIiwiZm9ybWF0TW9uZXkiLCJjdXJyZW5jeUZvcm1hdCIsIm1vbmV5Rm9ybWF0IiwiY3VycmVuY3lEZWNpbWFsIiwic2xpY2UiLCJpbmRleE9mIl0sInNvdXJjZVJvb3QiOiIifQ==