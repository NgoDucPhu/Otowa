/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/scripts/components/product-form.js":
/*!************************************************!*\
  !*** ./src/scripts/components/product-form.js ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/wrapNativeSuper */ "./node_modules/@babel/runtime/helpers/esm/wrapNativeSuper.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../utils/hugMoneyFormat */ "./src/scripts/utils/hugMoneyFormat.js");
/* harmony import */ var micromodal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! micromodal */ "./node_modules/micromodal/dist/micromodal.es.js");
/* harmony import */ var unfetch_polyfill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! unfetch/polyfill */ "./node_modules/unfetch/polyfill/index.js");
/* harmony import */ var unfetch_polyfill__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(unfetch_polyfill__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var es6_promise_auto__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! es6-promise/auto */ "./node_modules/es6-promise/auto.js");
/* harmony import */ var es6_promise_auto__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(es6_promise_auto__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @shopify/theme-cart */ "./node_modules/@shopify/theme-cart/theme-cart.js");











function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0,_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0,_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0,_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }


 // Only need to import these once


 // Import @shopify/theme-cart anywhere you need it



var imgURL = function imgURL(src, size, crop) {
  return src.replace(/_(pico|icon|thumb|small|compact|medium|large|grande|original|500x500|768x768|1024x1024|2048x2048|master)+\./g, '.').replace(/\.jpg|\.png|\.gif|\.jpeg/g, function (match) {
    return "_".concat(size, "_crop_").concat(crop).concat(match);
  });
};

var ProductForm = /*#__PURE__*/function (_HTMLElement) {
  (0,_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(ProductForm, _HTMLElement);

  var _super = _createSuper(ProductForm);

  function ProductForm() {
    var _this;

    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ProductForm);

    _this = _super.call(this);

    (0,_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])((0,_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleChangeVariant", function (variantID) {
      var variant = _this.product.variants.find(function (variant) {
        return variant.id === variantID;
      });

      _this.variantSelected = variant;
      var avaiable = _this.variantSelected.avaiable;
      if (!avaiable) _this.addBtn.classList.add('p-button--soldout');

      _this.addBtn.classList.remove('p-button--soldout');
    });

    (0,_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])((0,_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this), "handleAddToCart", /*#__PURE__*/(0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__["default"])( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_9___default().mark(function _callee() {
      var formData, request, res, result;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_9___default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              formData = {
                items: [{
                  id: _this.variantSelected.id,
                  quantity: 1
                }]
              };
              _context.prev = 1;
              _context.next = 4;
              return fetch('/cart/add.js', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
              });

            case 4:
              request = _context.sent;
              _context.next = 7;
              return fetch('/cart.js');

            case 7:
              res = _context.sent;

              if (res.ok) {
                _context.next = 10;
                break;
              }

              throw new Error('Bad response from server');

            case 10:
              _context.next = 12;
              return res.json();

            case 12:
              result = _context.sent;
              micromodal__WEBPACK_IMPORTED_MODULE_11__["default"].show('modal-1');

              _this.generateCart(result);

              _context.next = 20;
              break;

            case 17:
              _context.prev = 17;
              _context.t0 = _context["catch"](1);
              console.log(_context.t0);

            case 20:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[1, 17]]);
    })));

    _this.product = productJson;
    _this.variantSelected = {};
    _this.theme = theme;
    _this.data = cartInit;
    _this.selectVariantsEle = _this.querySelector('.product-page .select__select');
    _this.addBtn = _this.querySelector('.product-page .add-to-cart');
    _this.productPrice = _this.querySelector('.product__detail .price-item--regular');

    _this.init();

    return _this;
  }

  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ProductForm, [{
    key: "init",
    value: function init() {
      this.getVariantSelected();
      this.selectVariantsEle && this.updateVartianSelected();
      this.addToCart();
    }
  }, {
    key: "getVariantSelected",
    value: function getVariantSelected() {
      var variants = this.product.variants;
      this.variantSelected = variants.find(function (item) {
        return item.available === true;
      });
    }
  }, {
    key: "updateVartianSelected",
    value: function updateVartianSelected() {
      var _this2 = this;

      this.selectVariantsEle.addEventListener('change', function (e) {
        _this2.handleChangeVariant(Number(e.target.value));

        _this2.productPrice.innerHTML = (0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_10__["default"])(_this2.variantSelected.price, _this2.theme);
      });
    }
  }, {
    key: "addToCart",
    value: function addToCart() {
      var _this3 = this;

      this.addBtn.addEventListener('click', function (e) {
        e.preventDefault();

        _this3.handleAddToCart();
      });
    }
  }, {
    key: "generateCart",
    value: function generateCart(cart) {
      var _this4 = this;

      var tableBodyContent = document.querySelector('#tableContent');
      var totalPrice = document.querySelector('.total__price-final');
      var cartCount = document.querySelector('.cart-count');

      if (cart.item_count > 0) {
        cartCount.classList.add('is-active');
        cartCount.innerHTML = cart.item_count;
      } else {
        cartCount.classList.remove('is-active');
      }

      if (totalPrice) {
        totalPrice.innerHTML = (0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_10__["default"])(cart.total_price, this.theme);
      }

      var cartContent = cart.items.map(function (item, index) {
        return "<tr class=\"cart-item\">\n      <td class=\"row-item\">\n        <svg class=\"cart-item__remove\" data-key=\"".concat(item.key, "\" style=\"cursor: pointer\" width=\"15\" height=\"15\" viewBox=\"0 0 25 26\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n          <path d=\"M0.736328 1.62598L23.7363 24.626\" stroke=\"#A86D54\" stroke-width=\"2\"/>\n          <path d=\"M23.6445 1.70801L0.817893 24.5346\" stroke=\"#A86D54\" stroke-width=\"2\"/>\n        </svg>\n        <a href=\"").concat(item.url, "\" class=\"cart-item__image\">\n          <img src=\"").concat(imgURL(item.featured_image.url, '60x60', 'center'), "\" alt=\"").concat(item.featured_image.alt, "\">\n          <div class=\"cart-item__title\">").concat(item.title, "</div>\n        </a>\n      </td>\n      <td class=\"row-price\">").concat((0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_10__["default"])(item.price, _this4.theme), "</td>\n      <td class=\"row-quantity\">\n        <div class=\"js-input-number\" data-variantid=\"").concat(item.id, "\" data-key=\"").concat(item.key, "\">\n          <button type=\"button\" class=\"input-decreasement\">-</button>\n          <input class=\"input-value\" type=\"text\" value=\"").concat(item.quantity, "\" min=\"1\">\n          <button type=\"button\" class=\"input-increasement\">+</button>\n        </div>\n      </td>\n      <td class=\"row-total\">\n        ").concat((0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_10__["default"])(item.price * item.quantity, _this4.theme), "\n      </td>\n    </tr>");
      });

      if (tableBodyContent) {
        tableBodyContent.innerHTML = cartContent.join("");
      }

      this.handleIncrease();
      this.handleRemove();
    }
  }, {
    key: "handleIncrease",
    value: function handleIncrease() {
      var _this5 = this;

      var btnDecreasement = document.querySelectorAll('.input-decreasement');
      var btnIncreasement = document.querySelectorAll('.input-increasement');
      btnIncreasement.forEach(function (item) {
        item.addEventListener('click', function () {
          var key = item.closest('.js-input-number').dataset.key;
          var quantity = Number(item.previousElementSibling.value) + 1;
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__.updateItem(key, {
            quantity: quantity
          }).then(function (state) {
            _this5.generateCart(state);
          }).catch(function (error) {
            alert('Sold out or bad response from Shopify');
          });
        });
      });
      btnDecreasement.forEach(function (item) {
        item.addEventListener('click', function () {
          var key = item.closest('.js-input-number').dataset.key;
          var quantity = Number(item.nextElementSibling.value) - 1;
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__.updateItem(key, {
            quantity: quantity
          }).then(function (state) {
            _this5.generateCart(state);
          }).catch(function (error) {
            alert('Sold out or bad response from Shopify');
          });
        });
      });
    }
  }, {
    key: "handleRemove",
    value: function handleRemove() {
      var _this6 = this;

      var removeButton = document.querySelectorAll('.cart-item__remove');
      removeButton.forEach(function (item) {
        item.addEventListener('click', function () {
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__.removeItem(item.dataset.key).then(function (state) {
            _this6.generateCart(state);
          });
        });
      });
    }
  }]);

  return ProductForm;
}( /*#__PURE__*/(0,_babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_7__["default"])(HTMLElement));

customElements.define('product-form', ProductForm);

/***/ }),

/***/ "./src/scripts/utils/hugMoneyFormat.js":
/*!*********************************************!*\
  !*** ./src/scripts/utils/hugMoneyFormat.js ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shopify_theme_currency__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shopify/theme-currency */ "./node_modules/@shopify/theme-currency/currency.js");


var hugMoneyFormat = function hugMoneyFormat(value, theme) {
  var currencyFormat = (0,_shopify_theme_currency__WEBPACK_IMPORTED_MODULE_0__.formatMoney)(Number(value), theme.moneyFormat);
  var currencyDecimal = currencyFormat.slice(currencyFormat.indexOf(','));

  if (currencyDecimal === ',00') {
    return currencyFormat.slice(0, currencyFormat.indexOf(','));
  }

  return currencyFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (hugMoneyFormat);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"component-product-form": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkworkflow"] = self["webpackChunkworkflow"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors"], function() { return __webpack_require__("./src/scripts/components/product-form.js"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LXByb2R1Y3QtZm9ybS5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0NBRUE7O0FBQ0E7Q0FHQTs7QUFDQTs7QUFFQSxJQUFNRyxNQUFNLEdBQUcsU0FBVEEsTUFBUyxDQUFDQyxHQUFELEVBQU1DLElBQU4sRUFBWUMsSUFBWjtBQUFBLFNBQXFCRixHQUFHLENBQ3BDRyxPQURpQyxDQUN6Qiw4R0FEeUIsRUFDdUYsR0FEdkYsRUFFakNBLE9BRmlDLENBRXpCLDJCQUZ5QixFQUVJLFVBQUNDLEtBQUQ7QUFBQSxzQkFBZUgsSUFBZixtQkFBNEJDLElBQTVCLFNBQW1DRSxLQUFuQztBQUFBLEdBRkosQ0FBckI7QUFBQSxDQUFmOztJQUlNQzs7Ozs7QUFDSix5QkFBYztBQUFBOztBQUFBOztBQUNaOztBQURZLDhNQXNDUSxVQUFDQyxTQUFELEVBQWU7QUFDbkMsVUFBTUMsT0FBTyxHQUFHLE1BQUtDLE9BQUwsQ0FBYUMsUUFBYixDQUFzQkMsSUFBdEIsQ0FBMkIsVUFBQ0gsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0ksRUFBUixLQUFlTCxTQUE1QjtBQUFBLE9BQTNCLENBQWhCOztBQUNBLFlBQUtNLGVBQUwsR0FBdUJMLE9BQXZCO0FBQ0EsVUFBT00sUUFBUCxHQUFtQixNQUFLRCxlQUF4QixDQUFPQyxRQUFQO0FBQ0EsVUFBSSxDQUFDQSxRQUFMLEVBQWUsTUFBS0MsTUFBTCxDQUFZQyxTQUFaLENBQXNCQyxHQUF0QixDQUEwQixtQkFBMUI7O0FBQ2YsWUFBS0YsTUFBTCxDQUFZQyxTQUFaLENBQXNCRSxNQUF0QixDQUE2QixtQkFBN0I7QUFDRCxLQTVDYTs7QUFBQSxnWUE4Q0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1ZDLGNBQUFBLFFBRFUsR0FDQztBQUNmQyxnQkFBQUEsS0FBSyxFQUFFLENBQUM7QUFDTlIsa0JBQUFBLEVBQUUsRUFBRSxNQUFLQyxlQUFMLENBQXFCRCxFQURuQjtBQUVOUyxrQkFBQUEsUUFBUSxFQUFFO0FBRkosaUJBQUQ7QUFEUSxlQUREO0FBQUE7QUFBQTtBQUFBLHFCQVNRQyxLQUFLLENBQUMsY0FBRCxFQUFpQjtBQUMxQ0MsZ0JBQUFBLE1BQU0sRUFBRSxNQURrQztBQUUxQ0MsZ0JBQUFBLE9BQU8sRUFBRTtBQUNQLGtDQUFnQjtBQURULGlCQUZpQztBQUsxQ0MsZ0JBQUFBLElBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWVSLFFBQWY7QUFMb0MsZUFBakIsQ0FUYjs7QUFBQTtBQVNSUyxjQUFBQSxPQVRRO0FBQUE7QUFBQSxxQkFpQklOLEtBQUssQ0FBQyxVQUFELENBakJUOztBQUFBO0FBaUJSTyxjQUFBQSxHQWpCUTs7QUFBQSxrQkFrQlRBLEdBQUcsQ0FBQ0MsRUFsQks7QUFBQTtBQUFBO0FBQUE7O0FBQUEsb0JBa0JLLElBQUlDLEtBQUosQ0FBVSwwQkFBVixDQWxCTDs7QUFBQTtBQUFBO0FBQUEscUJBbUJPRixHQUFHLENBQUNHLElBQUosRUFuQlA7O0FBQUE7QUFtQlJDLGNBQUFBLE1BbkJRO0FBcUJkbkMsY0FBQUEsd0RBQUEsQ0FBZ0IsU0FBaEI7O0FBRUEsb0JBQUtxQyxZQUFMLENBQWtCRixNQUFsQjs7QUF2QmM7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUF5QmRHLGNBQUFBLE9BQU8sQ0FBQ0MsR0FBUjs7QUF6QmM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0E5Q0o7O0FBRVosVUFBSzVCLE9BQUwsR0FBZTZCLFdBQWY7QUFDQSxVQUFLekIsZUFBTCxHQUF1QixFQUF2QjtBQUNBLFVBQUswQixLQUFMLEdBQWFBLEtBQWI7QUFDQSxVQUFLQyxJQUFMLEdBQVlDLFFBQVo7QUFFQSxVQUFLQyxpQkFBTCxHQUF5QixNQUFLQyxhQUFMLENBQW1CLCtCQUFuQixDQUF6QjtBQUNBLFVBQUs1QixNQUFMLEdBQWMsTUFBSzRCLGFBQUwsQ0FBbUIsNEJBQW5CLENBQWQ7QUFDQSxVQUFLQyxZQUFMLEdBQW9CLE1BQUtELGFBQUwsQ0FBbUIsdUNBQW5CLENBQXBCOztBQUNBLFVBQUtFLElBQUw7O0FBVlk7QUFXYjs7OztXQUVELGdCQUFPO0FBQ0wsV0FBS0Msa0JBQUw7QUFDQSxXQUFLSixpQkFBTCxJQUEwQixLQUFLSyxxQkFBTCxFQUExQjtBQUNBLFdBQUtDLFNBQUw7QUFDRDs7O1dBRUQsOEJBQXFCO0FBQ25CLFVBQU90QyxRQUFQLEdBQW1CLEtBQUtELE9BQXhCLENBQU9DLFFBQVA7QUFDQSxXQUFLRyxlQUFMLEdBQXVCSCxRQUFRLENBQUNDLElBQVQsQ0FBYyxVQUFDc0MsSUFBRDtBQUFBLGVBQVVBLElBQUksQ0FBQ0MsU0FBTCxLQUFtQixJQUE3QjtBQUFBLE9BQWQsQ0FBdkI7QUFDRDs7O1dBRUQsaUNBQXdCO0FBQUE7O0FBQ3RCLFdBQUtSLGlCQUFMLENBQXVCUyxnQkFBdkIsQ0FBd0MsUUFBeEMsRUFBa0QsVUFBQ0MsQ0FBRCxFQUFPO0FBQ3ZELGNBQUksQ0FBQ0MsbUJBQUwsQ0FBeUJDLE1BQU0sQ0FBQ0YsQ0FBQyxDQUFDRyxNQUFGLENBQVNDLEtBQVYsQ0FBL0I7O0FBQ0EsY0FBSSxDQUFDWixZQUFMLENBQWtCYSxTQUFsQixHQUE4QjVELGtFQUFjLENBQUMsTUFBSSxDQUFDZ0IsZUFBTCxDQUFxQjZDLEtBQXRCLEVBQTZCLE1BQUksQ0FBQ25CLEtBQWxDLENBQTVDO0FBQ0QsT0FIRDtBQUlEOzs7V0FFRCxxQkFBWTtBQUFBOztBQUNWLFdBQUt4QixNQUFMLENBQVlvQyxnQkFBWixDQUE2QixPQUE3QixFQUF1QyxVQUFDQyxDQUFELEVBQU87QUFDN0NBLFFBQUFBLENBQUMsQ0FBQ08sY0FBRjs7QUFDQSxjQUFJLENBQUNDLGVBQUw7QUFDQSxPQUhEO0FBSUQ7OztXQXVDQSxzQkFBYTdELElBQWIsRUFBbUI7QUFBQTs7QUFDbEIsVUFBTThELGdCQUFnQixHQUFHQyxRQUFRLENBQUNuQixhQUFULENBQXVCLGVBQXZCLENBQXpCO0FBQ0EsVUFBTW9CLFVBQVUsR0FBR0QsUUFBUSxDQUFDbkIsYUFBVCxDQUF1QixxQkFBdkIsQ0FBbkI7QUFDQSxVQUFNcUIsU0FBUyxHQUFHRixRQUFRLENBQUNuQixhQUFULENBQXVCLGFBQXZCLENBQWxCOztBQUVBLFVBQUk1QyxJQUFJLENBQUNrRSxVQUFMLEdBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCRCxRQUFBQSxTQUFTLENBQUNoRCxTQUFWLENBQW9CQyxHQUFwQixDQUF3QixXQUF4QjtBQUNBK0MsUUFBQUEsU0FBUyxDQUFDUCxTQUFWLEdBQXNCMUQsSUFBSSxDQUFDa0UsVUFBM0I7QUFDRCxPQUhELE1BR087QUFDTEQsUUFBQUEsU0FBUyxDQUFDaEQsU0FBVixDQUFvQkUsTUFBcEIsQ0FBMkIsV0FBM0I7QUFDRDs7QUFFRCxVQUFJNkMsVUFBSixFQUFnQjtBQUNkQSxRQUFBQSxVQUFVLENBQUNOLFNBQVgsR0FBdUI1RCxrRUFBYyxDQUFDRSxJQUFJLENBQUNtRSxXQUFOLEVBQW1CLEtBQUszQixLQUF4QixDQUFyQztBQUNEOztBQUVELFVBQU00QixXQUFXLEdBQUdwRSxJQUFJLENBQUNxQixLQUFMLENBQVdnRCxHQUFYLENBQWUsVUFBQ25CLElBQUQsRUFBT29CLEtBQVAsRUFBaUI7QUFDbEQsc0lBRTZDcEIsSUFBSSxDQUFDcUIsR0FGbEQsbVhBTWFyQixJQUFJLENBQUNzQixHQU5sQixrRUFPZ0J2RSxNQUFNLENBQUNpRCxJQUFJLENBQUN1QixjQUFMLENBQW9CRCxHQUFyQixFQUEwQixPQUExQixFQUFtQyxRQUFuQyxDQVB0QixzQkFPNEV0QixJQUFJLENBQUN1QixjQUFMLENBQW9CQyxHQVBoRyw0REFRb0N4QixJQUFJLENBQUN5QixLQVJ6Qyw4RUFXd0I3RSxrRUFBYyxDQUFDb0QsSUFBSSxDQUFDUyxLQUFOLEVBQWEsTUFBSSxDQUFDbkIsS0FBbEIsQ0FYdEMsK0dBYWlEVSxJQUFJLENBQUNyQyxFQWJ0RCwyQkFhdUVxQyxJQUFJLENBQUNxQixHQWI1RSwwSkFlb0RyQixJQUFJLENBQUM1QixRQWZ6RCw0S0FvQkl4QixrRUFBYyxDQUFDb0QsSUFBSSxDQUFDUyxLQUFMLEdBQWFULElBQUksQ0FBQzVCLFFBQW5CLEVBQTZCLE1BQUksQ0FBQ2tCLEtBQWxDLENBcEJsQjtBQXVCRCxPQXhCbUIsQ0FBcEI7O0FBMEJDLFVBQUlzQixnQkFBSixFQUFzQjtBQUNyQkEsUUFBQUEsZ0JBQWdCLENBQUNKLFNBQWpCLEdBQTZCVSxXQUFXLENBQUNRLElBQVosQ0FBaUIsRUFBakIsQ0FBN0I7QUFDQTs7QUFFRixXQUFLQyxjQUFMO0FBQ0EsV0FBS0MsWUFBTDtBQUNEOzs7V0FFRCwwQkFBaUI7QUFBQTs7QUFDZixVQUFNQyxlQUFlLEdBQUdoQixRQUFRLENBQUNpQixnQkFBVCxDQUEwQixxQkFBMUIsQ0FBeEI7QUFDQSxVQUFNQyxlQUFlLEdBQUdsQixRQUFRLENBQUNpQixnQkFBVCxDQUEwQixxQkFBMUIsQ0FBeEI7QUFFQUMsTUFBQUEsZUFBZSxDQUFDQyxPQUFoQixDQUF3QixVQUFDaEMsSUFBRCxFQUFVO0FBQ2hDQSxRQUFBQSxJQUFJLENBQUNFLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFlBQU07QUFDbkMsY0FBTW1CLEdBQUcsR0FBR3JCLElBQUksQ0FBQ2lDLE9BQUwsQ0FBYSxrQkFBYixFQUFpQ0MsT0FBakMsQ0FBeUNiLEdBQXJEO0FBQ0EsY0FBSWpELFFBQVEsR0FBR2lDLE1BQU0sQ0FBQ0wsSUFBSSxDQUFDbUMsc0JBQUwsQ0FBNEI1QixLQUE3QixDQUFOLEdBQTRDLENBQTNEO0FBRUF6RCxVQUFBQSw0REFBQSxDQUFnQnVFLEdBQWhCLEVBQXFCO0FBQUVqRCxZQUFBQSxRQUFRLEVBQVJBO0FBQUYsV0FBckIsRUFDR2lFLElBREgsQ0FDUSxVQUFBQyxLQUFLLEVBQUk7QUFDYixrQkFBSSxDQUFDcEQsWUFBTCxDQUFrQm9ELEtBQWxCO0FBQ0QsV0FISCxFQUlHQyxLQUpILENBSVMsVUFBQ0MsS0FBRCxFQUFXO0FBQ2hCQyxZQUFBQSxLQUFLLENBQUMsdUNBQUQsQ0FBTDtBQUNELFdBTkg7QUFPRCxTQVhEO0FBWUQsT0FiRDtBQWVBWixNQUFBQSxlQUFlLENBQUNHLE9BQWhCLENBQXdCLFVBQUNoQyxJQUFELEVBQVU7QUFDaENBLFFBQUFBLElBQUksQ0FBQ0UsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsWUFBTTtBQUNuQyxjQUFNbUIsR0FBRyxHQUFHckIsSUFBSSxDQUFDaUMsT0FBTCxDQUFhLGtCQUFiLEVBQWlDQyxPQUFqQyxDQUF5Q2IsR0FBckQ7QUFDQSxjQUFJakQsUUFBUSxHQUFHaUMsTUFBTSxDQUFDTCxJQUFJLENBQUMwQyxrQkFBTCxDQUF3Qm5DLEtBQXpCLENBQU4sR0FBd0MsQ0FBdkQ7QUFFQXpELFVBQUFBLDREQUFBLENBQWdCdUUsR0FBaEIsRUFBcUI7QUFBRWpELFlBQUFBLFFBQVEsRUFBUkE7QUFBRixXQUFyQixFQUNHaUUsSUFESCxDQUNRLFVBQUFDLEtBQUssRUFBSTtBQUNiLGtCQUFJLENBQUNwRCxZQUFMLENBQWtCb0QsS0FBbEI7QUFDRCxXQUhILEVBSUdDLEtBSkgsQ0FJUyxVQUFDQyxLQUFELEVBQVc7QUFDaEJDLFlBQUFBLEtBQUssQ0FBQyx1Q0FBRCxDQUFMO0FBQ0QsV0FOSDtBQVFELFNBWkQ7QUFhRCxPQWREO0FBZUQ7OztXQUVELHdCQUFlO0FBQUE7O0FBQ2IsVUFBTUUsWUFBWSxHQUFHOUIsUUFBUSxDQUFDaUIsZ0JBQVQsQ0FBMEIsb0JBQTFCLENBQXJCO0FBQ0FhLE1BQUFBLFlBQVksQ0FBQ1gsT0FBYixDQUFxQixVQUFDaEMsSUFBRCxFQUFVO0FBQzdCQSxRQUFBQSxJQUFJLENBQUNFLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFlBQU07QUFDbkNwRCxVQUFBQSw0REFBQSxDQUFnQmtELElBQUksQ0FBQ2tDLE9BQUwsQ0FBYWIsR0FBN0IsRUFBa0NnQixJQUFsQyxDQUF1QyxVQUFBQyxLQUFLLEVBQUk7QUFDOUMsa0JBQUksQ0FBQ3BELFlBQUwsQ0FBa0JvRCxLQUFsQjtBQUNELFdBRkQ7QUFHRCxTQUpEO0FBS0QsT0FORDtBQU9EOzs7O21HQTNLdUJPOztBQThLMUJDLGNBQWMsQ0FBQ0MsTUFBZixDQUFzQixjQUF0QixFQUFzQzFGLFdBQXRDOzs7Ozs7Ozs7Ozs7QUMzTEE7O0FBRUEsSUFBTVQsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDMkQsS0FBRCxFQUFRakIsS0FBUixFQUFrQjtBQUN2QyxNQUFNMkQsY0FBYyxHQUFHRCxvRUFBVyxDQUFDM0MsTUFBTSxDQUFDRSxLQUFELENBQVAsRUFBZ0JqQixLQUFLLENBQUM0RCxXQUF0QixDQUFsQztBQUNBLE1BQU1DLGVBQWUsR0FBR0YsY0FBYyxDQUFDRyxLQUFmLENBQXFCSCxjQUFjLENBQUNJLE9BQWYsQ0FBdUIsR0FBdkIsQ0FBckIsQ0FBeEI7O0FBQ0EsTUFBSUYsZUFBZSxLQUFLLEtBQXhCLEVBQStCO0FBQzdCLFdBQU9GLGNBQWMsQ0FBQ0csS0FBZixDQUFxQixDQUFyQixFQUF3QkgsY0FBYyxDQUFDSSxPQUFmLENBQXVCLEdBQXZCLENBQXhCLENBQVA7QUFDRDs7QUFDRCxTQUFPSixjQUFQO0FBQ0QsQ0FQRDs7QUFTQSwrREFBZXJHLGNBQWY7Ozs7OztVQ1hBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7Ozs7V0N6QkE7V0FDQTtXQUNBO1dBQ0E7V0FDQSwrQkFBK0Isd0NBQXdDO1dBQ3ZFO1dBQ0E7V0FDQTtXQUNBO1dBQ0EsaUJBQWlCLHFCQUFxQjtXQUN0QztXQUNBO1dBQ0E7V0FDQTtXQUNBLGtCQUFrQixxQkFBcUI7V0FDdkMsb0hBQW9ILGlEQUFpRDtXQUNySztXQUNBLEtBQUs7V0FDTDtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7Ozs7O1dDN0JBO1dBQ0E7V0FDQTtXQUNBLGVBQWUsNEJBQTRCO1dBQzNDLGVBQWU7V0FDZixpQ0FBaUMsV0FBVztXQUM1QztXQUNBOzs7OztXQ1BBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EseUNBQXlDLHdDQUF3QztXQUNqRjtXQUNBO1dBQ0E7Ozs7O1dDUEE7V0FDQTtXQUNBO1dBQ0E7V0FDQSxHQUFHO1dBQ0g7V0FDQTtXQUNBLENBQUM7Ozs7O1dDUEQsOENBQThDOzs7OztXQ0E5QztXQUNBO1dBQ0E7V0FDQSx1REFBdUQsaUJBQWlCO1dBQ3hFO1dBQ0EsZ0RBQWdELGFBQWE7V0FDN0Q7Ozs7O1dDTkE7O1dBRUE7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBOztXQUVBOztXQUVBOztXQUVBOztXQUVBOztXQUVBOztXQUVBLDhDQUE4Qzs7V0FFOUM7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLGlDQUFpQyxtQ0FBbUM7V0FDcEU7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLE1BQU0scUJBQXFCO1dBQzNCO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7O1dBRUE7V0FDQTtXQUNBOzs7OztVRWxEQTtVQUNBO1VBQ0E7VUFDQSxxRkFBcUYseUVBQXlFO1VBQzlKIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vd29ya2Zsb3cvLi9zcmMvc2NyaXB0cy9jb21wb25lbnRzL3Byb2R1Y3QtZm9ybS5qcyIsIndlYnBhY2s6Ly93b3JrZmxvdy8uL3NyYy9zY3JpcHRzL3V0aWxzL2h1Z01vbmV5Rm9ybWF0LmpzIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svcnVudGltZS9jaHVuayBsb2FkZWQiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2NvbXBhdCBnZXQgZGVmYXVsdCBleHBvcnQiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2RlZmluZSBwcm9wZXJ0eSBnZXR0ZXJzIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svcnVudGltZS9nbG9iYWwiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly93b3JrZmxvdy93ZWJwYWNrL3J1bnRpbWUvbWFrZSBuYW1lc3BhY2Ugb2JqZWN0Iiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svcnVudGltZS9qc29ucCBjaHVuayBsb2FkaW5nIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svYmVmb3JlLXN0YXJ0dXAiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9zdGFydHVwIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svYWZ0ZXItc3RhcnR1cCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaHVnTW9uZXlGb3JtYXQgZnJvbSBcIi4uL3V0aWxzL2h1Z01vbmV5Rm9ybWF0XCI7XHJcbmltcG9ydCBNaWNyb01vZGFsIGZyb20gXCJtaWNyb21vZGFsXCI7XHJcbi8vIE9ubHkgbmVlZCB0byBpbXBvcnQgdGhlc2Ugb25jZVxyXG5pbXBvcnQgJ3VuZmV0Y2gvcG9seWZpbGwnO1xyXG5pbXBvcnQgJ2VzNi1wcm9taXNlL2F1dG8nO1xyXG5cclxuLy8gSW1wb3J0IEBzaG9waWZ5L3RoZW1lLWNhcnQgYW55d2hlcmUgeW91IG5lZWQgaXRcclxuaW1wb3J0ICogYXMgY2FydCBmcm9tICdAc2hvcGlmeS90aGVtZS1jYXJ0JztcclxuXHJcbmNvbnN0IGltZ1VSTCA9IChzcmMsIHNpemUsIGNyb3ApID0+IHNyY1xyXG4gIC5yZXBsYWNlKC9fKHBpY298aWNvbnx0aHVtYnxzbWFsbHxjb21wYWN0fG1lZGl1bXxsYXJnZXxncmFuZGV8b3JpZ2luYWx8NTAweDUwMHw3Njh4NzY4fDEwMjR4MTAyNHwyMDQ4eDIwNDh8bWFzdGVyKStcXC4vZywgJy4nKVxyXG4gIC5yZXBsYWNlKC9cXC5qcGd8XFwucG5nfFxcLmdpZnxcXC5qcGVnL2csIChtYXRjaCkgPT4gYF8ke3NpemV9X2Nyb3BfJHtjcm9wfSR7bWF0Y2h9YCk7XHJcblxyXG5jbGFzcyBQcm9kdWN0Rm9ybSBleHRlbmRzIEhUTUxFbGVtZW50IHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgICB0aGlzLnByb2R1Y3QgPSBwcm9kdWN0SnNvbjtcclxuICAgIHRoaXMudmFyaWFudFNlbGVjdGVkID0ge307XHJcbiAgICB0aGlzLnRoZW1lID0gdGhlbWU7XHJcbiAgICB0aGlzLmRhdGEgPSBjYXJ0SW5pdDtcclxuXHJcbiAgICB0aGlzLnNlbGVjdFZhcmlhbnRzRWxlID0gdGhpcy5xdWVyeVNlbGVjdG9yKCcucHJvZHVjdC1wYWdlIC5zZWxlY3RfX3NlbGVjdCcpOyBcclxuICAgIHRoaXMuYWRkQnRuID0gdGhpcy5xdWVyeVNlbGVjdG9yKCcucHJvZHVjdC1wYWdlIC5hZGQtdG8tY2FydCcpO1xyXG4gICAgdGhpcy5wcm9kdWN0UHJpY2UgPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5wcm9kdWN0X19kZXRhaWwgLnByaWNlLWl0ZW0tLXJlZ3VsYXInKTtcclxuICAgIHRoaXMuaW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgaW5pdCgpIHtcclxuICAgIHRoaXMuZ2V0VmFyaWFudFNlbGVjdGVkKCk7XHJcbiAgICB0aGlzLnNlbGVjdFZhcmlhbnRzRWxlICYmIHRoaXMudXBkYXRlVmFydGlhblNlbGVjdGVkKCk7XHJcbiAgICB0aGlzLmFkZFRvQ2FydCgpO1xyXG4gIH1cclxuXHJcbiAgZ2V0VmFyaWFudFNlbGVjdGVkKCkge1xyXG4gICAgY29uc3Qge3ZhcmlhbnRzfSA9IHRoaXMucHJvZHVjdDtcclxuICAgIHRoaXMudmFyaWFudFNlbGVjdGVkID0gdmFyaWFudHMuZmluZCgoaXRlbSkgPT4gaXRlbS5hdmFpbGFibGUgPT09IHRydWUpO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlVmFydGlhblNlbGVjdGVkKCkgeyBcclxuICAgIHRoaXMuc2VsZWN0VmFyaWFudHNFbGUuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgKGUpID0+IHtcclxuICAgICAgdGhpcy5oYW5kbGVDaGFuZ2VWYXJpYW50KE51bWJlcihlLnRhcmdldC52YWx1ZSkpXHJcbiAgICAgIHRoaXMucHJvZHVjdFByaWNlLmlubmVySFRNTCA9IGh1Z01vbmV5Rm9ybWF0KHRoaXMudmFyaWFudFNlbGVjdGVkLnByaWNlLCB0aGlzLnRoZW1lKTtcclxuICAgIH0pXHJcbiAgfVxyXG5cclxuICBhZGRUb0NhcnQoKSB7XHJcbiAgICB0aGlzLmFkZEJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICAoZSkgPT4ge1xyXG4gICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICB0aGlzLmhhbmRsZUFkZFRvQ2FydCgpO1xyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIGhhbmRsZUNoYW5nZVZhcmlhbnQgPSAodmFyaWFudElEKSA9PiB7XHJcbiAgICBjb25zdCB2YXJpYW50ID0gdGhpcy5wcm9kdWN0LnZhcmlhbnRzLmZpbmQoKHZhcmlhbnQpID0+IHZhcmlhbnQuaWQgPT09IHZhcmlhbnRJRCk7XHJcbiAgICB0aGlzLnZhcmlhbnRTZWxlY3RlZCA9IHZhcmlhbnQ7XHJcbiAgICBjb25zdCB7YXZhaWFibGV9ID0gdGhpcy52YXJpYW50U2VsZWN0ZWQ7XHJcbiAgICBpZiAoIWF2YWlhYmxlKSB0aGlzLmFkZEJ0bi5jbGFzc0xpc3QuYWRkKCdwLWJ1dHRvbi0tc29sZG91dCcpO1xyXG4gICAgdGhpcy5hZGRCdG4uY2xhc3NMaXN0LnJlbW92ZSgncC1idXR0b24tLXNvbGRvdXQnKTtcclxuICB9O1xyXG5cclxuICBoYW5kbGVBZGRUb0NhcnQgPSBhc3luYyAoKSA9PiB7XHJcbiAgICBjb25zdCBmb3JtRGF0YSA9IHtcclxuICAgICAgaXRlbXM6IFt7XHJcbiAgICAgICAgaWQ6IHRoaXMudmFyaWFudFNlbGVjdGVkLmlkLFxyXG4gICAgICAgIHF1YW50aXR5OiAxLFxyXG4gICAgICB9XSxcclxuICAgIH07XHJcbiAgXHJcbiAgICB0cnkge1xyXG4gICAgICBjb25zdCByZXF1ZXN0ID0gYXdhaXQgZmV0Y2goJy9jYXJ0L2FkZC5qcycsIHtcclxuICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoZm9ybURhdGEpLFxyXG4gICAgICB9KTtcclxuICAgICAgXHJcbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKCcvY2FydC5qcycpO1xyXG4gICAgICBpZiAoIXJlcy5vaykgdGhyb3cgbmV3IEVycm9yKCdCYWQgcmVzcG9uc2UgZnJvbSBzZXJ2ZXInKTtcclxuICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgcmVzLmpzb24oKTtcclxuICAgICAgXHJcbiAgICAgIE1pY3JvTW9kYWwuc2hvdygnbW9kYWwtMScpO1xyXG5cclxuICAgICAgdGhpcy5nZW5lcmF0ZUNhcnQocmVzdWx0KTtcclxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICAgZ2VuZXJhdGVDYXJ0KGNhcnQpIHtcclxuICAgIGNvbnN0IHRhYmxlQm9keUNvbnRlbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjdGFibGVDb250ZW50Jyk7XHJcbiAgICBjb25zdCB0b3RhbFByaWNlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnRvdGFsX19wcmljZS1maW5hbCcpO1xyXG4gICAgY29uc3QgY2FydENvdW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNhcnQtY291bnQnKTtcclxuXHJcbiAgICBpZiAoY2FydC5pdGVtX2NvdW50ID4gMCkge1xyXG4gICAgICBjYXJ0Q291bnQuY2xhc3NMaXN0LmFkZCgnaXMtYWN0aXZlJyk7XHJcbiAgICAgIGNhcnRDb3VudC5pbm5lckhUTUwgPSBjYXJ0Lml0ZW1fY291bnQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjYXJ0Q291bnQuY2xhc3NMaXN0LnJlbW92ZSgnaXMtYWN0aXZlJyk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRvdGFsUHJpY2UpIHtcclxuICAgICAgdG90YWxQcmljZS5pbm5lckhUTUwgPSBodWdNb25leUZvcm1hdChjYXJ0LnRvdGFsX3ByaWNlLCB0aGlzLnRoZW1lKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgY29uc3QgY2FydENvbnRlbnQgPSBjYXJ0Lml0ZW1zLm1hcCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgcmV0dXJuIGA8dHIgY2xhc3M9XCJjYXJ0LWl0ZW1cIj5cclxuICAgICAgPHRkIGNsYXNzPVwicm93LWl0ZW1cIj5cclxuICAgICAgICA8c3ZnIGNsYXNzPVwiY2FydC1pdGVtX19yZW1vdmVcIiBkYXRhLWtleT1cIiR7aXRlbS5rZXl9XCIgc3R5bGU9XCJjdXJzb3I6IHBvaW50ZXJcIiB3aWR0aD1cIjE1XCIgaGVpZ2h0PVwiMTVcIiB2aWV3Qm94PVwiMCAwIDI1IDI2XCIgZmlsbD1cIm5vbmVcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+XHJcbiAgICAgICAgICA8cGF0aCBkPVwiTTAuNzM2MzI4IDEuNjI1OThMMjMuNzM2MyAyNC42MjZcIiBzdHJva2U9XCIjQTg2RDU0XCIgc3Ryb2tlLXdpZHRoPVwiMlwiLz5cclxuICAgICAgICAgIDxwYXRoIGQ9XCJNMjMuNjQ0NSAxLjcwODAxTDAuODE3ODkzIDI0LjUzNDZcIiBzdHJva2U9XCIjQTg2RDU0XCIgc3Ryb2tlLXdpZHRoPVwiMlwiLz5cclxuICAgICAgICA8L3N2Zz5cclxuICAgICAgICA8YSBocmVmPVwiJHtpdGVtLnVybH1cIiBjbGFzcz1cImNhcnQtaXRlbV9faW1hZ2VcIj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiJHtpbWdVUkwoaXRlbS5mZWF0dXJlZF9pbWFnZS51cmwsICc2MHg2MCcsICdjZW50ZXInKX1cIiBhbHQ9XCIke2l0ZW0uZmVhdHVyZWRfaW1hZ2UuYWx0fVwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNhcnQtaXRlbV9fdGl0bGVcIj4ke2l0ZW0udGl0bGV9PC9kaXY+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICA8L3RkPlxyXG4gICAgICA8dGQgY2xhc3M9XCJyb3ctcHJpY2VcIj4ke2h1Z01vbmV5Rm9ybWF0KGl0ZW0ucHJpY2UsIHRoaXMudGhlbWUpIH08L3RkPlxyXG4gICAgICA8dGQgY2xhc3M9XCJyb3ctcXVhbnRpdHlcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwianMtaW5wdXQtbnVtYmVyXCIgZGF0YS12YXJpYW50aWQ9XCIke2l0ZW0uaWR9XCIgZGF0YS1rZXk9XCIke2l0ZW0ua2V5fVwiPlxyXG4gICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJpbnB1dC1kZWNyZWFzZW1lbnRcIj4tPC9idXR0b24+XHJcbiAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJpbnB1dC12YWx1ZVwiIHR5cGU9XCJ0ZXh0XCIgdmFsdWU9XCIke2l0ZW0ucXVhbnRpdHl9XCIgbWluPVwiMVwiPlxyXG4gICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJpbnB1dC1pbmNyZWFzZW1lbnRcIj4rPC9idXR0b24+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvdGQ+XHJcbiAgICAgIDx0ZCBjbGFzcz1cInJvdy10b3RhbFwiPlxyXG4gICAgICAgICR7aHVnTW9uZXlGb3JtYXQoaXRlbS5wcmljZSAqIGl0ZW0ucXVhbnRpdHksIHRoaXMudGhlbWUpIH1cclxuICAgICAgPC90ZD5cclxuICAgIDwvdHI+YDtcclxuICAgIH0pO1xyXG5cclxuICAgICBpZiAodGFibGVCb2R5Q29udGVudCkge1xyXG4gICAgICB0YWJsZUJvZHlDb250ZW50LmlubmVySFRNTCA9IGNhcnRDb250ZW50LmpvaW4oXCJcIilcclxuICAgICB9XHJcblxyXG4gICAgdGhpcy5oYW5kbGVJbmNyZWFzZSgpO1xyXG4gICAgdGhpcy5oYW5kbGVSZW1vdmUoKTtcclxuICB9XHJcblxyXG4gIGhhbmRsZUluY3JlYXNlKCkge1xyXG4gICAgY29uc3QgYnRuRGVjcmVhc2VtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmlucHV0LWRlY3JlYXNlbWVudCcpO1xyXG4gICAgY29uc3QgYnRuSW5jcmVhc2VtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmlucHV0LWluY3JlYXNlbWVudCcpO1xyXG5cclxuICAgIGJ0bkluY3JlYXNlbWVudC5mb3JFYWNoKChpdGVtKSA9PiB7XHJcbiAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgICAgY29uc3Qga2V5ID0gaXRlbS5jbG9zZXN0KCcuanMtaW5wdXQtbnVtYmVyJykuZGF0YXNldC5rZXk7XHJcbiAgICAgICAgbGV0IHF1YW50aXR5ID0gTnVtYmVyKGl0ZW0ucHJldmlvdXNFbGVtZW50U2libGluZy52YWx1ZSkgKyAxO1xyXG5cclxuICAgICAgICBjYXJ0LnVwZGF0ZUl0ZW0oa2V5LCB7IHF1YW50aXR5IH0pXHJcbiAgICAgICAgICAudGhlbihzdGF0ZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVDYXJ0KHN0YXRlKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGFsZXJ0KCdTb2xkIG91dCBvciBiYWQgcmVzcG9uc2UgZnJvbSBTaG9waWZ5JylcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9KVxyXG4gICAgfSlcclxuXHJcbiAgICBidG5EZWNyZWFzZW1lbnQuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGtleSA9IGl0ZW0uY2xvc2VzdCgnLmpzLWlucHV0LW51bWJlcicpLmRhdGFzZXQua2V5O1xyXG4gICAgICAgIGxldCBxdWFudGl0eSA9IE51bWJlcihpdGVtLm5leHRFbGVtZW50U2libGluZy52YWx1ZSkgLSAxO1xyXG5cclxuICAgICAgICBjYXJ0LnVwZGF0ZUl0ZW0oa2V5LCB7IHF1YW50aXR5IH0pXHJcbiAgICAgICAgICAudGhlbihzdGF0ZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVDYXJ0KHN0YXRlKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGFsZXJ0KCdTb2xkIG91dCBvciBiYWQgcmVzcG9uc2UgZnJvbSBTaG9waWZ5JylcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIGhhbmRsZVJlbW92ZSgpIHtcclxuICAgIGNvbnN0IHJlbW92ZUJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jYXJ0LWl0ZW1fX3JlbW92ZScpO1xyXG4gICAgcmVtb3ZlQnV0dG9uLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgICAgICBjYXJ0LnJlbW92ZUl0ZW0oaXRlbS5kYXRhc2V0LmtleSkudGhlbihzdGF0ZSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmdlbmVyYXRlQ2FydChzdGF0ZSlcclxuICAgICAgICB9KTtcclxuICAgICAgfSlcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcblxyXG5jdXN0b21FbGVtZW50cy5kZWZpbmUoJ3Byb2R1Y3QtZm9ybScsIFByb2R1Y3RGb3JtKTsiLCJpbXBvcnQgeyBmb3JtYXRNb25leSB9IGZyb20gJ0BzaG9waWZ5L3RoZW1lLWN1cnJlbmN5JztcclxuXHJcbmNvbnN0IGh1Z01vbmV5Rm9ybWF0ID0gKHZhbHVlLCB0aGVtZSkgPT4ge1xyXG4gIGNvbnN0IGN1cnJlbmN5Rm9ybWF0ID0gZm9ybWF0TW9uZXkoTnVtYmVyKHZhbHVlKSwgdGhlbWUubW9uZXlGb3JtYXQpO1xyXG4gIGNvbnN0IGN1cnJlbmN5RGVjaW1hbCA9IGN1cnJlbmN5Rm9ybWF0LnNsaWNlKGN1cnJlbmN5Rm9ybWF0LmluZGV4T2YoJywnKSk7XHJcbiAgaWYgKGN1cnJlbmN5RGVjaW1hbCA9PT0gJywwMCcpIHtcclxuICAgIHJldHVybiBjdXJyZW5jeUZvcm1hdC5zbGljZSgwLCBjdXJyZW5jeUZvcm1hdC5pbmRleE9mKCcsJykpO1xyXG4gIH1cclxuICByZXR1cm4gY3VycmVuY3lGb3JtYXQ7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBodWdNb25leUZvcm1hdDsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuLy8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbl9fd2VicGFja19yZXF1aXJlX18ubSA9IF9fd2VicGFja19tb2R1bGVzX187XG5cbiIsInZhciBkZWZlcnJlZCA9IFtdO1xuX193ZWJwYWNrX3JlcXVpcmVfXy5PID0gZnVuY3Rpb24ocmVzdWx0LCBjaHVua0lkcywgZm4sIHByaW9yaXR5KSB7XG5cdGlmKGNodW5rSWRzKSB7XG5cdFx0cHJpb3JpdHkgPSBwcmlvcml0eSB8fCAwO1xuXHRcdGZvcih2YXIgaSA9IGRlZmVycmVkLmxlbmd0aDsgaSA+IDAgJiYgZGVmZXJyZWRbaSAtIDFdWzJdID4gcHJpb3JpdHk7IGktLSkgZGVmZXJyZWRbaV0gPSBkZWZlcnJlZFtpIC0gMV07XG5cdFx0ZGVmZXJyZWRbaV0gPSBbY2h1bmtJZHMsIGZuLCBwcmlvcml0eV07XG5cdFx0cmV0dXJuO1xuXHR9XG5cdHZhciBub3RGdWxmaWxsZWQgPSBJbmZpbml0eTtcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZC5sZW5ndGg7IGkrKykge1xuXHRcdHZhciBjaHVua0lkcyA9IGRlZmVycmVkW2ldWzBdO1xuXHRcdHZhciBmbiA9IGRlZmVycmVkW2ldWzFdO1xuXHRcdHZhciBwcmlvcml0eSA9IGRlZmVycmVkW2ldWzJdO1xuXHRcdHZhciBmdWxmaWxsZWQgPSB0cnVlO1xuXHRcdGZvciAodmFyIGogPSAwOyBqIDwgY2h1bmtJZHMubGVuZ3RoOyBqKyspIHtcblx0XHRcdGlmICgocHJpb3JpdHkgJiAxID09PSAwIHx8IG5vdEZ1bGZpbGxlZCA+PSBwcmlvcml0eSkgJiYgT2JqZWN0LmtleXMoX193ZWJwYWNrX3JlcXVpcmVfXy5PKS5ldmVyeShmdW5jdGlvbihrZXkpIHsgcmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18uT1trZXldKGNodW5rSWRzW2pdKTsgfSkpIHtcblx0XHRcdFx0Y2h1bmtJZHMuc3BsaWNlKGotLSwgMSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRmdWxmaWxsZWQgPSBmYWxzZTtcblx0XHRcdFx0aWYocHJpb3JpdHkgPCBub3RGdWxmaWxsZWQpIG5vdEZ1bGZpbGxlZCA9IHByaW9yaXR5O1xuXHRcdFx0fVxuXHRcdH1cblx0XHRpZihmdWxmaWxsZWQpIHtcblx0XHRcdGRlZmVycmVkLnNwbGljZShpLS0sIDEpXG5cdFx0XHR2YXIgciA9IGZuKCk7XG5cdFx0XHRpZiAociAhPT0gdW5kZWZpbmVkKSByZXN1bHQgPSByO1xuXHRcdH1cblx0fVxuXHRyZXR1cm4gcmVzdWx0O1xufTsiLCIvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuX193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG5cdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuXHRcdGZ1bmN0aW9uKCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuXHRcdGZ1bmN0aW9uKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCB7IGE6IGdldHRlciB9KTtcblx0cmV0dXJuIGdldHRlcjtcbn07IiwiLy8gZGVmaW5lIGdldHRlciBmdW5jdGlvbnMgZm9yIGhhcm1vbnkgZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgZGVmaW5pdGlvbikge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLmcgPSAoZnVuY3Rpb24oKSB7XG5cdGlmICh0eXBlb2YgZ2xvYmFsVGhpcyA9PT0gJ29iamVjdCcpIHJldHVybiBnbG9iYWxUaGlzO1xuXHR0cnkge1xuXHRcdHJldHVybiB0aGlzIHx8IG5ldyBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpO1xuXHR9IGNhdGNoIChlKSB7XG5cdFx0aWYgKHR5cGVvZiB3aW5kb3cgPT09ICdvYmplY3QnKSByZXR1cm4gd2luZG93O1xuXHR9XG59KSgpOyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iaiwgcHJvcCkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCk7IH0iLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG5cdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuXHR9XG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG59OyIsIi8vIG5vIGJhc2VVUklcblxuLy8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3Ncbi8vIHVuZGVmaW5lZCA9IGNodW5rIG5vdCBsb2FkZWQsIG51bGwgPSBjaHVuayBwcmVsb2FkZWQvcHJlZmV0Y2hlZFxuLy8gW3Jlc29sdmUsIHJlamVjdCwgUHJvbWlzZV0gPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG52YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuXHRcImNvbXBvbmVudC1wcm9kdWN0LWZvcm1cIjogMFxufTtcblxuLy8gbm8gY2h1bmsgb24gZGVtYW5kIGxvYWRpbmdcblxuLy8gbm8gcHJlZmV0Y2hpbmdcblxuLy8gbm8gcHJlbG9hZGVkXG5cbi8vIG5vIEhNUlxuXG4vLyBubyBITVIgbWFuaWZlc3RcblxuX193ZWJwYWNrX3JlcXVpcmVfXy5PLmogPSBmdW5jdGlvbihjaHVua0lkKSB7IHJldHVybiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPT09IDA7IH07XG5cbi8vIGluc3RhbGwgYSBKU09OUCBjYWxsYmFjayBmb3IgY2h1bmsgbG9hZGluZ1xudmFyIHdlYnBhY2tKc29ucENhbGxiYWNrID0gZnVuY3Rpb24ocGFyZW50Q2h1bmtMb2FkaW5nRnVuY3Rpb24sIGRhdGEpIHtcblx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcblx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcblx0dmFyIHJ1bnRpbWUgPSBkYXRhWzJdO1xuXHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcblx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG5cdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDA7XG5cdGlmKGNodW5rSWRzLnNvbWUoZnVuY3Rpb24oaWQpIHsgcmV0dXJuIGluc3RhbGxlZENodW5rc1tpZF0gIT09IDA7IH0pKSB7XG5cdFx0Zm9yKG1vZHVsZUlkIGluIG1vcmVNb2R1bGVzKSB7XG5cdFx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8obW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuXHRcdFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLm1bbW9kdWxlSWRdID0gbW9yZU1vZHVsZXNbbW9kdWxlSWRdO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRpZihydW50aW1lKSB2YXIgcmVzdWx0ID0gcnVudGltZShfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblx0fVxuXHRpZihwYXJlbnRDaHVua0xvYWRpbmdGdW5jdGlvbikgcGFyZW50Q2h1bmtMb2FkaW5nRnVuY3Rpb24oZGF0YSk7XG5cdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG5cdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuXHRcdGlmKF9fd2VicGFja19yZXF1aXJlX18ubyhpbnN0YWxsZWRDaHVua3MsIGNodW5rSWQpICYmIGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKCk7XG5cdFx0fVxuXHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkc1tpXV0gPSAwO1xuXHR9XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fLk8ocmVzdWx0KTtcbn1cblxudmFyIGNodW5rTG9hZGluZ0dsb2JhbCA9IHNlbGZbXCJ3ZWJwYWNrQ2h1bmt3b3JrZmxvd1wiXSA9IHNlbGZbXCJ3ZWJwYWNrQ2h1bmt3b3JrZmxvd1wiXSB8fCBbXTtcbmNodW5rTG9hZGluZ0dsb2JhbC5mb3JFYWNoKHdlYnBhY2tKc29ucENhbGxiYWNrLmJpbmQobnVsbCwgMCkpO1xuY2h1bmtMb2FkaW5nR2xvYmFsLnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjay5iaW5kKG51bGwsIGNodW5rTG9hZGluZ0dsb2JhbC5wdXNoLmJpbmQoY2h1bmtMb2FkaW5nR2xvYmFsKSk7IiwiIiwiLy8gc3RhcnR1cFxuLy8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4vLyBUaGlzIGVudHJ5IG1vZHVsZSBkZXBlbmRzIG9uIG90aGVyIGxvYWRlZCBjaHVua3MgYW5kIGV4ZWN1dGlvbiBuZWVkIHRvIGJlIGRlbGF5ZWRcbnZhciBfX3dlYnBhY2tfZXhwb3J0c19fID0gX193ZWJwYWNrX3JlcXVpcmVfXy5PKHVuZGVmaW5lZCwgW1widmVuZG9yc1wiXSwgZnVuY3Rpb24oKSB7IHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKFwiLi9zcmMvc2NyaXB0cy9jb21wb25lbnRzL3Byb2R1Y3QtZm9ybS5qc1wiKTsgfSlcbl9fd2VicGFja19leHBvcnRzX18gPSBfX3dlYnBhY2tfcmVxdWlyZV9fLk8oX193ZWJwYWNrX2V4cG9ydHNfXyk7XG4iLCIiXSwibmFtZXMiOlsiaHVnTW9uZXlGb3JtYXQiLCJNaWNyb01vZGFsIiwiY2FydCIsImltZ1VSTCIsInNyYyIsInNpemUiLCJjcm9wIiwicmVwbGFjZSIsIm1hdGNoIiwiUHJvZHVjdEZvcm0iLCJ2YXJpYW50SUQiLCJ2YXJpYW50IiwicHJvZHVjdCIsInZhcmlhbnRzIiwiZmluZCIsImlkIiwidmFyaWFudFNlbGVjdGVkIiwiYXZhaWFibGUiLCJhZGRCdG4iLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmUiLCJmb3JtRGF0YSIsIml0ZW1zIiwicXVhbnRpdHkiLCJmZXRjaCIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsInJlcXVlc3QiLCJyZXMiLCJvayIsIkVycm9yIiwianNvbiIsInJlc3VsdCIsInNob3ciLCJnZW5lcmF0ZUNhcnQiLCJjb25zb2xlIiwibG9nIiwicHJvZHVjdEpzb24iLCJ0aGVtZSIsImRhdGEiLCJjYXJ0SW5pdCIsInNlbGVjdFZhcmlhbnRzRWxlIiwicXVlcnlTZWxlY3RvciIsInByb2R1Y3RQcmljZSIsImluaXQiLCJnZXRWYXJpYW50U2VsZWN0ZWQiLCJ1cGRhdGVWYXJ0aWFuU2VsZWN0ZWQiLCJhZGRUb0NhcnQiLCJpdGVtIiwiYXZhaWxhYmxlIiwiYWRkRXZlbnRMaXN0ZW5lciIsImUiLCJoYW5kbGVDaGFuZ2VWYXJpYW50IiwiTnVtYmVyIiwidGFyZ2V0IiwidmFsdWUiLCJpbm5lckhUTUwiLCJwcmljZSIsInByZXZlbnREZWZhdWx0IiwiaGFuZGxlQWRkVG9DYXJ0IiwidGFibGVCb2R5Q29udGVudCIsImRvY3VtZW50IiwidG90YWxQcmljZSIsImNhcnRDb3VudCIsIml0ZW1fY291bnQiLCJ0b3RhbF9wcmljZSIsImNhcnRDb250ZW50IiwibWFwIiwiaW5kZXgiLCJrZXkiLCJ1cmwiLCJmZWF0dXJlZF9pbWFnZSIsImFsdCIsInRpdGxlIiwiam9pbiIsImhhbmRsZUluY3JlYXNlIiwiaGFuZGxlUmVtb3ZlIiwiYnRuRGVjcmVhc2VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsImJ0bkluY3JlYXNlbWVudCIsImZvckVhY2giLCJjbG9zZXN0IiwiZGF0YXNldCIsInByZXZpb3VzRWxlbWVudFNpYmxpbmciLCJ1cGRhdGVJdGVtIiwidGhlbiIsInN0YXRlIiwiY2F0Y2giLCJlcnJvciIsImFsZXJ0IiwibmV4dEVsZW1lbnRTaWJsaW5nIiwicmVtb3ZlQnV0dG9uIiwicmVtb3ZlSXRlbSIsIkhUTUxFbGVtZW50IiwiY3VzdG9tRWxlbWVudHMiLCJkZWZpbmUiLCJmb3JtYXRNb25leSIsImN1cnJlbmN5Rm9ybWF0IiwibW9uZXlGb3JtYXQiLCJjdXJyZW5jeURlY2ltYWwiLCJzbGljZSIsImluZGV4T2YiXSwic291cmNlUm9vdCI6IiJ9