/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/scripts/base.js":
/*!*****************************!*\
  !*** ./src/scripts/base.js ***!
  \*****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/wrapNativeSuper */ "./node_modules/@babel/runtime/helpers/esm/wrapNativeSuper.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var micromodal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! micromodal */ "./node_modules/micromodal/dist/micromodal.es.js");
/* harmony import */ var tippy_js_dist_tippy_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tippy.js/dist/tippy.css */ "./node_modules/tippy.js/dist/tippy.css");
/* harmony import */ var tippy_js_themes_light_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tippy.js/themes/light.css */ "./node_modules/tippy.js/themes/light.css");
/* harmony import */ var _utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./utils/hugMoneyFormat */ "./src/scripts/utils/hugMoneyFormat.js");
/* harmony import */ var unfetch_polyfill__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! unfetch/polyfill */ "./node_modules/unfetch/polyfill/index.js");
/* harmony import */ var unfetch_polyfill__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(unfetch_polyfill__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var es6_promise_auto__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! es6-promise/auto */ "./node_modules/es6-promise/auto.js");
/* harmony import */ var es6_promise_auto__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(es6_promise_auto__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @shopify/theme-cart */ "./node_modules/@shopify/theme-cart/theme-cart.js");









function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0,_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0,_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0,_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }




 // Only need to import these once


 // Import @shopify/theme-cart anywhere you need it



var imgURL = function imgURL(src, size, crop) {
  return src.replace(/_(pico|icon|thumb|small|compact|medium|large|grande|original|500x500|768x768|1024x1024|2048x2048|master)+\./g, '.').replace(/\.jpg|\.png|\.gif|\.jpeg/g, function (match) {
    return "_".concat(size, "_crop_").concat(crop).concat(match);
  });
};

if (document.querySelector('.faq_shiping')) {
  document.querySelector('.faq_shiping').addEventListener('click', function () {
    micromodal__WEBPACK_IMPORTED_MODULE_8__["default"].show('modal-shiping');
  });
}

if (document.querySelector('.specified-item-link-popup')) {
  document.querySelector('.specified-item-link-popup').addEventListener('click', function () {
    micromodal__WEBPACK_IMPORTED_MODULE_8__["default"].show('modal-shiping');
  });
}

if (document.getElementById('register')) {
  var input = document.querySelector('.field-check input');
  var button = document.querySelector('.field-check__btn');
  input.addEventListener('click', function () {
    input.checked ? button.classList.remove('p-button--soldout') : button.classList.add('p-button--soldout');
  });
}

fetch('/cart.js').then(function (response) {
  return response.json();
}).then(function (data) {
  var cartCount = document.querySelector('.cart-count');

  if (data.item_count > 0) {
    cartCount.classList.add('is-active');
    cartCount.innerHTML = data.item_count;
  } else {
    cartCount.classList.remove('is-active');
  }
});

var ShipingPopuup = /*#__PURE__*/function (_HTMLElement) {
  (0,_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(ShipingPopuup, _HTMLElement);

  var _super = _createSuper(ShipingPopuup);

  function ShipingPopuup() {
    var _this;

    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ShipingPopuup);

    _this = _super.call(this);
    _this.openPopupBtn = _this.querySelector('button');

    _this.onOpen();

    return _this;
  }

  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ShipingPopuup, [{
    key: "onOpen",
    value: function onOpen() {
      this.openPopupBtn.addEventListener('click', function () {
        micromodal__WEBPACK_IMPORTED_MODULE_8__["default"].show('modal-shiping');
      });
    }
  }]);

  return ShipingPopuup;
}( /*#__PURE__*/(0,_babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__["default"])(HTMLElement));

customElements.define('shiping-popup-component', ShipingPopuup);

var MessageBox = /*#__PURE__*/function (_HTMLElement2) {
  (0,_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(MessageBox, _HTMLElement2);

  var _super2 = _createSuper(MessageBox);

  function MessageBox() {
    var _this2;

    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, MessageBox);

    _this2 = _super2.call(this);
    _this2.openPopupBtn = _this2.querySelector('a');

    _this2.onOpen();

    return _this2;
  }

  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(MessageBox, [{
    key: "onOpen",
    value: function onOpen() {
      this.openPopupBtn.addEventListener('click', function () {
        micromodal__WEBPACK_IMPORTED_MODULE_8__["default"].show('modal-message');
      });
    }
  }]);

  return MessageBox;
}( /*#__PURE__*/(0,_babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__["default"])(HTMLElement));

customElements.define('message-box', MessageBox);

var SoldoutModal = /*#__PURE__*/function (_HTMLElement3) {
  (0,_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(SoldoutModal, _HTMLElement3);

  var _super3 = _createSuper(SoldoutModal);

  function SoldoutModal() {
    var _this3;

    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, SoldoutModal);

    _this3 = _super3.call(this);
    _this3.openPopupBtn = _this3.querySelector('a');

    _this3.onOpen();

    return _this3;
  }

  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(SoldoutModal, [{
    key: "onOpen",
    value: function onOpen() {
      this.openPopupBtn.addEventListener('click', function () {
        micromodal__WEBPACK_IMPORTED_MODULE_8__["default"].show('modal-soldout');
      });
    }
  }]);

  return SoldoutModal;
}( /*#__PURE__*/(0,_babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__["default"])(HTMLElement));

customElements.define('soldout-modal', SoldoutModal);

var ProductInfor = /*#__PURE__*/function (_HTMLElement4) {
  (0,_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(ProductInfor, _HTMLElement4);

  var _super4 = _createSuper(ProductInfor);

  function ProductInfor() {
    var _this4;

    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ProductInfor);

    _this4 = _super4.call(this);
    _this4.wrapperEle = _this4.querySelector('.product__infor');
    _this4.imgSrc = document.querySelector('#img-src');
    _this4.productTitle = document.querySelector('#product-page-vue');

    _this4.init();

    return _this4;
  }

  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ProductInfor, [{
    key: "init",
    value: function init() {
      if (this.imgSrc && this.productTitle) {
        var src = this.imgSrc.dataset.src;
        var title = this.productTitle.dataset.productTitle;
        var content = "<img src=\"".concat(src, "\" alt=\"image\">\n        <label>").concat(title, "</label>\n        <input name=\"contact[product]\" value=\"").concat(title, "\" style=\"display: none;\">\n      ");
        this.wrapperEle.innerHTML = content;
      }
    }
  }]);

  return ProductInfor;
}( /*#__PURE__*/(0,_babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__["default"])(HTMLElement));

customElements.define('product-infor', ProductInfor);

var CartModal = /*#__PURE__*/function (_HTMLElement5) {
  (0,_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(CartModal, _HTMLElement5);

  var _super5 = _createSuper(CartModal);

  function CartModal() {
    var _this5;

    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, CartModal);

    _this5 = _super5.call(this);
    _this5.theme = theme;
    _this5.dateEle = _this5.querySelector('.date');
    _this5.timeEle = _this5.querySelector('#time');
    _this5.btnCheckout = _this5.querySelectorAll('.btn__checkout');
    _this5.loginEle = _this5.querySelector('.btn__checkout--showmodal');
    _this5.inputDate = _this5.querySelector('.table-date input[type="date"]');

    _this5.init();

    return _this5;
  }

  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(CartModal, [{
    key: "init",
    value: function init() {
      var _this6 = this;

      this.handleChangeTime();
      this.handleChangeDate();
      this.handleUpdateNote();
      this.handleDatePicker();
      this.loginEle && this.showLoginModal();
      window.addEventListener('DOMContentLoaded', /*#__PURE__*/(0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__["default"])( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default().mark(function _callee() {
        var respon, responJson;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(window.location.href.indexOf('cart') != -1)) {
                  _context.next = 9;
                  break;
                }

                _context.next = 3;
                return fetch('/cart.js');

              case 3:
                respon = _context.sent;
                _context.next = 6;
                return respon.json();

              case 6:
                responJson = _context.sent;

                if (responJson.item_count >= 1) {
                  micromodal__WEBPACK_IMPORTED_MODULE_8__["default"].show('modal-1');
                }

                _this6.generateCart(responJson);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      })));
    }
  }, {
    key: "generateCart",
    value: function generateCart(cart) {
      var _this7 = this;

      var tableBodyContent = document.querySelector('#tableContent');
      var totalPrice = document.querySelector('.total__price-final');
      var cartCount = document.querySelector('.cart-count');

      if (cart.item_count > 0) {
        cartCount.classList.add('is-active');
        cartCount.innerHTML = cart.item_count;
      } else {
        cartCount.classList.remove('is-active');
      }

      if (totalPrice) {
        totalPrice.innerHTML = (0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_11__["default"])(cart.total_price, this.theme);
      }

      var cartContent = cart.items.map(function (item, index) {
        return "<tr class=\"cart-item\">\n      <td class=\"row-item\">\n        <svg class=\"cart-item__remove\" data-key=\"".concat(item.key, "\" style=\"cursor: pointer\" width=\"15\" height=\"15\" viewBox=\"0 0 25 26\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n          <path d=\"M0.736328 1.62598L23.7363 24.626\" stroke=\"#A86D54\" stroke-width=\"2\"/>\n          <path d=\"M23.6445 1.70801L0.817893 24.5346\" stroke=\"#A86D54\" stroke-width=\"2\"/>\n        </svg>\n        <a href=\"").concat(item.url, "\" class=\"cart-item__image\">\n          <img src=\"").concat(imgURL(item.featured_image.url, '60x60', 'center'), "\" alt=\"").concat(item.featured_image.alt, "\">\n          <div class=\"cart-item__title\">").concat(item.title, "</div>\n        </a>\n      </td>\n      <td class=\"row-price\">").concat((0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_11__["default"])(item.price, _this7.theme), "</td>\n      <td class=\"row-quantity\">\n        <div class=\"js-input-number\" data-variantid=\"").concat(item.id, "\" data-key=\"").concat(item.key, "\">\n          <button type=\"button\" class=\"input-decreasement\">-</button>\n          <input class=\"input-value\" type=\"text\" value=\"").concat(item.quantity, "\" min=\"1\">\n          <button type=\"button\" class=\"input-increasement\">+</button>\n        </div>\n      </td>\n      <td class=\"row-total\">\n        ").concat((0,_utils_hugMoneyFormat__WEBPACK_IMPORTED_MODULE_11__["default"])(item.price * item.quantity, _this7.theme), "\n      </td>\n    </tr>");
      });

      if (tableBodyContent) {
        tableBodyContent.innerHTML = cartContent.join("");
      }

      this.handleIncrease();
      this.handleRemove();
    }
  }, {
    key: "handleIncrease",
    value: function handleIncrease() {
      var _this8 = this;

      var btnDecreasement = document.querySelectorAll('.input-decreasement');
      var btnIncreasement = document.querySelectorAll('.input-increasement');
      btnIncreasement.forEach(function (item) {
        item.addEventListener('click', function () {
          var key = item.closest('.js-input-number').dataset.key;
          var quantity = Number(item.previousElementSibling.value) + 1;
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__.updateItem(key, {
            quantity: quantity
          }).then(function (state) {
            _this8.generateCart(state);
          }).catch(function (error) {
            alert('Sold out or bad response from Shopify');
          });
        });
      });
      btnDecreasement.forEach(function (item) {
        item.addEventListener('click', function () {
          var key = item.closest('.js-input-number').dataset.key;
          var quantity = Number(item.nextElementSibling.value) - 1;
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__.updateItem(key, {
            quantity: quantity
          }).then(function (state) {
            _this8.generateCart(state);
          }).catch(function (error) {
            alert('Sold out or bad response from Shopify');
          });
        });
      });
    }
  }, {
    key: "handleRemove",
    value: function handleRemove() {
      var _this9 = this;

      var removeButton = document.querySelectorAll('.cart-item__remove');
      removeButton.forEach(function (item) {
        item.addEventListener('click', function () {
          _shopify_theme_cart__WEBPACK_IMPORTED_MODULE_14__.removeItem(item.dataset.key).then(function (state) {
            _this9.generateCart(state);
          });
        });
      });
    }
  }, {
    key: "showLoginModal",
    value: function showLoginModal() {
      this.loginEle.addEventListener('click', function () {
        micromodal__WEBPACK_IMPORTED_MODULE_8__["default"].show('modal-login');
      });
    }
  }, {
    key: "handleChangeDate",
    value: function handleChangeDate() {
      this.dateEle.setAttribute('data-date', '');

      if (localStorage.getItem('date')) {
        var now = $.datepicker.formatDate('yy/mm/dd', new Date());

        if (localStorage.getItem('date') <= now) {
          this.dateEle.value = '';
          localStorage.setItem('date', '');
        } else {
          this.dateEle.value = localStorage.getItem('date');
        }
      } // this.dateEle.addEventListener('change', (e) => {
      //   console.log("event change: ", e.target);
      //   let date = '';
      //   date = e.target.value;
      //   date = `お届け希望日 : ${date}`;
      //   this.dateEle.setAttribute('data-date', date);
      //   const dateStorage = e.target.value;
      //   localStorage.setItem('date', dateStorage);
      // })


      $("#datepicker").on("change", function () {
        var date = '';
        date = $(this).val();
        date = "\u304A\u5C4A\u3051\u5E0C\u671B\u65E5 : ".concat(date);
        $(this).attr('data-date', date);
        var dateStorage = $(this).val();
        localStorage.setItem('date', dateStorage);
      });
    }
  }, {
    key: "handleChangeTime",
    value: function handleChangeTime() {
      var _this10 = this;

      this.timeEle.setAttribute('data-time', '');

      if (localStorage.getItem('time')) {
        this.timeEle.value = localStorage.getItem('time');
      }

      this.timeEle.addEventListener('change', function (e) {
        var time = '';

        if (e.target.value !== '指定なし') {
          time = "\u6642\u9593\u5E2F\u6307\u5B9A : ".concat(e.target.value);

          _this10.timeEle.setAttribute('data-time', time);
        }

        var timeStorage = e.target.value;
        localStorage.setItem('time', timeStorage);
      });
    }
  }, {
    key: "handleUpdateNote",
    value: function handleUpdateNote() {
      var _this11 = this;

      this.btnCheckout.forEach(function (item) {
        item.addEventListener('click', /*#__PURE__*/(0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__["default"])( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default().mark(function _callee2() {
          var date, time;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_7___default().wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  date = _this11.dateEle.dataset.date;
                  time = _this11.timeEle.dataset.time;

                  if (!(date && time)) {
                    _context2.next = 5;
                    break;
                  }

                  _context2.next = 5;
                  return fetch('/cart/update.js', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      note: "".concat(date, ", ").concat(time)
                    })
                  });

                case 5:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2);
        })));
      });
    } // handleDisablePastDate() {
    //   this.dateEle.setAttribute('min', '');
    //   var dtToday = new Date();
    //   var month = dtToday.getMonth() + 1;
    //   var day = dtToday.getDate() + 1;
    //   var year = dtToday.getFullYear();
    //   if(month < 10)
    //       month = '0' + month.toString();
    //   if(day < 10)
    //       day = '0' + day.toString();
    //   var minDate = year + '-' + month + '-' + day;
    //   this.dateEle.setAttribute('min', minDate);
    // }

  }, {
    key: "handleDatePicker",
    value: function handleDatePicker() {
      $("#datepicker").datepicker({
        minDate: '+1D',
        //最短日の指定*3日後から指定可能
        maxDate: '+2M',
        //最長日の指定*2ヶ月後まで指定可能
        beforeShowDay: function beforeShowDay(date) {
          if (date.getDay() == 0) {
            return [true, 'day-sunday', null]; //trueで土曜日を選択可に変更
          } else if (date.getDay() == 6) {
            return [true, 'day-saturday', null]; //trueで日曜日を選択可に変更
          }

          return [true, 'day-weekday', null];
        }
      });
    }
  }]);

  return CartModal;
}( /*#__PURE__*/(0,_babel_runtime_helpers_wrapNativeSuper__WEBPACK_IMPORTED_MODULE_6__["default"])(HTMLElement));

customElements.define('cart-modal', CartModal);

/***/ }),

/***/ "./src/scripts/utils/hugMoneyFormat.js":
/*!*********************************************!*\
  !*** ./src/scripts/utils/hugMoneyFormat.js ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shopify_theme_currency__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shopify/theme-currency */ "./node_modules/@shopify/theme-currency/currency.js");


var hugMoneyFormat = function hugMoneyFormat(value, theme) {
  var currencyFormat = (0,_shopify_theme_currency__WEBPACK_IMPORTED_MODULE_0__.formatMoney)(Number(value), theme.moneyFormat);
  var currencyDecimal = currencyFormat.slice(currencyFormat.indexOf(','));

  if (currencyDecimal === ',00') {
    return currencyFormat.slice(0, currencyFormat.indexOf(','));
  }

  return currencyFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (hugMoneyFormat);

/***/ }),

/***/ "./src/styles/base.scss":
/*!******************************!*\
  !*** ./src/styles/base.scss ***!
  \******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"base": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkworkflow"] = self["webpackChunkworkflow"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["vendors"], function() { return __webpack_require__("./src/scripts/base.js"); })
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors"], function() { return __webpack_require__("./src/styles/base.scss"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7Q0FFQTs7QUFDQTtDQUdBOztBQUNBOztBQUVBLElBQU1HLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUNDLEdBQUQsRUFBTUMsSUFBTixFQUFZQyxJQUFaO0FBQUEsU0FBcUJGLEdBQUcsQ0FDcENHLE9BRGlDLENBQ3pCLDhHQUR5QixFQUN1RixHQUR2RixFQUVqQ0EsT0FGaUMsQ0FFekIsMkJBRnlCLEVBRUksVUFBQ0MsS0FBRDtBQUFBLHNCQUFlSCxJQUFmLG1CQUE0QkMsSUFBNUIsU0FBbUNFLEtBQW5DO0FBQUEsR0FGSixDQUFyQjtBQUFBLENBQWY7O0FBS0EsSUFBSUMsUUFBUSxDQUFDQyxhQUFULENBQXVCLGNBQXZCLENBQUosRUFBNEM7QUFDMUNELEVBQUFBLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixjQUF2QixFQUF1Q0MsZ0JBQXZDLENBQXdELE9BQXhELEVBQWlFLFlBQU07QUFDckVYLElBQUFBLHVEQUFBLENBQWdCLGVBQWhCO0FBQ0QsR0FGRDtBQUdEOztBQUdELElBQUlTLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1Qiw0QkFBdkIsQ0FBSixFQUEwRDtBQUN4REQsRUFBQUEsUUFBUSxDQUFDQyxhQUFULENBQXVCLDRCQUF2QixFQUFxREMsZ0JBQXJELENBQXNFLE9BQXRFLEVBQStFLFlBQU07QUFDbkZYLElBQUFBLHVEQUFBLENBQWdCLGVBQWhCO0FBQ0QsR0FGRDtBQUdEOztBQUVELElBQUlTLFFBQVEsQ0FBQ0ksY0FBVCxDQUF3QixVQUF4QixDQUFKLEVBQXlDO0FBQ3ZDLE1BQU1DLEtBQUssR0FBR0wsUUFBUSxDQUFDQyxhQUFULENBQXVCLG9CQUF2QixDQUFkO0FBQ0EsTUFBTUssTUFBTSxHQUFHTixRQUFRLENBQUNDLGFBQVQsQ0FBdUIsbUJBQXZCLENBQWY7QUFFQUksRUFBQUEsS0FBSyxDQUFDSCxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxZQUFNO0FBQ3BDRyxJQUFBQSxLQUFLLENBQUNFLE9BQU4sR0FBZ0JELE1BQU0sQ0FBQ0UsU0FBUCxDQUFpQkMsTUFBakIsQ0FBd0IsbUJBQXhCLENBQWhCLEdBQStESCxNQUFNLENBQUNFLFNBQVAsQ0FBaUJFLEdBQWpCLENBQXFCLG1CQUFyQixDQUEvRDtBQUNELEdBRkQ7QUFHRDs7QUFFREMsS0FBSyxDQUFDLFVBQUQsQ0FBTCxDQUNHQyxJQURILENBQ1EsVUFBQUMsUUFBUTtBQUFBLFNBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxFQUFKO0FBQUEsQ0FEaEIsRUFFR0YsSUFGSCxDQUVRLFVBQUFHLElBQUksRUFBSTtBQUNaLE1BQU1DLFNBQVMsR0FBR2hCLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixhQUF2QixDQUFsQjs7QUFFQSxNQUFJYyxJQUFJLENBQUNFLFVBQUwsR0FBa0IsQ0FBdEIsRUFBeUI7QUFDdkJELElBQUFBLFNBQVMsQ0FBQ1IsU0FBVixDQUFvQkUsR0FBcEIsQ0FBd0IsV0FBeEI7QUFDQU0sSUFBQUEsU0FBUyxDQUFDRSxTQUFWLEdBQXNCSCxJQUFJLENBQUNFLFVBQTNCO0FBQ0QsR0FIRCxNQUdPO0FBQ0xELElBQUFBLFNBQVMsQ0FBQ1IsU0FBVixDQUFvQkMsTUFBcEIsQ0FBMkIsV0FBM0I7QUFDRDtBQUNGLENBWEg7O0lBWU1VOzs7OztBQUNKLDJCQUFjO0FBQUE7O0FBQUE7O0FBQ1o7QUFDQSxVQUFLQyxZQUFMLEdBQW9CLE1BQUtuQixhQUFMLENBQW1CLFFBQW5CLENBQXBCOztBQUVBLFVBQUtvQixNQUFMOztBQUpZO0FBS2I7Ozs7V0FFRCxrQkFBUztBQUNQLFdBQUtELFlBQUwsQ0FBa0JsQixnQkFBbEIsQ0FBbUMsT0FBbkMsRUFBNEMsWUFBTTtBQUNoRFgsUUFBQUEsdURBQUEsQ0FBZ0IsZUFBaEI7QUFDRCxPQUZEO0FBR0Q7Ozs7bUdBWnlCK0I7O0FBZ0I1QkMsY0FBYyxDQUFDQyxNQUFmLENBQXNCLHlCQUF0QixFQUFpREwsYUFBakQ7O0lBR01NOzs7OztBQUNKLHdCQUFjO0FBQUE7O0FBQUE7O0FBQ1o7QUFDQSxXQUFLTCxZQUFMLEdBQW9CLE9BQUtuQixhQUFMLENBQW1CLEdBQW5CLENBQXBCOztBQUVBLFdBQUtvQixNQUFMOztBQUpZO0FBS2I7Ozs7V0FFRCxrQkFBUztBQUNQLFdBQUtELFlBQUwsQ0FBa0JsQixnQkFBbEIsQ0FBbUMsT0FBbkMsRUFBNEMsWUFBTTtBQUNoRFgsUUFBQUEsdURBQUEsQ0FBZ0IsZUFBaEI7QUFDRCxPQUZEO0FBR0Q7Ozs7bUdBWnNCK0I7O0FBZXpCQyxjQUFjLENBQUNDLE1BQWYsQ0FBc0IsYUFBdEIsRUFBcUNDLFVBQXJDOztJQUVNQzs7Ozs7QUFDSiwwQkFBYztBQUFBOztBQUFBOztBQUNaO0FBQ0EsV0FBS04sWUFBTCxHQUFvQixPQUFLbkIsYUFBTCxDQUFtQixHQUFuQixDQUFwQjs7QUFFQSxXQUFLb0IsTUFBTDs7QUFKWTtBQUtiOzs7O1dBRUQsa0JBQVM7QUFDUCxXQUFLRCxZQUFMLENBQWtCbEIsZ0JBQWxCLENBQW1DLE9BQW5DLEVBQTRDLFlBQU07QUFDaERYLFFBQUFBLHVEQUFBLENBQWdCLGVBQWhCO0FBQ0QsT0FGRDtBQUdEOzs7O21HQVp3QitCOztBQWUzQkMsY0FBYyxDQUFDQyxNQUFmLENBQXNCLGVBQXRCLEVBQXVDRSxZQUF2Qzs7SUFFTUM7Ozs7O0FBQ0osMEJBQWM7QUFBQTs7QUFBQTs7QUFDWjtBQUVBLFdBQUtDLFVBQUwsR0FBa0IsT0FBSzNCLGFBQUwsQ0FBbUIsaUJBQW5CLENBQWxCO0FBQ0EsV0FBSzRCLE1BQUwsR0FBYzdCLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixVQUF2QixDQUFkO0FBQ0EsV0FBSzZCLFlBQUwsR0FBb0I5QixRQUFRLENBQUNDLGFBQVQsQ0FBdUIsbUJBQXZCLENBQXBCOztBQUNBLFdBQUs4QixJQUFMOztBQU5ZO0FBT2I7Ozs7V0FFRCxnQkFBTztBQUNMLFVBQUksS0FBS0YsTUFBTCxJQUFlLEtBQUtDLFlBQXhCLEVBQXNDO0FBQ3BDLFlBQU1uQyxHQUFHLEdBQUcsS0FBS2tDLE1BQUwsQ0FBWUcsT0FBWixDQUFvQnJDLEdBQWhDO0FBQ0EsWUFBTXNDLEtBQUssR0FBRyxLQUFLSCxZQUFMLENBQWtCRSxPQUFsQixDQUEwQkYsWUFBeEM7QUFDQSxZQUFNSSxPQUFPLHdCQUFnQnZDLEdBQWhCLCtDQUNGc0MsS0FERSx3RUFFNkJBLEtBRjdCLHlDQUFiO0FBS0EsYUFBS0wsVUFBTCxDQUFnQlYsU0FBaEIsR0FBNEJnQixPQUE1QjtBQUNEO0FBQ0Y7Ozs7bUdBckJ3Qlo7O0FBd0IzQkMsY0FBYyxDQUFDQyxNQUFmLENBQXNCLGVBQXRCLEVBQXVDRyxZQUF2Qzs7SUFFTVE7Ozs7O0FBQ0osdUJBQWM7QUFBQTs7QUFBQTs7QUFDWjtBQUNBLFdBQUtDLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFdBQUtDLE9BQUwsR0FBZSxPQUFLcEMsYUFBTCxDQUFtQixPQUFuQixDQUFmO0FBQ0EsV0FBS3FDLE9BQUwsR0FBZSxPQUFLckMsYUFBTCxDQUFtQixPQUFuQixDQUFmO0FBQ0EsV0FBS3NDLFdBQUwsR0FBbUIsT0FBS0MsZ0JBQUwsQ0FBc0IsZ0JBQXRCLENBQW5CO0FBQ0EsV0FBS0MsUUFBTCxHQUFnQixPQUFLeEMsYUFBTCxDQUFtQiwyQkFBbkIsQ0FBaEI7QUFDQSxXQUFLeUMsU0FBTCxHQUFpQixPQUFLekMsYUFBTCxDQUFtQixnQ0FBbkIsQ0FBakI7O0FBQ0EsV0FBSzhCLElBQUw7O0FBUlk7QUFTYjs7OztXQUVELGdCQUFPO0FBQUE7O0FBQ0wsV0FBS1ksZ0JBQUw7QUFDQSxXQUFLQyxnQkFBTDtBQUNBLFdBQUtDLGdCQUFMO0FBQ0EsV0FBS0MsZ0JBQUw7QUFDQSxXQUFLTCxRQUFMLElBQWlCLEtBQUtNLGNBQUwsRUFBakI7QUFDQUMsTUFBQUEsTUFBTSxDQUFDOUMsZ0JBQVAsQ0FBd0Isa0JBQXhCLHdMQUE0QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFDdEM4QyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQWhCLENBQXFCQyxPQUFyQixDQUE2QixNQUE3QixLQUF3QyxDQUFDLENBREg7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSx1QkFFbkJ4QyxLQUFLLENBQUMsVUFBRCxDQUZjOztBQUFBO0FBRWxDeUMsZ0JBQUFBLE1BRmtDO0FBQUE7QUFBQSx1QkFHZkEsTUFBTSxDQUFDdEMsSUFBUCxFQUhlOztBQUFBO0FBR2xDdUMsZ0JBQUFBLFVBSGtDOztBQUt4QyxvQkFBSUEsVUFBVSxDQUFDcEMsVUFBWCxJQUF5QixDQUE3QixFQUFnQztBQUM5QjFCLGtCQUFBQSx1REFBQSxDQUFnQixTQUFoQjtBQUNEOztBQUVELHNCQUFJLENBQUMrRCxZQUFMLENBQWtCRCxVQUFsQjs7QUFUd0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBNUM7QUFZRDs7O1dBRUQsc0JBQWE1RCxJQUFiLEVBQW1CO0FBQUE7O0FBQ2pCLFVBQU04RCxnQkFBZ0IsR0FBR3ZELFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixlQUF2QixDQUF6QjtBQUNBLFVBQU11RCxVQUFVLEdBQUd4RCxRQUFRLENBQUNDLGFBQVQsQ0FBdUIscUJBQXZCLENBQW5CO0FBRUEsVUFBTWUsU0FBUyxHQUFHaEIsUUFBUSxDQUFDQyxhQUFULENBQXVCLGFBQXZCLENBQWxCOztBQUVBLFVBQUlSLElBQUksQ0FBQ3dCLFVBQUwsR0FBa0IsQ0FBdEIsRUFBeUI7QUFDdkJELFFBQUFBLFNBQVMsQ0FBQ1IsU0FBVixDQUFvQkUsR0FBcEIsQ0FBd0IsV0FBeEI7QUFDQU0sUUFBQUEsU0FBUyxDQUFDRSxTQUFWLEdBQXNCekIsSUFBSSxDQUFDd0IsVUFBM0I7QUFDRCxPQUhELE1BR087QUFDTEQsUUFBQUEsU0FBUyxDQUFDUixTQUFWLENBQW9CQyxNQUFwQixDQUEyQixXQUEzQjtBQUNEOztBQUdELFVBQUkrQyxVQUFKLEVBQWdCO0FBQ2RBLFFBQUFBLFVBQVUsQ0FBQ3RDLFNBQVgsR0FBdUIxQixrRUFBYyxDQUFDQyxJQUFJLENBQUNnRSxXQUFOLEVBQW1CLEtBQUtyQixLQUF4QixDQUFyQztBQUNEOztBQUVELFVBQU1zQixXQUFXLEdBQUdqRSxJQUFJLENBQUNrRSxLQUFMLENBQVdDLEdBQVgsQ0FBZSxVQUFDQyxJQUFELEVBQU9DLEtBQVAsRUFBaUI7QUFDbEQsc0lBRTZDRCxJQUFJLENBQUNFLEdBRmxELG1YQU1hRixJQUFJLENBQUNHLEdBTmxCLGtFQU9nQnRFLE1BQU0sQ0FBQ21FLElBQUksQ0FBQ0ksY0FBTCxDQUFvQkQsR0FBckIsRUFBMEIsT0FBMUIsRUFBbUMsUUFBbkMsQ0FQdEIsc0JBTzRFSCxJQUFJLENBQUNJLGNBQUwsQ0FBb0JDLEdBUGhHLDREQVFvQ0wsSUFBSSxDQUFDNUIsS0FSekMsOEVBV3dCekMsa0VBQWMsQ0FBQ3FFLElBQUksQ0FBQ00sS0FBTixFQUFhLE1BQUksQ0FBQy9CLEtBQWxCLENBWHRDLCtHQWFpRHlCLElBQUksQ0FBQ08sRUFidEQsMkJBYXVFUCxJQUFJLENBQUNFLEdBYjVFLDBKQWVvREYsSUFBSSxDQUFDUSxRQWZ6RCw0S0FvQkk3RSxrRUFBYyxDQUFDcUUsSUFBSSxDQUFDTSxLQUFMLEdBQWFOLElBQUksQ0FBQ1EsUUFBbkIsRUFBNkIsTUFBSSxDQUFDakMsS0FBbEMsQ0FwQmxCO0FBdUJELE9BeEJtQixDQUFwQjs7QUEwQkMsVUFBSW1CLGdCQUFKLEVBQXNCO0FBQ3JCQSxRQUFBQSxnQkFBZ0IsQ0FBQ3JDLFNBQWpCLEdBQTZCd0MsV0FBVyxDQUFDWSxJQUFaLENBQWlCLEVBQWpCLENBQTdCO0FBQ0E7O0FBRUQsV0FBS0MsY0FBTDtBQUNDLFdBQUtDLFlBQUw7QUFDSDs7O1dBRUQsMEJBQWlCO0FBQUE7O0FBQ2YsVUFBTUMsZUFBZSxHQUFHekUsUUFBUSxDQUFDd0MsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQXhCO0FBQ0EsVUFBTWtDLGVBQWUsR0FBRzFFLFFBQVEsQ0FBQ3dDLGdCQUFULENBQTBCLHFCQUExQixDQUF4QjtBQUdBa0MsTUFBQUEsZUFBZSxDQUFDQyxPQUFoQixDQUF3QixVQUFDZCxJQUFELEVBQVU7QUFDaENBLFFBQUFBLElBQUksQ0FBQzNELGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFlBQU07QUFDbkMsY0FBTTZELEdBQUcsR0FBR0YsSUFBSSxDQUFDZSxPQUFMLENBQWEsa0JBQWIsRUFBaUM1QyxPQUFqQyxDQUF5QytCLEdBQXJEO0FBQ0EsY0FBSU0sUUFBUSxHQUFHUSxNQUFNLENBQUNoQixJQUFJLENBQUNpQixzQkFBTCxDQUE0QkMsS0FBN0IsQ0FBTixHQUE0QyxDQUEzRDtBQUdBdEYsVUFBQUEsNERBQUEsQ0FBZ0JzRSxHQUFoQixFQUFxQjtBQUFFTSxZQUFBQSxRQUFRLEVBQVJBO0FBQUYsV0FBckIsRUFDR3pELElBREgsQ0FDUSxVQUFBcUUsS0FBSyxFQUFJO0FBQ2Isa0JBQUksQ0FBQzNCLFlBQUwsQ0FBa0IyQixLQUFsQjtBQUNELFdBSEgsRUFJR0MsS0FKSCxDQUlTLFVBQUNDLEtBQUQsRUFBVztBQUNoQkMsWUFBQUEsS0FBSyxDQUFDLHVDQUFELENBQUw7QUFDRCxXQU5IO0FBUUQsU0FiRDtBQWNELE9BZkQ7QUFpQkFYLE1BQUFBLGVBQWUsQ0FBQ0UsT0FBaEIsQ0FBd0IsVUFBQ2QsSUFBRCxFQUFVO0FBQ2hDQSxRQUFBQSxJQUFJLENBQUMzRCxnQkFBTCxDQUFzQixPQUF0QixFQUErQixZQUFNO0FBQ25DLGNBQU02RCxHQUFHLEdBQUdGLElBQUksQ0FBQ2UsT0FBTCxDQUFhLGtCQUFiLEVBQWlDNUMsT0FBakMsQ0FBeUMrQixHQUFyRDtBQUNBLGNBQUlNLFFBQVEsR0FBR1EsTUFBTSxDQUFDaEIsSUFBSSxDQUFDd0Isa0JBQUwsQ0FBd0JOLEtBQXpCLENBQU4sR0FBd0MsQ0FBdkQ7QUFFQXRGLFVBQUFBLDREQUFBLENBQWdCc0UsR0FBaEIsRUFBcUI7QUFBRU0sWUFBQUEsUUFBUSxFQUFSQTtBQUFGLFdBQXJCLEVBQ0d6RCxJQURILENBQ1EsVUFBQXFFLEtBQUssRUFBSTtBQUNiLGtCQUFJLENBQUMzQixZQUFMLENBQWtCMkIsS0FBbEI7QUFDRCxXQUhILEVBSUdDLEtBSkgsQ0FJUyxVQUFDQyxLQUFELEVBQVc7QUFDaEJDLFlBQUFBLEtBQUssQ0FBQyx1Q0FBRCxDQUFMO0FBQ0QsV0FOSDtBQVFELFNBWkQ7QUFhRCxPQWREO0FBZUQ7OztXQUVELHdCQUFlO0FBQUE7O0FBQ2IsVUFBTUUsWUFBWSxHQUFHdEYsUUFBUSxDQUFDd0MsZ0JBQVQsQ0FBMEIsb0JBQTFCLENBQXJCO0FBQ0E4QyxNQUFBQSxZQUFZLENBQUNYLE9BQWIsQ0FBcUIsVUFBQ2QsSUFBRCxFQUFVO0FBQzdCQSxRQUFBQSxJQUFJLENBQUMzRCxnQkFBTCxDQUFzQixPQUF0QixFQUErQixZQUFNO0FBQ25DVCxVQUFBQSw0REFBQSxDQUFnQm9FLElBQUksQ0FBQzdCLE9BQUwsQ0FBYStCLEdBQTdCLEVBQWtDbkQsSUFBbEMsQ0FBdUMsVUFBQXFFLEtBQUssRUFBSTtBQUM5QyxrQkFBSSxDQUFDM0IsWUFBTCxDQUFrQjJCLEtBQWxCO0FBQ0QsV0FGRDtBQUdELFNBSkQ7QUFLRCxPQU5EO0FBT0Q7OztXQUVELDBCQUFpQjtBQUNmLFdBQUt4QyxRQUFMLENBQWN2QyxnQkFBZCxDQUErQixPQUEvQixFQUF3QyxZQUFNO0FBQzVDWCxRQUFBQSx1REFBQSxDQUFnQixhQUFoQjtBQUNELE9BRkQ7QUFHRDs7O1dBRUQsNEJBQW1CO0FBQ2pCLFdBQUs4QyxPQUFMLENBQWFtRCxZQUFiLENBQTBCLFdBQTFCLEVBQXVDLEVBQXZDOztBQUNBLFVBQUlDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixNQUFyQixDQUFKLEVBQWtDO0FBQ2hDLFlBQUlDLEdBQUcsR0FBR0MsQ0FBQyxDQUFDQyxVQUFGLENBQWFDLFVBQWIsQ0FBd0IsVUFBeEIsRUFBb0MsSUFBSUMsSUFBSixFQUFwQyxDQUFWOztBQUNBLFlBQUlOLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixNQUFyQixLQUFnQ0MsR0FBcEMsRUFBeUM7QUFDdkMsZUFBS3RELE9BQUwsQ0FBYTBDLEtBQWIsR0FBcUIsRUFBckI7QUFDQVUsVUFBQUEsWUFBWSxDQUFDTyxPQUFiLENBQXFCLE1BQXJCLEVBQTZCLEVBQTdCO0FBQ0QsU0FIRCxNQUdPO0FBQ0wsZUFBSzNELE9BQUwsQ0FBYTBDLEtBQWIsR0FBb0JVLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixNQUFyQixDQUFwQjtBQUNEO0FBQ0YsT0FWZ0IsQ0FZakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFDQUUsTUFBQUEsQ0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQkssRUFBakIsQ0FBb0IsUUFBcEIsRUFBNkIsWUFBVTtBQUNuQyxZQUFJQyxJQUFJLEdBQUcsRUFBWDtBQUNBQSxRQUFBQSxJQUFJLEdBQUdOLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUU8sR0FBUixFQUFQO0FBQ0FELFFBQUFBLElBQUksb0RBQWVBLElBQWYsQ0FBSjtBQUNBTixRQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFRLElBQVIsQ0FBYSxXQUFiLEVBQTBCRixJQUExQjtBQUVBLFlBQU1HLFdBQVcsR0FBR1QsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRTyxHQUFSLEVBQXBCO0FBQ0FWLFFBQUFBLFlBQVksQ0FBQ08sT0FBYixDQUFxQixNQUFyQixFQUE2QkssV0FBN0I7QUFDSCxPQVJEO0FBVUQ7OztXQUVELDRCQUFtQjtBQUFBOztBQUNqQixXQUFLL0QsT0FBTCxDQUFha0QsWUFBYixDQUEwQixXQUExQixFQUF1QyxFQUF2Qzs7QUFFQSxVQUFJQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsTUFBckIsQ0FBSixFQUFrQztBQUNoQyxhQUFLcEQsT0FBTCxDQUFheUMsS0FBYixHQUFxQlUsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE1BQXJCLENBQXJCO0FBQ0Q7O0FBRUQsV0FBS3BELE9BQUwsQ0FBYXBDLGdCQUFiLENBQThCLFFBQTlCLEVBQXdDLFVBQUNvRyxDQUFELEVBQU87QUFDN0MsWUFBSUMsSUFBSSxHQUFHLEVBQVg7O0FBQ0EsWUFBSUQsQ0FBQyxDQUFDRSxNQUFGLENBQVN6QixLQUFULEtBQW1CLE1BQXZCLEVBQStCO0FBQzdCd0IsVUFBQUEsSUFBSSw4Q0FBY0QsQ0FBQyxDQUFDRSxNQUFGLENBQVN6QixLQUF2QixDQUFKOztBQUNBLGlCQUFJLENBQUN6QyxPQUFMLENBQWFrRCxZQUFiLENBQTBCLFdBQTFCLEVBQXVDZSxJQUF2QztBQUNEOztBQUVELFlBQU1FLFdBQVcsR0FBR0gsQ0FBQyxDQUFDRSxNQUFGLENBQVN6QixLQUE3QjtBQUNBVSxRQUFBQSxZQUFZLENBQUNPLE9BQWIsQ0FBcUIsTUFBckIsRUFBNkJTLFdBQTdCO0FBQ0QsT0FURDtBQVVEOzs7V0FFRCw0QkFBbUI7QUFBQTs7QUFDakIsV0FBS2xFLFdBQUwsQ0FBaUJvQyxPQUFqQixDQUF5QixVQUFBZCxJQUFJLEVBQUk7QUFDL0JBLFFBQUFBLElBQUksQ0FBQzNELGdCQUFMLENBQXNCLE9BQXRCLHdMQUErQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDekJnRyxrQkFBQUEsSUFEeUIsR0FDbEIsT0FBSSxDQUFDN0QsT0FBTCxDQUFhTCxPQUFiLENBQXFCa0UsSUFESDtBQUV6Qkssa0JBQUFBLElBRnlCLEdBRWxCLE9BQUksQ0FBQ2pFLE9BQUwsQ0FBYU4sT0FBYixDQUFxQnVFLElBRkg7O0FBQUEsd0JBSXpCTCxJQUFJLElBQUlLLElBSmlCO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEseUJBS3JCNUYsS0FBSyxDQUFDLGlCQUFELEVBQW9CO0FBQzdCK0Ysb0JBQUFBLE1BQU0sRUFBRSxNQURxQjtBQUU3QkMsb0JBQUFBLE9BQU8sRUFBRTtBQUNQLHNDQUFnQjtBQURULHFCQUZvQjtBQUs3QkMsb0JBQUFBLElBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDbkJDLHNCQUFBQSxJQUFJLFlBQUtiLElBQUwsZUFBY0ssSUFBZDtBQURlLHFCQUFmO0FBTHVCLG1CQUFwQixDQUxnQjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUEvQjtBQWdCRCxPQWpCRDtBQWtCRCxNQUVEO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFHQTtBQUNBOzs7O1dBRUEsNEJBQW1CO0FBQ2pCWCxNQUFBQSxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCQyxVQUFqQixDQUE0QjtBQUMxQm1CLFFBQUFBLE9BQU8sRUFBRSxLQURpQjtBQUNWO0FBQ2hCQyxRQUFBQSxPQUFPLEVBQUUsS0FGaUI7QUFFVjtBQUNmQyxRQUFBQSxhQUFhLEVBQUUsdUJBQVNoQixJQUFULEVBQWU7QUFDNUIsY0FBSUEsSUFBSSxDQUFDaUIsTUFBTCxNQUFpQixDQUFyQixFQUF3QjtBQUN0QixtQkFBTyxDQUFDLElBQUQsRUFBTyxZQUFQLEVBQXFCLElBQXJCLENBQVAsQ0FEc0IsQ0FDWTtBQUNuQyxXQUZELE1BRU8sSUFBSWpCLElBQUksQ0FBQ2lCLE1BQUwsTUFBaUIsQ0FBckIsRUFBd0I7QUFDN0IsbUJBQU8sQ0FBQyxJQUFELEVBQU8sY0FBUCxFQUF1QixJQUF2QixDQUFQLENBRDZCLENBQ087QUFDckM7O0FBQ0QsaUJBQU8sQ0FBQyxJQUFELEVBQU8sYUFBUCxFQUFzQixJQUF0QixDQUFQO0FBQ0Q7QUFWd0IsT0FBNUI7QUFZRDs7OzttR0FyUHFCN0Y7O0FBd1B4QkMsY0FBYyxDQUFDQyxNQUFmLENBQXNCLFlBQXRCLEVBQW9DVyxTQUFwQzs7Ozs7Ozs7Ozs7O0FDelhBOztBQUVBLElBQU0zQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUN1RixLQUFELEVBQVEzQyxLQUFSLEVBQWtCO0FBQ3ZDLE1BQU1pRixjQUFjLEdBQUdELG9FQUFXLENBQUN2QyxNQUFNLENBQUNFLEtBQUQsQ0FBUCxFQUFnQjNDLEtBQUssQ0FBQ2tGLFdBQXRCLENBQWxDO0FBQ0EsTUFBTUMsZUFBZSxHQUFHRixjQUFjLENBQUNHLEtBQWYsQ0FBcUJILGNBQWMsQ0FBQ2xFLE9BQWYsQ0FBdUIsR0FBdkIsQ0FBckIsQ0FBeEI7O0FBQ0EsTUFBSW9FLGVBQWUsS0FBSyxLQUF4QixFQUErQjtBQUM3QixXQUFPRixjQUFjLENBQUNHLEtBQWYsQ0FBcUIsQ0FBckIsRUFBd0JILGNBQWMsQ0FBQ2xFLE9BQWYsQ0FBdUIsR0FBdkIsQ0FBeEIsQ0FBUDtBQUNEOztBQUNELFNBQU9rRSxjQUFQO0FBQ0QsQ0FQRDs7QUFTQSwrREFBZTdILGNBQWY7Ozs7Ozs7Ozs7O0FDWEE7Ozs7Ozs7VUNBQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOztVQUVBO1VBQ0E7Ozs7O1dDekJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EsK0JBQStCLHdDQUF3QztXQUN2RTtXQUNBO1dBQ0E7V0FDQTtXQUNBLGlCQUFpQixxQkFBcUI7V0FDdEM7V0FDQTtXQUNBO1dBQ0E7V0FDQSxrQkFBa0IscUJBQXFCO1dBQ3ZDLG9IQUFvSCxpREFBaUQ7V0FDcks7V0FDQSxLQUFLO1dBQ0w7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBOzs7OztXQzdCQTtXQUNBO1dBQ0E7V0FDQSxlQUFlLDRCQUE0QjtXQUMzQyxlQUFlO1dBQ2YsaUNBQWlDLFdBQVc7V0FDNUM7V0FDQTs7Ozs7V0NQQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLHlDQUF5Qyx3Q0FBd0M7V0FDakY7V0FDQTtXQUNBOzs7OztXQ1BBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EsR0FBRztXQUNIO1dBQ0E7V0FDQSxDQUFDOzs7OztXQ1BELDhDQUE4Qzs7Ozs7V0NBOUM7V0FDQTtXQUNBO1dBQ0EsdURBQXVELGlCQUFpQjtXQUN4RTtXQUNBLGdEQUFnRCxhQUFhO1dBQzdEOzs7OztXQ05BOztXQUVBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTs7V0FFQTs7V0FFQTs7V0FFQTs7V0FFQTs7V0FFQTs7V0FFQSw4Q0FBOEM7O1dBRTlDO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQSxpQ0FBaUMsbUNBQW1DO1dBQ3BFO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQSxNQUFNLHFCQUFxQjtXQUMzQjtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBOztXQUVBO1dBQ0E7V0FDQTs7Ozs7VUVsREE7VUFDQTtVQUNBO1VBQ0EsMkRBQTJELHNEQUFzRDtVQUNqSCxxRkFBcUYsdURBQXVEO1VBQzVJIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vd29ya2Zsb3cvLi9zcmMvc2NyaXB0cy9iYXNlLmpzIiwid2VicGFjazovL3dvcmtmbG93Ly4vc3JjL3NjcmlwdHMvdXRpbHMvaHVnTW9uZXlGb3JtYXQuanMiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvLi9zcmMvc3R5bGVzL2Jhc2Uuc2Nzcz9iYzA5Iiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svcnVudGltZS9jaHVuayBsb2FkZWQiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2NvbXBhdCBnZXQgZGVmYXVsdCBleHBvcnQiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2RlZmluZSBwcm9wZXJ0eSBnZXR0ZXJzIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svcnVudGltZS9nbG9iYWwiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly93b3JrZmxvdy93ZWJwYWNrL3J1bnRpbWUvbWFrZSBuYW1lc3BhY2Ugb2JqZWN0Iiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svcnVudGltZS9qc29ucCBjaHVuayBsb2FkaW5nIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svYmVmb3JlLXN0YXJ0dXAiLCJ3ZWJwYWNrOi8vd29ya2Zsb3cvd2VicGFjay9zdGFydHVwIiwid2VicGFjazovL3dvcmtmbG93L3dlYnBhY2svYWZ0ZXItc3RhcnR1cCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTWljcm9Nb2RhbCBmcm9tIFwibWljcm9tb2RhbFwiO1xyXG5pbXBvcnQgJ3RpcHB5LmpzL2Rpc3QvdGlwcHkuY3NzJztcclxuaW1wb3J0ICd0aXBweS5qcy90aGVtZXMvbGlnaHQuY3NzJztcclxuaW1wb3J0IGh1Z01vbmV5Rm9ybWF0IGZyb20gXCIuL3V0aWxzL2h1Z01vbmV5Rm9ybWF0XCI7XHJcbi8vIE9ubHkgbmVlZCB0byBpbXBvcnQgdGhlc2Ugb25jZVxyXG5pbXBvcnQgJ3VuZmV0Y2gvcG9seWZpbGwnO1xyXG5pbXBvcnQgJ2VzNi1wcm9taXNlL2F1dG8nO1xyXG5cclxuLy8gSW1wb3J0IEBzaG9waWZ5L3RoZW1lLWNhcnQgYW55d2hlcmUgeW91IG5lZWQgaXRcclxuaW1wb3J0ICogYXMgY2FydCBmcm9tICdAc2hvcGlmeS90aGVtZS1jYXJ0JztcclxuXHJcbmNvbnN0IGltZ1VSTCA9IChzcmMsIHNpemUsIGNyb3ApID0+IHNyY1xyXG4gIC5yZXBsYWNlKC9fKHBpY298aWNvbnx0aHVtYnxzbWFsbHxjb21wYWN0fG1lZGl1bXxsYXJnZXxncmFuZGV8b3JpZ2luYWx8NTAweDUwMHw3Njh4NzY4fDEwMjR4MTAyNHwyMDQ4eDIwNDh8bWFzdGVyKStcXC4vZywgJy4nKVxyXG4gIC5yZXBsYWNlKC9cXC5qcGd8XFwucG5nfFxcLmdpZnxcXC5qcGVnL2csIChtYXRjaCkgPT4gYF8ke3NpemV9X2Nyb3BfJHtjcm9wfSR7bWF0Y2h9YCk7XHJcblxyXG5cclxuaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5mYXFfc2hpcGluZycpKSB7XHJcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZhcV9zaGlwaW5nJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcbiAgICBNaWNyb01vZGFsLnNob3coJ21vZGFsLXNoaXBpbmcnKTtcclxuICB9KVxyXG59XHJcblxyXG5cclxuaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zcGVjaWZpZWQtaXRlbS1saW5rLXBvcHVwJykpIHtcclxuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc3BlY2lmaWVkLWl0ZW0tbGluay1wb3B1cCcpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgTWljcm9Nb2RhbC5zaG93KCdtb2RhbC1zaGlwaW5nJyk7XHJcbiAgfSlcclxufVxyXG5cclxuaWYgKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyZWdpc3RlcicpKSB7XHJcbiAgY29uc3QgaW5wdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZmllbGQtY2hlY2sgaW5wdXQnKTtcclxuICBjb25zdCBidXR0b24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZmllbGQtY2hlY2tfX2J0bicpO1xyXG5cclxuICBpbnB1dC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgIGlucHV0LmNoZWNrZWQgPyBidXR0b24uY2xhc3NMaXN0LnJlbW92ZSgncC1idXR0b24tLXNvbGRvdXQnKSA6IGJ1dHRvbi5jbGFzc0xpc3QuYWRkKCdwLWJ1dHRvbi0tc29sZG91dCcpXHJcbiAgfSlcclxufVxyXG5cclxuZmV0Y2goJy9jYXJ0LmpzJylcclxuICAudGhlbihyZXNwb25zZSA9PiByZXNwb25zZS5qc29uKCkpXHJcbiAgLnRoZW4oZGF0YSA9PiB7XHJcbiAgICBjb25zdCBjYXJ0Q291bnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY2FydC1jb3VudCcpO1xyXG5cclxuICAgIGlmIChkYXRhLml0ZW1fY291bnQgPiAwKSB7XHJcbiAgICAgIGNhcnRDb3VudC5jbGFzc0xpc3QuYWRkKCdpcy1hY3RpdmUnKTtcclxuICAgICAgY2FydENvdW50LmlubmVySFRNTCA9IGRhdGEuaXRlbV9jb3VudDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNhcnRDb3VudC5jbGFzc0xpc3QucmVtb3ZlKCdpcy1hY3RpdmUnKTtcclxuICAgIH1cclxuICB9KTtcclxuY2xhc3MgU2hpcGluZ1BvcHV1cCBleHRlbmRzIEhUTUxFbGVtZW50IHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgICB0aGlzLm9wZW5Qb3B1cEJ0biA9IHRoaXMucXVlcnlTZWxlY3RvcignYnV0dG9uJyk7XHJcblxyXG4gICAgdGhpcy5vbk9wZW4oKTtcclxuICB9XHJcblxyXG4gIG9uT3BlbigpIHtcclxuICAgIHRoaXMub3BlblBvcHVwQnRuLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICBNaWNyb01vZGFsLnNob3coJ21vZGFsLXNoaXBpbmcnKTtcclxuICAgIH0pXHJcbiAgfVxyXG5cclxufVxyXG5cclxuY3VzdG9tRWxlbWVudHMuZGVmaW5lKCdzaGlwaW5nLXBvcHVwLWNvbXBvbmVudCcsIFNoaXBpbmdQb3B1dXApO1xyXG5cclxuXHJcbmNsYXNzIE1lc3NhZ2VCb3ggZXh0ZW5kcyBIVE1MRWxlbWVudCB7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gICAgdGhpcy5vcGVuUG9wdXBCdG4gPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJ2EnKTtcclxuXHJcbiAgICB0aGlzLm9uT3BlbigpO1xyXG4gIH1cclxuXHJcbiAgb25PcGVuKCkge1xyXG4gICAgdGhpcy5vcGVuUG9wdXBCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgIE1pY3JvTW9kYWwuc2hvdygnbW9kYWwtbWVzc2FnZScpXHJcbiAgICB9KVxyXG4gIH1cclxufVxyXG5cclxuY3VzdG9tRWxlbWVudHMuZGVmaW5lKCdtZXNzYWdlLWJveCcsIE1lc3NhZ2VCb3gpO1xyXG5cclxuY2xhc3MgU29sZG91dE1vZGFsIGV4dGVuZHMgSFRNTEVsZW1lbnQge1xyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuICAgIHRoaXMub3BlblBvcHVwQnRuID0gdGhpcy5xdWVyeVNlbGVjdG9yKCdhJyk7XHJcblxyXG4gICAgdGhpcy5vbk9wZW4oKTtcclxuICB9XHJcblxyXG4gIG9uT3BlbigpIHtcclxuICAgIHRoaXMub3BlblBvcHVwQnRuLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICBNaWNyb01vZGFsLnNob3coJ21vZGFsLXNvbGRvdXQnKTtcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcblxyXG5jdXN0b21FbGVtZW50cy5kZWZpbmUoJ3NvbGRvdXQtbW9kYWwnLCBTb2xkb3V0TW9kYWwpO1xyXG5cclxuY2xhc3MgUHJvZHVjdEluZm9yIGV4dGVuZHMgSFRNTEVsZW1lbnQge1xyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgc3VwZXIoKTtcclxuXHJcbiAgICB0aGlzLndyYXBwZXJFbGUgPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5wcm9kdWN0X19pbmZvcicpO1xyXG4gICAgdGhpcy5pbWdTcmMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjaW1nLXNyYycpO1xyXG4gICAgdGhpcy5wcm9kdWN0VGl0bGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjcHJvZHVjdC1wYWdlLXZ1ZScpO1xyXG4gICAgdGhpcy5pbml0KCk7XHJcbiAgfVxyXG5cclxuICBpbml0KCkge1xyXG4gICAgaWYgKHRoaXMuaW1nU3JjICYmIHRoaXMucHJvZHVjdFRpdGxlKSB7XHJcbiAgICAgIGNvbnN0IHNyYyA9IHRoaXMuaW1nU3JjLmRhdGFzZXQuc3JjO1xyXG4gICAgICBjb25zdCB0aXRsZSA9IHRoaXMucHJvZHVjdFRpdGxlLmRhdGFzZXQucHJvZHVjdFRpdGxlO1xyXG4gICAgICBjb25zdCBjb250ZW50ID0gYDxpbWcgc3JjPVwiJHtzcmN9XCIgYWx0PVwiaW1hZ2VcIj5cclxuICAgICAgICA8bGFiZWw+JHt0aXRsZX08L2xhYmVsPlxyXG4gICAgICAgIDxpbnB1dCBuYW1lPVwiY29udGFjdFtwcm9kdWN0XVwiIHZhbHVlPVwiJHt0aXRsZX1cIiBzdHlsZT1cImRpc3BsYXk6IG5vbmU7XCI+XHJcbiAgICAgIGA7XHJcblxyXG4gICAgICB0aGlzLndyYXBwZXJFbGUuaW5uZXJIVE1MID0gY29udGVudDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmN1c3RvbUVsZW1lbnRzLmRlZmluZSgncHJvZHVjdC1pbmZvcicsIFByb2R1Y3RJbmZvcik7XHJcblxyXG5jbGFzcyBDYXJ0TW9kYWwgZXh0ZW5kcyBIVE1MRWxlbWVudCB7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gICAgdGhpcy50aGVtZSA9IHRoZW1lO1xyXG4gICAgdGhpcy5kYXRlRWxlID0gdGhpcy5xdWVyeVNlbGVjdG9yKCcuZGF0ZScpO1xyXG4gICAgdGhpcy50aW1lRWxlID0gdGhpcy5xdWVyeVNlbGVjdG9yKCcjdGltZScpO1xyXG4gICAgdGhpcy5idG5DaGVja291dCA9IHRoaXMucXVlcnlTZWxlY3RvckFsbCgnLmJ0bl9fY2hlY2tvdXQnKTtcclxuICAgIHRoaXMubG9naW5FbGUgPSB0aGlzLnF1ZXJ5U2VsZWN0b3IoJy5idG5fX2NoZWNrb3V0LS1zaG93bW9kYWwnKTtcclxuICAgIHRoaXMuaW5wdXREYXRlID0gdGhpcy5xdWVyeVNlbGVjdG9yKCcudGFibGUtZGF0ZSBpbnB1dFt0eXBlPVwiZGF0ZVwiXScpXHJcbiAgICB0aGlzLmluaXQoKTtcclxuICB9XHJcblxyXG4gIGluaXQoKSB7XHJcbiAgICB0aGlzLmhhbmRsZUNoYW5nZVRpbWUoKTtcclxuICAgIHRoaXMuaGFuZGxlQ2hhbmdlRGF0ZSgpO1xyXG4gICAgdGhpcy5oYW5kbGVVcGRhdGVOb3RlKCk7XHJcbiAgICB0aGlzLmhhbmRsZURhdGVQaWNrZXIoKTtcclxuICAgIHRoaXMubG9naW5FbGUgJiYgdGhpcy5zaG93TG9naW5Nb2RhbCgpO1xyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBhc3luYyAoKSA9PiB7XHJcbiAgICAgIGlmICh3aW5kb3cubG9jYXRpb24uaHJlZi5pbmRleE9mKCdjYXJ0JykgIT0gLTEpIHtcclxuICAgICAgICBjb25zdCByZXNwb24gPSBhd2FpdCBmZXRjaCgnL2NhcnQuanMnKTtcclxuICAgICAgICBjb25zdCByZXNwb25Kc29uID0gYXdhaXQgcmVzcG9uLmpzb24oKTtcclxuXHJcbiAgICAgICAgaWYgKHJlc3Bvbkpzb24uaXRlbV9jb3VudCA+PSAxKSB7XHJcbiAgICAgICAgICBNaWNyb01vZGFsLnNob3coJ21vZGFsLTEnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZ2VuZXJhdGVDYXJ0KHJlc3Bvbkpzb24pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdlbmVyYXRlQ2FydChjYXJ0KSB7XHJcbiAgICBjb25zdCB0YWJsZUJvZHlDb250ZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3RhYmxlQ29udGVudCcpO1xyXG4gICAgY29uc3QgdG90YWxQcmljZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy50b3RhbF9fcHJpY2UtZmluYWwnKTtcclxuXHJcbiAgICBjb25zdCBjYXJ0Q291bnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY2FydC1jb3VudCcpO1xyXG5cclxuICAgIGlmIChjYXJ0Lml0ZW1fY291bnQgPiAwKSB7XHJcbiAgICAgIGNhcnRDb3VudC5jbGFzc0xpc3QuYWRkKCdpcy1hY3RpdmUnKTtcclxuICAgICAgY2FydENvdW50LmlubmVySFRNTCA9IGNhcnQuaXRlbV9jb3VudDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNhcnRDb3VudC5jbGFzc0xpc3QucmVtb3ZlKCdpcy1hY3RpdmUnKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgaWYgKHRvdGFsUHJpY2UpIHtcclxuICAgICAgdG90YWxQcmljZS5pbm5lckhUTUwgPSBodWdNb25leUZvcm1hdChjYXJ0LnRvdGFsX3ByaWNlLCB0aGlzLnRoZW1lKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgY29uc3QgY2FydENvbnRlbnQgPSBjYXJ0Lml0ZW1zLm1hcCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgcmV0dXJuIGA8dHIgY2xhc3M9XCJjYXJ0LWl0ZW1cIj5cclxuICAgICAgPHRkIGNsYXNzPVwicm93LWl0ZW1cIj5cclxuICAgICAgICA8c3ZnIGNsYXNzPVwiY2FydC1pdGVtX19yZW1vdmVcIiBkYXRhLWtleT1cIiR7aXRlbS5rZXl9XCIgc3R5bGU9XCJjdXJzb3I6IHBvaW50ZXJcIiB3aWR0aD1cIjE1XCIgaGVpZ2h0PVwiMTVcIiB2aWV3Qm94PVwiMCAwIDI1IDI2XCIgZmlsbD1cIm5vbmVcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+XHJcbiAgICAgICAgICA8cGF0aCBkPVwiTTAuNzM2MzI4IDEuNjI1OThMMjMuNzM2MyAyNC42MjZcIiBzdHJva2U9XCIjQTg2RDU0XCIgc3Ryb2tlLXdpZHRoPVwiMlwiLz5cclxuICAgICAgICAgIDxwYXRoIGQ9XCJNMjMuNjQ0NSAxLjcwODAxTDAuODE3ODkzIDI0LjUzNDZcIiBzdHJva2U9XCIjQTg2RDU0XCIgc3Ryb2tlLXdpZHRoPVwiMlwiLz5cclxuICAgICAgICA8L3N2Zz5cclxuICAgICAgICA8YSBocmVmPVwiJHtpdGVtLnVybH1cIiBjbGFzcz1cImNhcnQtaXRlbV9faW1hZ2VcIj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiJHtpbWdVUkwoaXRlbS5mZWF0dXJlZF9pbWFnZS51cmwsICc2MHg2MCcsICdjZW50ZXInKX1cIiBhbHQ9XCIke2l0ZW0uZmVhdHVyZWRfaW1hZ2UuYWx0fVwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNhcnQtaXRlbV9fdGl0bGVcIj4ke2l0ZW0udGl0bGV9PC9kaXY+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICA8L3RkPlxyXG4gICAgICA8dGQgY2xhc3M9XCJyb3ctcHJpY2VcIj4ke2h1Z01vbmV5Rm9ybWF0KGl0ZW0ucHJpY2UsIHRoaXMudGhlbWUpIH08L3RkPlxyXG4gICAgICA8dGQgY2xhc3M9XCJyb3ctcXVhbnRpdHlcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwianMtaW5wdXQtbnVtYmVyXCIgZGF0YS12YXJpYW50aWQ9XCIke2l0ZW0uaWR9XCIgZGF0YS1rZXk9XCIke2l0ZW0ua2V5fVwiPlxyXG4gICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJpbnB1dC1kZWNyZWFzZW1lbnRcIj4tPC9idXR0b24+XHJcbiAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJpbnB1dC12YWx1ZVwiIHR5cGU9XCJ0ZXh0XCIgdmFsdWU9XCIke2l0ZW0ucXVhbnRpdHl9XCIgbWluPVwiMVwiPlxyXG4gICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJpbnB1dC1pbmNyZWFzZW1lbnRcIj4rPC9idXR0b24+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvdGQ+XHJcbiAgICAgIDx0ZCBjbGFzcz1cInJvdy10b3RhbFwiPlxyXG4gICAgICAgICR7aHVnTW9uZXlGb3JtYXQoaXRlbS5wcmljZSAqIGl0ZW0ucXVhbnRpdHksIHRoaXMudGhlbWUpIH1cclxuICAgICAgPC90ZD5cclxuICAgIDwvdHI+YDtcclxuICAgIH0pO1xyXG5cclxuICAgICBpZiAodGFibGVCb2R5Q29udGVudCkge1xyXG4gICAgICB0YWJsZUJvZHlDb250ZW50LmlubmVySFRNTCA9IGNhcnRDb250ZW50LmpvaW4oXCJcIilcclxuICAgICB9XHJcblxyXG4gICAgIHRoaXMuaGFuZGxlSW5jcmVhc2UoKTtcclxuICAgICAgdGhpcy5oYW5kbGVSZW1vdmUoKTtcclxuICB9XHJcblxyXG4gIGhhbmRsZUluY3JlYXNlKCkge1xyXG4gICAgY29uc3QgYnRuRGVjcmVhc2VtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmlucHV0LWRlY3JlYXNlbWVudCcpO1xyXG4gICAgY29uc3QgYnRuSW5jcmVhc2VtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmlucHV0LWluY3JlYXNlbWVudCcpO1xyXG5cclxuICAgIFxyXG4gICAgYnRuSW5jcmVhc2VtZW50LmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgICAgICBjb25zdCBrZXkgPSBpdGVtLmNsb3Nlc3QoJy5qcy1pbnB1dC1udW1iZXInKS5kYXRhc2V0LmtleTtcclxuICAgICAgICBsZXQgcXVhbnRpdHkgPSBOdW1iZXIoaXRlbS5wcmV2aW91c0VsZW1lbnRTaWJsaW5nLnZhbHVlKSArIDE7XHJcblxyXG5cclxuICAgICAgICBjYXJ0LnVwZGF0ZUl0ZW0oa2V5LCB7IHF1YW50aXR5IH0pXHJcbiAgICAgICAgICAudGhlbihzdGF0ZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVDYXJ0KHN0YXRlKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGFsZXJ0KCdTb2xkIG91dCBvciBiYWQgcmVzcG9uc2UgZnJvbSBTaG9waWZ5JylcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuXHJcbiAgICBidG5EZWNyZWFzZW1lbnQuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGtleSA9IGl0ZW0uY2xvc2VzdCgnLmpzLWlucHV0LW51bWJlcicpLmRhdGFzZXQua2V5O1xyXG4gICAgICAgIGxldCBxdWFudGl0eSA9IE51bWJlcihpdGVtLm5leHRFbGVtZW50U2libGluZy52YWx1ZSkgLSAxO1xyXG5cclxuICAgICAgICBjYXJ0LnVwZGF0ZUl0ZW0oa2V5LCB7IHF1YW50aXR5IH0pXHJcbiAgICAgICAgICAudGhlbihzdGF0ZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVDYXJ0KHN0YXRlKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGFsZXJ0KCdTb2xkIG91dCBvciBiYWQgcmVzcG9uc2UgZnJvbSBTaG9waWZ5JylcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIGhhbmRsZVJlbW92ZSgpIHtcclxuICAgIGNvbnN0IHJlbW92ZUJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jYXJ0LWl0ZW1fX3JlbW92ZScpO1xyXG4gICAgcmVtb3ZlQnV0dG9uLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgICAgICBjYXJ0LnJlbW92ZUl0ZW0oaXRlbS5kYXRhc2V0LmtleSkudGhlbihzdGF0ZSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmdlbmVyYXRlQ2FydChzdGF0ZSlcclxuICAgICAgICB9KTtcclxuICAgICAgfSlcclxuICAgIH0pXHJcbiAgfVxyXG5cclxuICBzaG93TG9naW5Nb2RhbCgpIHtcclxuICAgIHRoaXMubG9naW5FbGUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgIE1pY3JvTW9kYWwuc2hvdygnbW9kYWwtbG9naW4nKTtcclxuICAgIH0pXHJcbiAgfVxyXG5cclxuICBoYW5kbGVDaGFuZ2VEYXRlKCkge1xyXG4gICAgdGhpcy5kYXRlRWxlLnNldEF0dHJpYnV0ZSgnZGF0YS1kYXRlJywgJycpO1xyXG4gICAgaWYgKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdkYXRlJykpIHtcclxuICAgICAgdmFyIG5vdyA9ICQuZGF0ZXBpY2tlci5mb3JtYXREYXRlKCd5eS9tbS9kZCcsIG5ldyBEYXRlKCkpO1xyXG4gICAgICBpZiggbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2RhdGUnKSA8PSBub3cpIHtcclxuICAgICAgICB0aGlzLmRhdGVFbGUudmFsdWUgPSAnJztcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnZGF0ZScsICcnKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmRhdGVFbGUudmFsdWUgPWxvY2FsU3RvcmFnZS5nZXRJdGVtKCdkYXRlJyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyB0aGlzLmRhdGVFbGUuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgKGUpID0+IHtcclxuICAgIC8vICAgY29uc29sZS5sb2coXCJldmVudCBjaGFuZ2U6IFwiLCBlLnRhcmdldCk7XHJcbiAgICAvLyAgIGxldCBkYXRlID0gJyc7XHJcbiAgICAvLyAgIGRhdGUgPSBlLnRhcmdldC52YWx1ZTtcclxuICAgIC8vICAgZGF0ZSA9IGDjgYrlsYrjgZHluIzmnJvml6UgOiAke2RhdGV9YDtcclxuICAgIC8vICAgdGhpcy5kYXRlRWxlLnNldEF0dHJpYnV0ZSgnZGF0YS1kYXRlJywgZGF0ZSk7XHJcblxyXG4gICAgLy8gICBjb25zdCBkYXRlU3RvcmFnZSA9IGUudGFyZ2V0LnZhbHVlO1xyXG4gICAgLy8gICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnZGF0ZScsIGRhdGVTdG9yYWdlKTtcclxuICAgIC8vIH0pXHJcbiAgICAkKFwiI2RhdGVwaWNrZXJcIikub24oXCJjaGFuZ2VcIixmdW5jdGlvbigpe1xyXG4gICAgICAgIGxldCBkYXRlID0gJyc7XHJcbiAgICAgICAgZGF0ZSA9ICQodGhpcykudmFsKCk7XHJcbiAgICAgICAgZGF0ZSA9IGDjgYrlsYrjgZHluIzmnJvml6UgOiAke2RhdGV9YDtcclxuICAgICAgICAkKHRoaXMpLmF0dHIoJ2RhdGEtZGF0ZScsIGRhdGUpO1xyXG5cclxuICAgICAgICBjb25zdCBkYXRlU3RvcmFnZSA9ICQodGhpcykudmFsKCk7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2RhdGUnLCBkYXRlU3RvcmFnZSk7XHJcbiAgICB9KTtcclxuXHJcbiAgfVxyXG5cclxuICBoYW5kbGVDaGFuZ2VUaW1lKCkge1xyXG4gICAgdGhpcy50aW1lRWxlLnNldEF0dHJpYnV0ZSgnZGF0YS10aW1lJywgJycpO1xyXG5cclxuICAgIGlmIChsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndGltZScpKSB7XHJcbiAgICAgIHRoaXMudGltZUVsZS52YWx1ZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd0aW1lJyk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHRoaXMudGltZUVsZS5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCAoZSkgPT4ge1xyXG4gICAgICBsZXQgdGltZSA9ICcnO1xyXG4gICAgICBpZiAoZS50YXJnZXQudmFsdWUgIT09ICfmjIflrprjgarjgZcnKSB7XHJcbiAgICAgICAgdGltZSA9IGDmmYLplpPluK/mjIflrpogOiAke2UudGFyZ2V0LnZhbHVlfWA7XHJcbiAgICAgICAgdGhpcy50aW1lRWxlLnNldEF0dHJpYnV0ZSgnZGF0YS10aW1lJywgdGltZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IHRpbWVTdG9yYWdlID0gZS50YXJnZXQudmFsdWU7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd0aW1lJywgdGltZVN0b3JhZ2UpO1xyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIGhhbmRsZVVwZGF0ZU5vdGUoKSB7XHJcbiAgICB0aGlzLmJ0bkNoZWNrb3V0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgbGV0IGRhdGUgPSB0aGlzLmRhdGVFbGUuZGF0YXNldC5kYXRlO1xyXG4gICAgICAgIGxldCB0aW1lID0gdGhpcy50aW1lRWxlLmRhdGFzZXQudGltZTtcclxuXHJcbiAgICAgICAgaWYgKGRhdGUgJiYgdGltZSkge1xyXG4gICAgICAgICAgYXdhaXQgZmV0Y2goJy9jYXJ0L3VwZGF0ZS5qcycsIHtcclxuICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgICAgbm90ZTogYCR7ZGF0ZX0sICR7dGltZX1gXHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIC8vIGhhbmRsZURpc2FibGVQYXN0RGF0ZSgpIHtcclxuICAvLyAgIHRoaXMuZGF0ZUVsZS5zZXRBdHRyaWJ1dGUoJ21pbicsICcnKTtcclxuICAvLyAgIHZhciBkdFRvZGF5ID0gbmV3IERhdGUoKTtcclxuICAgIFxyXG4gIC8vICAgdmFyIG1vbnRoID0gZHRUb2RheS5nZXRNb250aCgpICsgMTtcclxuICAvLyAgIHZhciBkYXkgPSBkdFRvZGF5LmdldERhdGUoKSArIDE7XHJcbiAgLy8gICB2YXIgeWVhciA9IGR0VG9kYXkuZ2V0RnVsbFllYXIoKTtcclxuICAvLyAgIGlmKG1vbnRoIDwgMTApXHJcbiAgLy8gICAgICAgbW9udGggPSAnMCcgKyBtb250aC50b1N0cmluZygpO1xyXG4gIC8vICAgaWYoZGF5IDwgMTApXHJcbiAgLy8gICAgICAgZGF5ID0gJzAnICsgZGF5LnRvU3RyaW5nKCk7XHJcbiAgICBcclxuICAvLyAgIHZhciBtaW5EYXRlID0geWVhciArICctJyArIG1vbnRoICsgJy0nICsgZGF5O1xyXG5cclxuXHJcbiAgLy8gICB0aGlzLmRhdGVFbGUuc2V0QXR0cmlidXRlKCdtaW4nLCBtaW5EYXRlKTtcclxuICAvLyB9XHJcblxyXG4gIGhhbmRsZURhdGVQaWNrZXIoKSB7XHJcbiAgICAkKFwiI2RhdGVwaWNrZXJcIikuZGF0ZXBpY2tlcih7XHJcbiAgICAgIG1pbkRhdGU6ICcrMUQnLCAvL+acgOefreaXpeOBruaMh+Wumioz5pel5b6M44GL44KJ5oyH5a6a5Y+v6IO9XHJcbiAgICAgIG1heERhdGU6ICcrMk0nLCAvL+acgOmVt+aXpeOBruaMh+Wumioy44O25pyI5b6M44G+44Gn5oyH5a6a5Y+v6IO9XHJcbiAgICAgICBiZWZvcmVTaG93RGF5OiBmdW5jdGlvbihkYXRlKSB7XHJcbiAgICAgICAgIGlmIChkYXRlLmdldERheSgpID09IDApIHtcclxuICAgICAgICAgICByZXR1cm4gW3RydWUsICdkYXktc3VuZGF5JywgbnVsbF07Ly90cnVl44Gn5Zyf5puc5pel44KS6YG45oqe5Y+v44Gr5aSJ5pu0XHJcbiAgICAgICAgIH0gZWxzZSBpZiAoZGF0ZS5nZXREYXkoKSA9PSA2KSB7XHJcbiAgICAgICAgICAgcmV0dXJuIFt0cnVlLCAnZGF5LXNhdHVyZGF5JywgbnVsbF07Ly90cnVl44Gn5pel5puc5pel44KS6YG45oqe5Y+v44Gr5aSJ5pu0XHJcbiAgICAgICAgIH1cclxuICAgICAgICAgcmV0dXJuIFt0cnVlLCAnZGF5LXdlZWtkYXknLCBudWxsXTtcclxuICAgICAgIH1cclxuICAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmN1c3RvbUVsZW1lbnRzLmRlZmluZSgnY2FydC1tb2RhbCcsIENhcnRNb2RhbCk7IiwiaW1wb3J0IHsgZm9ybWF0TW9uZXkgfSBmcm9tICdAc2hvcGlmeS90aGVtZS1jdXJyZW5jeSc7XHJcblxyXG5jb25zdCBodWdNb25leUZvcm1hdCA9ICh2YWx1ZSwgdGhlbWUpID0+IHtcclxuICBjb25zdCBjdXJyZW5jeUZvcm1hdCA9IGZvcm1hdE1vbmV5KE51bWJlcih2YWx1ZSksIHRoZW1lLm1vbmV5Rm9ybWF0KTtcclxuICBjb25zdCBjdXJyZW5jeURlY2ltYWwgPSBjdXJyZW5jeUZvcm1hdC5zbGljZShjdXJyZW5jeUZvcm1hdC5pbmRleE9mKCcsJykpO1xyXG4gIGlmIChjdXJyZW5jeURlY2ltYWwgPT09ICcsMDAnKSB7XHJcbiAgICByZXR1cm4gY3VycmVuY3lGb3JtYXQuc2xpY2UoMCwgY3VycmVuY3lGb3JtYXQuaW5kZXhPZignLCcpKTtcclxuICB9XHJcbiAgcmV0dXJuIGN1cnJlbmN5Rm9ybWF0O1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgaHVnTW9uZXlGb3JtYXQ7IiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbi8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG5fX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBfX3dlYnBhY2tfbW9kdWxlc19fO1xuXG4iLCJ2YXIgZGVmZXJyZWQgPSBbXTtcbl9fd2VicGFja19yZXF1aXJlX18uTyA9IGZ1bmN0aW9uKHJlc3VsdCwgY2h1bmtJZHMsIGZuLCBwcmlvcml0eSkge1xuXHRpZihjaHVua0lkcykge1xuXHRcdHByaW9yaXR5ID0gcHJpb3JpdHkgfHwgMDtcblx0XHRmb3IodmFyIGkgPSBkZWZlcnJlZC5sZW5ndGg7IGkgPiAwICYmIGRlZmVycmVkW2kgLSAxXVsyXSA+IHByaW9yaXR5OyBpLS0pIGRlZmVycmVkW2ldID0gZGVmZXJyZWRbaSAtIDFdO1xuXHRcdGRlZmVycmVkW2ldID0gW2NodW5rSWRzLCBmbiwgcHJpb3JpdHldO1xuXHRcdHJldHVybjtcblx0fVxuXHR2YXIgbm90RnVsZmlsbGVkID0gSW5maW5pdHk7XG5cdGZvciAodmFyIGkgPSAwOyBpIDwgZGVmZXJyZWQubGVuZ3RoOyBpKyspIHtcblx0XHR2YXIgY2h1bmtJZHMgPSBkZWZlcnJlZFtpXVswXTtcblx0XHR2YXIgZm4gPSBkZWZlcnJlZFtpXVsxXTtcblx0XHR2YXIgcHJpb3JpdHkgPSBkZWZlcnJlZFtpXVsyXTtcblx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcblx0XHRmb3IgKHZhciBqID0gMDsgaiA8IGNodW5rSWRzLmxlbmd0aDsgaisrKSB7XG5cdFx0XHRpZiAoKHByaW9yaXR5ICYgMSA9PT0gMCB8fCBub3RGdWxmaWxsZWQgPj0gcHJpb3JpdHkpICYmIE9iamVjdC5rZXlzKF9fd2VicGFja19yZXF1aXJlX18uTykuZXZlcnkoZnVuY3Rpb24oa2V5KSB7IHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fLk9ba2V5XShjaHVua0lkc1tqXSk7IH0pKSB7XG5cdFx0XHRcdGNodW5rSWRzLnNwbGljZShqLS0sIDEpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0ZnVsZmlsbGVkID0gZmFsc2U7XG5cdFx0XHRcdGlmKHByaW9yaXR5IDwgbm90RnVsZmlsbGVkKSBub3RGdWxmaWxsZWQgPSBwcmlvcml0eTtcblx0XHRcdH1cblx0XHR9XG5cdFx0aWYoZnVsZmlsbGVkKSB7XG5cdFx0XHRkZWZlcnJlZC5zcGxpY2UoaS0tLCAxKVxuXHRcdFx0dmFyIHIgPSBmbigpO1xuXHRcdFx0aWYgKHIgIT09IHVuZGVmaW5lZCkgcmVzdWx0ID0gcjtcblx0XHR9XG5cdH1cblx0cmV0dXJuIHJlc3VsdDtcbn07IiwiLy8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbl9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuXHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cblx0XHRmdW5jdGlvbigpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcblx0XHRmdW5jdGlvbigpIHsgcmV0dXJuIG1vZHVsZTsgfTtcblx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgeyBhOiBnZXR0ZXIgfSk7XG5cdHJldHVybiBnZXR0ZXI7XG59OyIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIGRlZmluaXRpb24pIHtcblx0Zm9yKHZhciBrZXkgaW4gZGVmaW5pdGlvbikge1xuXHRcdGlmKF9fd2VicGFja19yZXF1aXJlX18ubyhkZWZpbml0aW9uLCBrZXkpICYmICFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywga2V5KSkge1xuXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIGtleSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGRlZmluaXRpb25ba2V5XSB9KTtcblx0XHR9XG5cdH1cbn07IiwiX193ZWJwYWNrX3JlcXVpcmVfXy5nID0gKGZ1bmN0aW9uKCkge1xuXHRpZiAodHlwZW9mIGdsb2JhbFRoaXMgPT09ICdvYmplY3QnKSByZXR1cm4gZ2xvYmFsVGhpcztcblx0dHJ5IHtcblx0XHRyZXR1cm4gdGhpcyB8fCBuZXcgRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcblx0fSBjYXRjaCAoZSkge1xuXHRcdGlmICh0eXBlb2Ygd2luZG93ID09PSAnb2JqZWN0JykgcmV0dXJuIHdpbmRvdztcblx0fVxufSkoKTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmosIHByb3ApIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIHByb3ApOyB9IiwiLy8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCIvLyBubyBiYXNlVVJJXG5cbi8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4vLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbi8vIFtyZXNvbHZlLCByZWplY3QsIFByb21pc2VdID0gY2h1bmsgbG9hZGluZywgMCA9IGNodW5rIGxvYWRlZFxudmFyIGluc3RhbGxlZENodW5rcyA9IHtcblx0XCJiYXNlXCI6IDBcbn07XG5cbi8vIG5vIGNodW5rIG9uIGRlbWFuZCBsb2FkaW5nXG5cbi8vIG5vIHByZWZldGNoaW5nXG5cbi8vIG5vIHByZWxvYWRlZFxuXG4vLyBubyBITVJcblxuLy8gbm8gSE1SIG1hbmlmZXN0XG5cbl9fd2VicGFja19yZXF1aXJlX18uTy5qID0gZnVuY3Rpb24oY2h1bmtJZCkgeyByZXR1cm4gaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID09PSAwOyB9O1xuXG4vLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbnZhciB3ZWJwYWNrSnNvbnBDYWxsYmFjayA9IGZ1bmN0aW9uKHBhcmVudENodW5rTG9hZGluZ0Z1bmN0aW9uLCBkYXRhKSB7XG5cdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG5cdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG5cdHZhciBydW50aW1lID0gZGF0YVsyXTtcblx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG5cdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuXHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwO1xuXHRpZihjaHVua0lkcy5zb21lKGZ1bmN0aW9uKGlkKSB7IHJldHVybiBpbnN0YWxsZWRDaHVua3NbaWRdICE9PSAwOyB9KSkge1xuXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuXHRcdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcblx0XHRcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcblx0XHRcdH1cblx0XHR9XG5cdFx0aWYocnVudGltZSkgdmFyIHJlc3VsdCA9IHJ1bnRpbWUoX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cdH1cblx0aWYocGFyZW50Q2h1bmtMb2FkaW5nRnVuY3Rpb24pIHBhcmVudENodW5rTG9hZGluZ0Z1bmN0aW9uKGRhdGEpO1xuXHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuXHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcblx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSgpO1xuXHRcdH1cblx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZHNbaV1dID0gMDtcblx0fVxuXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXy5PKHJlc3VsdCk7XG59XG5cbnZhciBjaHVua0xvYWRpbmdHbG9iYWwgPSBzZWxmW1wid2VicGFja0NodW5rd29ya2Zsb3dcIl0gPSBzZWxmW1wid2VicGFja0NodW5rd29ya2Zsb3dcIl0gfHwgW107XG5jaHVua0xvYWRpbmdHbG9iYWwuZm9yRWFjaCh3ZWJwYWNrSnNvbnBDYWxsYmFjay5iaW5kKG51bGwsIDApKTtcbmNodW5rTG9hZGluZ0dsb2JhbC5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2suYmluZChudWxsLCBjaHVua0xvYWRpbmdHbG9iYWwucHVzaC5iaW5kKGNodW5rTG9hZGluZ0dsb2JhbCkpOyIsIiIsIi8vIHN0YXJ0dXBcbi8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuLy8gVGhpcyBlbnRyeSBtb2R1bGUgZGVwZW5kcyBvbiBvdGhlciBsb2FkZWQgY2h1bmtzIGFuZCBleGVjdXRpb24gbmVlZCB0byBiZSBkZWxheWVkXG5fX3dlYnBhY2tfcmVxdWlyZV9fLk8odW5kZWZpbmVkLCBbXCJ2ZW5kb3JzXCJdLCBmdW5jdGlvbigpIHsgcmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oXCIuL3NyYy9zY3JpcHRzL2Jhc2UuanNcIik7IH0pXG52YXIgX193ZWJwYWNrX2V4cG9ydHNfXyA9IF9fd2VicGFja19yZXF1aXJlX18uTyh1bmRlZmluZWQsIFtcInZlbmRvcnNcIl0sIGZ1bmN0aW9uKCkgeyByZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhcIi4vc3JjL3N0eWxlcy9iYXNlLnNjc3NcIik7IH0pXG5fX3dlYnBhY2tfZXhwb3J0c19fID0gX193ZWJwYWNrX3JlcXVpcmVfXy5PKF9fd2VicGFja19leHBvcnRzX18pO1xuIiwiIl0sIm5hbWVzIjpbIk1pY3JvTW9kYWwiLCJodWdNb25leUZvcm1hdCIsImNhcnQiLCJpbWdVUkwiLCJzcmMiLCJzaXplIiwiY3JvcCIsInJlcGxhY2UiLCJtYXRjaCIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvciIsImFkZEV2ZW50TGlzdGVuZXIiLCJzaG93IiwiZ2V0RWxlbWVudEJ5SWQiLCJpbnB1dCIsImJ1dHRvbiIsImNoZWNrZWQiLCJjbGFzc0xpc3QiLCJyZW1vdmUiLCJhZGQiLCJmZXRjaCIsInRoZW4iLCJyZXNwb25zZSIsImpzb24iLCJkYXRhIiwiY2FydENvdW50IiwiaXRlbV9jb3VudCIsImlubmVySFRNTCIsIlNoaXBpbmdQb3B1dXAiLCJvcGVuUG9wdXBCdG4iLCJvbk9wZW4iLCJIVE1MRWxlbWVudCIsImN1c3RvbUVsZW1lbnRzIiwiZGVmaW5lIiwiTWVzc2FnZUJveCIsIlNvbGRvdXRNb2RhbCIsIlByb2R1Y3RJbmZvciIsIndyYXBwZXJFbGUiLCJpbWdTcmMiLCJwcm9kdWN0VGl0bGUiLCJpbml0IiwiZGF0YXNldCIsInRpdGxlIiwiY29udGVudCIsIkNhcnRNb2RhbCIsInRoZW1lIiwiZGF0ZUVsZSIsInRpbWVFbGUiLCJidG5DaGVja291dCIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJsb2dpbkVsZSIsImlucHV0RGF0ZSIsImhhbmRsZUNoYW5nZVRpbWUiLCJoYW5kbGVDaGFuZ2VEYXRlIiwiaGFuZGxlVXBkYXRlTm90ZSIsImhhbmRsZURhdGVQaWNrZXIiLCJzaG93TG9naW5Nb2RhbCIsIndpbmRvdyIsImxvY2F0aW9uIiwiaHJlZiIsImluZGV4T2YiLCJyZXNwb24iLCJyZXNwb25Kc29uIiwiZ2VuZXJhdGVDYXJ0IiwidGFibGVCb2R5Q29udGVudCIsInRvdGFsUHJpY2UiLCJ0b3RhbF9wcmljZSIsImNhcnRDb250ZW50IiwiaXRlbXMiLCJtYXAiLCJpdGVtIiwiaW5kZXgiLCJrZXkiLCJ1cmwiLCJmZWF0dXJlZF9pbWFnZSIsImFsdCIsInByaWNlIiwiaWQiLCJxdWFudGl0eSIsImpvaW4iLCJoYW5kbGVJbmNyZWFzZSIsImhhbmRsZVJlbW92ZSIsImJ0bkRlY3JlYXNlbWVudCIsImJ0bkluY3JlYXNlbWVudCIsImZvckVhY2giLCJjbG9zZXN0IiwiTnVtYmVyIiwicHJldmlvdXNFbGVtZW50U2libGluZyIsInZhbHVlIiwidXBkYXRlSXRlbSIsInN0YXRlIiwiY2F0Y2giLCJlcnJvciIsImFsZXJ0IiwibmV4dEVsZW1lbnRTaWJsaW5nIiwicmVtb3ZlQnV0dG9uIiwicmVtb3ZlSXRlbSIsInNldEF0dHJpYnV0ZSIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJub3ciLCIkIiwiZGF0ZXBpY2tlciIsImZvcm1hdERhdGUiLCJEYXRlIiwic2V0SXRlbSIsIm9uIiwiZGF0ZSIsInZhbCIsImF0dHIiLCJkYXRlU3RvcmFnZSIsImUiLCJ0aW1lIiwidGFyZ2V0IiwidGltZVN0b3JhZ2UiLCJtZXRob2QiLCJoZWFkZXJzIiwiYm9keSIsIkpTT04iLCJzdHJpbmdpZnkiLCJub3RlIiwibWluRGF0ZSIsIm1heERhdGUiLCJiZWZvcmVTaG93RGF5IiwiZ2V0RGF5IiwiZm9ybWF0TW9uZXkiLCJjdXJyZW5jeUZvcm1hdCIsIm1vbmV5Rm9ybWF0IiwiY3VycmVuY3lEZWNpbWFsIiwic2xpY2UiXSwic291cmNlUm9vdCI6IiJ9